-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 12, 2019 at 07:31 AM
-- Server version: 5.6.44
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customer_credit_march`
--

-- --------------------------------------------------------

--
-- Table structure for table `age_range`
--

CREATE TABLE `age_range` (
  `id` int(11) NOT NULL,
  `range` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `age_range`
--

INSERT INTO `age_range` (`id`, `range`, `status`) VALUES
(1, '15-25', 1),
(2, '26-35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat_detail`
--

CREATE TABLE `chat_detail` (
  `id` int(11) NOT NULL,
  `referId` varchar(255) NOT NULL,
  `msg_from` varchar(255) NOT NULL,
  `msg_to` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `msg_date` varchar(255) NOT NULL,
  `msg_time` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_detail`
--

INSERT INTO `chat_detail` (`id`, `referId`, `msg_from`, `msg_to`, `message`, `status`, `msg_date`, `msg_time`) VALUES
(1, 'mCo1uFiM', '4nsRcQqe', 'Admin', 'hfhjgfghf', 1, '2018-09-12', '18:18:44'),
(2, 'R3aMLtlc', 'Admin', '4nsRcQqe', 'test', 1, '2018-09-12', '18:21:01'),
(3, 'Y48lCAVN', '4nsRcQqe', 'Admin', 'hello', 1, '2018-09-12', '18:23:38'),
(4, 'D8gLPGAT', '4nsRcQqe', 'Admin', 'how are you', 1, '2018-09-12', '18:25:43');

-- --------------------------------------------------------

--
-- Table structure for table `credit_blog`
--

CREATE TABLE `credit_blog` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `addedondate` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_blog`
--

INSERT INTO `credit_blog` (`id`, `image`, `title`, `description`, `addedondate`) VALUES
(1, 'images_1537258069.jpg', 'Lorem ipsum dolor sit amet, consectetur', '<p style=\"box-sizing: border-box; margin: 0px 0px 2.5rem; color: #2a2a2a; font-size: 16px; line-height: 1.6; font-family: Poppins, sans-serif; -webkit-text-stroke: 0.1px rgba(255, 255, 255, 0.01);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Etiam vel purus diam. Morbi tincidunt vel justo ut congue. Vivamus hendrerit, lacus sit amet dignissim venenatis, turpis tortor tristique ligula, in aliquet erat tortor sed velit. Donec aliquam lobortis rhoncus. Integer ut vestibulum justo. Sed volutpat nulla libero, sed vehicula neque condimentum et. Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac, dignissim at nisi. Fusce laoreet dolor ac fermentum cursus. Donec ultricies dolor faucibus aliquet posuere. Suspendisse volutpat purus eget dictum ultricies. Curabitur nec tortor volutpat lorem cursus tempus. Sed efficitur felis in mauris posuere viverra. Ut dolor neque, faucibus id tempus eu, sagittis eget tellus.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2.5rem; color: #2a2a2a; font-size: 16px; line-height: 1.6; font-family: Poppins, sans-serif; -webkit-text-stroke: 0.1px rgba(255, 255, 255, 0.01);\">Vivamus hendrerit, lacus sit amet dignissim venenatis, turpis tortor tristique ligula, in aliquet erat tortor sed velit. Donec aliquam lobortis rhoncus. Integer ut vestibulum justo. Sed volutpat nulla libero, sed vehicula neque condimentum et. Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac, dignissim at nisi. Fusce laoreet dolor ac fermentum cursus. Donec ultricies dolor faucibus aliquet posuere.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2.5rem; color: #2a2a2a; font-size: 16px; line-height: 1.6; font-family: Poppins, sans-serif; -webkit-text-stroke: 0.1px rgba(255, 255, 255, 0.01);\">Suspendisse volutpat purus eget dictum ultricies. Curabitur nec tortor volutpat lorem cursus tempus. Sed efficitur felis in mauris posuere viverra. Ut dolor neque, faucibus id tempus eu, sagittis eget tellus. Sed volutpat nulla libero, sed vehicula neque condimentum et. Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac, dignissim at nisi. Fusce laoreet dolor ac fermentum cursus. Donec ultricies dolor faucibus aliquet posuere. Suspendisse volutpat purus eget dictum ultricies. Curabitur nec tortor volutpat lorem cursus tempus. Sed efficitur felis in mauris posuere viverra. Ut dolor neque, faucibus id tempus eu, sagittis eget tellus.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; color: #2a2a2a; font-size: 16px; line-height: 1.6; font-family: Poppins, sans-serif; -webkit-text-stroke: 0.1px rgba(255, 255, 255, 0.01);\">Vivamus hendrerit, lacus sit amet dignissim venenatis, turpis tortor tristique ligula, in aliquet erat tortor sed velit. Donec aliquam lobortis rhoncus. Integer ut vestibulum justo. Sed volutpat nulla libero, sed vehicula neque condimentum et. Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac, dignissim at nisi. Fusce laoreet dolor ac fermentum cursus. Donec ultricies dolor faucibus aliquet posuere.</p>', '');

-- --------------------------------------------------------

--
-- Table structure for table `credit_commission`
--

CREATE TABLE `credit_commission` (
  `id` int(11) NOT NULL,
  `commission` double NOT NULL,
  `credit_percent` double NOT NULL,
  `selling_percent` double NOT NULL,
  `commisioin_paid_seller` double NOT NULL,
  `commisioin_paid_buyer` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_commission`
--

INSERT INTO `credit_commission` (`id`, `commission`, `credit_percent`, `selling_percent`, `commisioin_paid_seller`, `commisioin_paid_buyer`) VALUES
(1, 7, 70, 30, 50, 50);

-- --------------------------------------------------------

--
-- Table structure for table `credit_contact_form`
--

CREATE TABLE `credit_contact_form` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(225) NOT NULL,
  `phone` varchar(225) NOT NULL,
  `message` varchar(225) NOT NULL,
  `addedondate` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_contact_form`
--

INSERT INTO `credit_contact_form` (`id`, `first_name`, `last_name`, `email`, `phone`, `message`, `addedondate`) VALUES
(1, 'Mariam', 'Powers', 'wydyqopi@mailinator.net', '7888832721', 'test', '2018-09-13'),
(3, 'demo', 'test', 'demo@gmail.com', '345345345', 'fgdgdf34543', '2018-09-13'),
(4, '', '', '', '', '', '2018-09-13'),
(5, '', '', '', '', '', '2018-09-13'),
(6, '', '', '', '', '', '2019-02-20'),
(7, 'Aly', 'Chiman', 'aly1@alychidesigns.com', '061 256 64 28', 'Hello there,\r\n\r\nMy name is Aly and I would like to know if you would have any interest to have your website here at customer-devreview.com promoted as a resource on our blog alychidesign.com ? \r\n\r\nWe are in the midst of updat', '2019-02-20'),
(8, '', '', '', '', '', '2019-03-10'),
(9, '', '', '', '', '', '2019-04-08'),
(10, '', '', '', '', '', '2019-05-03'),
(11, '', '', '', '', '', '2019-06-01'),
(12, '', '', '', '', '', '2019-06-10');

-- --------------------------------------------------------

--
-- Table structure for table `credit_faq`
--

CREATE TABLE `credit_faq` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_faq`
--

INSERT INTO `credit_faq` (`id`, `title`, `description`) VALUES
(1, 'The standard Lorem Ipsum passage, used since the 1500s?', '<p><span style=\"color: #3a3a3a; font-family: Poppins, sans-serif; font-size: 16px; -webkit-text-stroke: 0.1px rgba(255, 255, 255, 0.01);\">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.</span></p>'),
(2, 'The standard Lorem Ipsum passage, used since the 1500s?', '<p><span style=\"color: #3a3a3a; font-family: Poppins, sans-serif; font-size: 16px; -webkit-text-stroke: 0.1px rgba(255, 255, 255, 0.01);\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets.</span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `credit_frontend_footer`
--

CREATE TABLE `credit_frontend_footer` (
  `id` int(11) NOT NULL,
  `image` varchar(225) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_frontend_footer`
--

INSERT INTO `credit_frontend_footer` (`id`, `image`, `title`, `description`) VALUES
(2, '', 'Terms and Conditions', '<h2 style=\"box-sizing: border-box; margin: 0px 0px 0.8125rem; font-family: Poppins, sans-serif; font-weight: 500; line-height: 1.2; color: #3a3a3a; font-size: 1.75rem; -webkit-text-stroke: 0.1px rgba(255, 255, 255, 0.01);\">Lorem ipsum dolor sit amet, consectetur</h2>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2.75rem; color: #2a2a2a; font-size: 1rem; line-height: 1.6; -webkit-text-stroke-width: 0.1px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Etiam vel purus diam. Morbi tincidunt vel justo ut congue. Vivamus hendrerit, lacus sit amet dignissim venenatis, turpis tortor tristique ligula, in aliquet erat tortor sed velit. Donec aliquam lobortis rhoncus. Integer ut vestibulum justo. Sed volutpat nulla libero, sed vehicula neque condimentum et. Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac, dignissim at nisi. Fusce laoreet dolor ac fermentum cursus. Donec ultricies dolor faucibus aliquet posuere. Suspendisse volutpat purus eget dictum ultricies. Curabitur nec tortor volutpat lorem cursus tempus. Sed efficitur felis in mauris posuere viverra. Ut dolor neque, faucibus id tempus eu, sagittis eget tellus.</p>\r\n<h2 style=\"box-sizing: border-box; margin: 0px 0px 0.8125rem; font-family: Poppins, sans-serif; font-weight: 500; line-height: 1.2; color: #3a3a3a; font-size: 1.75rem; -webkit-text-stroke: 0.1px rgba(255, 255, 255, 0.01);\">Lorem ipsum dolor sit</h2>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2.75rem; color: #2a2a2a; font-size: 1rem; line-height: 1.6; -webkit-text-stroke-width: 0.1px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Etiam vel purus diam. Morbi tincidunt vel justo ut congue. Vivamus hendrerit, lacus sit amet dignissim venenatis, turpis tortor tristique ligula, in aliquet erat tortor sed velit.</p>\r\n<ul style=\"box-sizing: border-box; margin: 0px; list-style: none; padding: 0px; color: #212529; font-size: 10.5625px; -webkit-text-stroke-width: 0.1px;\">\r\n<li style=\"box-sizing: border-box; list-style: none; padding: 0px 0px 0px 1.875rem; margin: 0px 0px 2.5rem; color: #2a2a2a; font-size: 1rem; line-height: 1.6; position: relative;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li style=\"box-sizing: border-box; list-style: none; padding: 0px 0px 0px 1.875rem; margin: 0px 0px 2.5rem; color: #2a2a2a; font-size: 1rem; line-height: 1.6; position: relative;\">Cras et lectus at neque mattis ultrices.</li>\r\n<li style=\"box-sizing: border-box; list-style: none; padding: 0px 0px 0px 1.875rem; margin: 0px 0px 2.5rem; color: #2a2a2a; font-size: 1rem; line-height: 1.6; position: relative;\">Phasellus luctus purus ac mi finibus, vitae pharetra arcu porttitor.</li>\r\n<li style=\"box-sizing: border-box; list-style: none; padding: 0px 0px 0px 1.875rem; margin: 0px 0px 2.5rem; color: #2a2a2a; font-size: 1rem; line-height: 1.6; position: relative;\">Mauris mollis risus eu venenatis facilisis.</li>\r\n<li style=\"box-sizing: border-box; list-style: none; padding: 0px 0px 0px 1.875rem; margin: 0px 0px 2.5rem; color: #2a2a2a; font-size: 1rem; line-height: 1.6; position: relative;\">Donec a erat luctus, porta ipsum eget, cursus lacus.</li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2.75rem; color: #2a2a2a; font-size: 1rem; line-height: 1.6; -webkit-text-stroke-width: 0.1px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Etiam vel purus diam. Morbi tincidunt vel justo ut congue.</p>\r\n<h2 style=\"box-sizing: border-box; margin: 0px 0px 0.8125rem; font-family: Poppins, sans-serif; font-weight: 500; line-height: 1.2; color: #3a3a3a; font-size: 1.75rem; -webkit-text-stroke: 0.1px rgba(255, 255, 255, 0.01);\">Lorem ipsum dolor sit amet</h2>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2.75rem; color: #2a2a2a; font-size: 1rem; line-height: 1.6; -webkit-text-stroke-width: 0.1px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Etiam vel purus diam. Morbi tincidunt vel justo ut congue. Vivamus hendrerit, lacus sit amet dignissim venenatis, turpis tortor tristique ligula, in aliquet erat tortor sed velit. Donec aliquam lobortis rhoncus. Integer ut vestibulum justo. Sed volutpat nulla libero, sed vehicula neque condimentum et.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2.75rem; color: #2a2a2a; font-size: 1rem; line-height: 1.6; -webkit-text-stroke-width: 0.1px;\">Nulla est risus, mollis ac risus ac, luctus lacinia tortor. Nunc sed velit magna. Nulla erat quam, auctor vitae volutpat ac, dignissim at nisi. Fusce laoreet dolor ac fermentum cursus. Donec ultricies dolor faucibus aliquet posuere. Suspendisse volutpat purus eget dictum ultricies. Curabitur nec tortor volutpat lorem cursus tempus. Sed efficitur felis in mauris posuere viverra. Ut dolor neque, faucibus id tempus eu, sagittis eget tellus.</p>\r\n<h2 style=\"box-sizing: border-box; margin: 0px 0px 0.8125rem; font-family: Poppins, sans-serif; font-weight: 500; line-height: 1.2; color: #3a3a3a; font-size: 1.75rem; -webkit-text-stroke: 0.1px rgba(255, 255, 255, 0.01);\">Lorem ipsum dolor</h2>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 2.75rem; color: #2a2a2a; font-size: 1rem; line-height: 1.6; -webkit-text-stroke-width: 0.1px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Etiam vel purus diam. Morbi tincidunt vel justo ut congue. Vivamus hendrerit, lacus sit amet dignissim venenatis, turpis tortor tristique ligula, in aliquet erat tortor sed velit. Donec aliquam lobortis rhoncus. Integer ut vestibulum justo. Sed volutpat nulla libero, sed vehicula neque condimentum et.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `credit_login_detials`
--

CREATE TABLE `credit_login_detials` (
  `id` int(11) NOT NULL,
  `userRefId` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `country_code` varchar(255) NOT NULL,
  `tel_number` varchar(255) NOT NULL,
  `access_pin` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `otp` int(11) NOT NULL,
  `expiry_time` varchar(255) NOT NULL,
  `user_active` int(11) NOT NULL COMMENT '0. In Active 1. Active ',
  `status` int(11) NOT NULL COMMENT '1. Approved 0. Disapprove',
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_login_detials`
--

INSERT INTO `credit_login_detials` (`id`, `userRefId`, `first_name`, `country_code`, `tel_number`, `access_pin`, `email`, `otp`, `expiry_time`, `user_active`, `status`, `type`) VALUES
(1, 'rl2TemVb', 'gur', '+91', ' !øîË&/¡\r,Ñ\"ñIé\\', '$2y$10$aH18glCGeL2igtaI46D/pe4.FnTLFgwwrmCObnBB74z5u/7ZI4evm', 'éurzŽÒ´AIœ!Ðq', 0, '2018-08-14 12:04:08', 1, 1, 'Admin'),
(2, 'TBRqe2Iz', 'jeevan ', '+91', 'Aåñ[þ—*gÇÔ)/	ÂÕ', '$2y$10$bR252wqwweFKqxPKVPL8UuWAy4AlSEHSz2E3V/vmF3x/SIdw2QyUG', 'äX²I„8b!¡tÒV¾»¨îËUÜý€Å', 0, '2018-08-14 12:24:11', 1, 1, 'Customer'),
(3, '4nsRcQqe', 'sharan', '+91', 'Ìå¼ÉÎ«m¿Í·xõöŸ„', '$2y$10$s8dENn4nEI8810iyIMZ0FeelM9dhN7RpyhaNbmQN2tweYB4O7Mlyq', 'Ð=‘d]¤*1‚ª£ãè¥XtÒV¾»¨îËUÜý€Å', 0, '2018-08-14 12:34:38', 1, 1, 'Customer'),
(4, '8J6bOpua', 'Papi', '+221', '¨9\ZµÈ¤›¶ƒæÖ’ÙŸ­', '$2y$10$RJt.d1d26aYClqXrTpe1Z.2g0CxqbToggg5dTWQagUi0QH6dPFUCW', 'lî\Z0›GFRã ßÚ†<\róØ\níMDÇÁùxPKŒ', 0, '2018-08-17 19:13:18', 1, 1, 'Customer'),
(5, 'FQf4CXJU', 'Malick ', '+221', '”¶è!¯ö~&\n–*', '$2y$10$e5KgeTvD4peZeAxk7EdrAO.tJCJH5T82D.gEef1xKLYYmAIHkdcvC', '(‡‡\"ž›ýöõHÒÈÃº¦Clþ\0Ò³G›býÚ', 0, '2018-08-17 19:15:29', 1, 1, 'Customer'),
(6, '2w7x6BIb', 'Karim ', '+221', 'b RqµœÛ9‚_&xq–›', '$2y$10$9KVQo2LKn7An24oOEMBMh.aPB4ZiyQIC11FtazwL4vIckDXlQ10Fa', 'Y:bÒŽÌ±(æ…¦Õ´	È<\róØ\níMDÇÁùxPKŒ', 0, '2018-08-17 19:16:46', 1, 1, 'Customer');

-- --------------------------------------------------------

--
-- Table structure for table `credit_networks`
--

CREATE TABLE `credit_networks` (
  `id` int(11) NOT NULL,
  `network` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_networks`
--

INSERT INTO `credit_networks` (`id`, `network`, `status`) VALUES
(1, 'Orange', 1),
(2, 'Kirène avec Orange', 1),
(3, 'expresso', 1),
(4, 'Tous Réseaux', 1);

-- --------------------------------------------------------

--
-- Table structure for table `credit_notification`
--

CREATE TABLE `credit_notification` (
  `id` int(11) NOT NULL,
  `notification_to` varchar(255) NOT NULL,
  `notification_from` varchar(255) NOT NULL,
  `notification_msg` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_notification`
--

INSERT INTO `credit_notification` (`id`, `notification_to`, `notification_from`, `notification_msg`, `status`) VALUES
(1, 'TBRqe2Iz', '4nsRcQqe', '8566841554 offer selling price 2000 purchasing Price 2000', 0),
(2, '8J6bOpua', 'TBRqe2Iz', '9877725795 offer selling price 300 purchasing Price 300', 0),
(3, '8J6bOpua', '2w7x6BIb', '78-425-5932 offer selling price 2000 purchasing Price 2000', 0),
(4, '2w7x6BIb', 'TBRqe2Iz', '9877725795 offer selling price 4000 purchasing Price 4000', 0),
(5, '4nsRcQqe', '2w7x6BIb', '78-425-5932 offer selling price 34 purchasing Price 34', 0);

-- --------------------------------------------------------

--
-- Table structure for table `credit_offer_purchased`
--

CREATE TABLE `credit_offer_purchased` (
  `id` int(11) NOT NULL,
  `userRefId` varchar(255) NOT NULL,
  `offerId` int(11) NOT NULL,
  `transactionId` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1. purchsed 2. not purchased',
  `offer_confirmation` int(11) NOT NULL COMMENT '0. not confirmed 1, confirmed',
  `addedondate` varchar(255) NOT NULL,
  `createdDateTime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_offer_purchased`
--

INSERT INTO `credit_offer_purchased` (`id`, `userRefId`, `offerId`, `transactionId`, `status`, `offer_confirmation`, `addedondate`, `createdDateTime`) VALUES
(1, '4nsRcQqe', 3, 'CM-EfG6gJq', 1, 1, '2018-08-22', '2018-08-22 10:56:38'),
(2, '4nsRcQqe', 2, 'CM-W6HJYCh', 1, 1, '2018-08-22', '2018-08-22 10:57:32'),
(3, '4nsRcQqe', 4, 'CM-Qx7MnTg', 1, 0, '2018-08-22', '2018-08-22 11:02:15'),
(4, 'TBRqe2Iz', 10, 'CM-px7q9Ry', 1, 1, '2018-08-22', '2018-08-22 14:12:41'),
(5, 'TBRqe2Iz', 9, 'CM-6obI8XD', 1, 1, '2018-08-22', '2018-08-22 14:13:07'),
(6, 'TBRqe2Iz', 8, 'CM-Gh0WLmd', 1, 1, '2018-08-22', '2018-08-22 14:13:27'),
(7, '2w7x6BIb', 14, 'CM-5asloI1', 1, 1, '2018-08-23', '2018-08-23 01:40:04'),
(8, '2w7x6BIb', 5, 'CM-9WzPioy', 1, 1, '2018-08-23', '2018-08-23 01:57:34'),
(9, 'TBRqe2Iz', 18, 'CM-2cJDQk3', 1, 1, '2018-08-23', '2018-08-23 12:10:57'),
(10, '2w7x6BIb', 20, 'CM-WH8b7Ac', 1, 1, '2018-08-23', '2018-08-23 15:43:30'),
(11, '8J6bOpua', 11, 'CM-aRwUNdz', 1, 1, '2018-08-23', '2018-08-23 15:50:39'),
(12, '2w7x6BIb', 15, 'CM-loZt6Um', 1, 1, '2018-08-23', '2018-08-23 17:36:42'),
(13, '8J6bOpua', 17, 'CM-RqA3mOu', 1, 1, '2018-08-23', '2018-08-23 18:23:34'),
(14, '2w7x6BIb', 23, 'CM-GbSdkAq', 1, 1, '2018-08-23', '2018-08-23 22:40:33'),
(15, '8J6bOpua', 24, 'CM-ZIubCRn', 1, 1, '2018-08-29', '2018-08-29 02:45:37'),
(16, '2w7x6BIb', 25, 'CM-WYRom4L', 1, 1, '2018-08-29', '2018-08-29 03:12:54'),
(17, '8J6bOpua', 26, 'CM-t1BLuCZ', 1, 1, '2018-09-03', '2018-09-03 03:11:34'),
(18, '2w7x6BIb', 27, 'CM-1RHQblF', 1, 1, '2018-09-03', '2018-09-03 03:31:23'),
(19, '8J6bOpua', 30, 'CM-3yWparm', 1, 1, '2018-09-04', '2018-09-04 03:12:07'),
(20, '8J6bOpua', 32, 'CM-W0cUR5f', 1, 1, '2018-09-04', '2018-09-04 03:14:25'),
(21, '8J6bOpua', 31, 'CM-asImxkR', 1, 0, '2018-09-04', '2018-09-04 04:16:20'),
(22, '2w7x6BIb', 41, 'CM-cmA49CJ', 1, 1, '2018-11-21', '2018-11-21 21:17:59');

-- --------------------------------------------------------

--
-- Table structure for table `credit_offer_rating`
--

CREATE TABLE `credit_offer_rating` (
  `id` int(11) NOT NULL,
  `ratingBy` varchar(255) NOT NULL,
  `userRefId` varchar(255) NOT NULL,
  `offerId` varchar(255) NOT NULL,
  `rating` double NOT NULL,
  `addedondate` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_offer_rating`
--

INSERT INTO `credit_offer_rating` (`id`, `ratingBy`, `userRefId`, `offerId`, `rating`, `addedondate`) VALUES
(1, '2w7x6BIb', 'TBRqe2Iz', '14', 5, '2018-08-23'),
(2, 'TBRqe2Iz', '4nsRcQqe', '10', 5, '2018-08-23'),
(3, 'TBRqe2Iz', '4nsRcQqe', '8', 5, '2018-08-23'),
(4, '2w7x6BIb', '8J6bOpua', '20', 5, '2018-08-23'),
(5, '2w7x6BIb', 'TBRqe2Iz', '15', 4, '2018-08-23'),
(6, '8J6bOpua', 'TBRqe2Iz', '11', 5, '2018-08-23'),
(7, '8J6bOpua', '2w7x6BIb', '17', 4, '2018-08-23'),
(8, '8J6bOpua', '2w7x6BIb', '24', 4, '2018-08-29'),
(9, '2w7x6BIb', '8J6bOpua', '23', 3, '2018-08-29'),
(10, '8J6bOpua', '2w7x6BIb', '26', 3, '2018-09-03'),
(11, '8J6bOpua', '2w7x6BIb', '26', 3, '2018-09-03'),
(12, '2w7x6BIb', '8J6bOpua', '25', 5, '2018-09-03'),
(13, '2w7x6BIb', '8J6bOpua', '27', 4, '2018-09-03'),
(14, '2w7x6BIb', '8J6bOpua', '27', 4, '2018-09-03'),
(15, '8J6bOpua', '2w7x6BIb', '30', 4, '2018-09-04'),
(16, '8J6bOpua', '2w7x6BIb', '32', 5, '2018-09-04');

-- --------------------------------------------------------

--
-- Table structure for table `credit_payment`
--

CREATE TABLE `credit_payment` (
  `id` int(11) NOT NULL,
  `userRefId` varchar(255) NOT NULL,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `date` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `running_balance` double NOT NULL,
  `commission` int(11) NOT NULL,
  `transaction_type` varchar(255) NOT NULL,
  `addedondate` varchar(255) NOT NULL,
  `createdDateTime` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_payment`
--

INSERT INTO `credit_payment` (`id`, `userRefId`, `credit`, `debit`, `date`, `time`, `transaction_id`, `running_balance`, `commission`, `transaction_type`, `addedondate`, `createdDateTime`, `status`) VALUES
(1, '4nsRcQqe', 0, 234, '2018-08-22', '10:56:38', 'CM-EfG6gJq', 2124, 0, 'Purchased', '2018-08-22', '2018-08-22 10:56:38', 1),
(2, '4nsRcQqe', 0, 0, '2018-08-22', '10:56:38', 'CM-EfG6gJq', 2124, 0, 'Sales Commission', '2018-08-22', '2018-08-22 10:56:38', 1),
(3, 'TBRqe2Iz', 234, 0, '2018-08-22', '10:56:38', 'CM-EfG6gJq', 4806, 0, 'OfferSold', '2018-08-22', '2018-08-22 10:56:38', 1),
(4, 'TBRqe2Iz', 0, 0, '2018-08-22', '10:56:38', 'CM-EfG6gJq', 4806, 0, 'Sales Commission', '2018-08-22', '2018-08-22 10:56:38', 1),
(5, '4nsRcQqe', 0, 2000, '2018-08-22', '10:57:32', 'CM-W6HJYCh', 124, 0, 'Purchased', '2018-08-22', '2018-08-22 10:57:32', 1),
(6, '4nsRcQqe', 0, 0, '2018-08-22', '10:57:32', 'CM-W6HJYCh', 124, 0, 'Sales Commission', '2018-08-22', '2018-08-22 10:57:32', 1),
(7, 'TBRqe2Iz', 2000, 0, '2018-08-22', '10:57:32', 'CM-W6HJYCh', 6806, 0, 'OfferSold', '2018-08-22', '2018-08-22 10:57:32', 1),
(8, 'TBRqe2Iz', 0, 200, '2018-08-22', '10:57:32', 'CM-W6HJYCh', 6606, 200, 'Sales Commission', '2018-08-22', '2018-08-22 10:57:32', 1),
(9, '4nsRcQqe', 0, 2000, '2018-08-22', '11:02:15', 'CM-Qx7MnTg', 28000, 0, 'Purchased', '2018-08-22', '2018-08-22 11:02:15', 1),
(10, '4nsRcQqe', 0, 380, '2018-08-22', '11:02:15', 'CM-Qx7MnTg', 27620, 0, 'Sales Commission', '2018-08-22', '2018-08-22 11:02:15', 1),
(11, 'TBRqe2Iz', 2000, 0, '2018-08-22', '11:02:15', 'CM-Qx7MnTg', 8606, 0, 'OfferSold', '2018-08-22', '2018-08-22 11:02:15', 1),
(12, 'TBRqe2Iz', 0, 170, '2018-08-22', '11:02:15', 'CM-Qx7MnTg', 8436, 170, 'Sales Commission', '2018-08-22', '2018-08-22 11:02:15', 1),
(13, 'TBRqe2Iz', 0, 134, '2018-08-22', '14:12:41', 'CM-px7q9Ry', 8302, 0, 'Purchased', '2018-08-22', '2018-08-22 14:12:41', 1),
(14, 'TBRqe2Iz', 0, 30, '2018-08-22', '14:12:41', 'CM-px7q9Ry', 8272, 0, 'Sales Commission', '2018-08-22', '2018-08-22 14:12:41', 1),
(15, '4nsRcQqe', 134, 0, '2018-08-22', '14:12:41', 'CM-px7q9Ry', 27754, 0, 'OfferSold', '2018-08-22', '2018-08-22 14:12:41', 1),
(16, '4nsRcQqe', 0, 20, '2018-08-22', '14:12:41', 'CM-px7q9Ry', 27734, 20, 'Sales Commission', '2018-08-22', '2018-08-22 14:12:41', 1),
(17, 'TBRqe2Iz', 0, 300, '2018-08-22', '14:13:07', 'CM-6obI8XD', 7972, 0, 'Purchased', '2018-08-22', '2018-08-22 14:13:07', 1),
(18, 'TBRqe2Iz', 0, 0, '2018-08-22', '14:13:07', 'CM-6obI8XD', 7972, 0, 'Sales Commission', '2018-08-22', '2018-08-22 14:13:07', 1),
(19, '4nsRcQqe', 300, 0, '2018-08-22', '14:13:07', 'CM-6obI8XD', 28034, 0, 'OfferSold', '2018-08-22', '2018-08-22 14:13:07', 1),
(20, '4nsRcQqe', 0, 0, '2018-08-22', '14:13:07', 'CM-6obI8XD', 28034, 0, 'Sales Commission', '2018-08-22', '2018-08-22 14:13:07', 1),
(21, 'TBRqe2Iz', 0, 2133, '2018-08-22', '14:13:27', 'CM-Gh0WLmd', 5839, 0, 'Purchased', '2018-08-22', '2018-08-22 14:13:27', 1),
(22, 'TBRqe2Iz', 0, 100, '2018-08-22', '14:13:27', 'CM-Gh0WLmd', 5739, 0, 'Sales Commission', '2018-08-22', '2018-08-22 14:13:27', 1),
(23, '4nsRcQqe', 2133, 0, '2018-08-22', '14:13:27', 'CM-Gh0WLmd', 30167, 0, 'OfferSold', '2018-08-22', '2018-08-22 14:13:27', 1),
(24, '4nsRcQqe', 0, 50, '2018-08-22', '14:13:27', 'CM-Gh0WLmd', 30117, 50, 'Sales Commission', '2018-08-22', '2018-08-22 14:13:27', 1),
(25, '2w7x6BIb', 0, 300, '2018-08-23', '01:40:04', 'CM-5asloI1', 494700, 0, 'Purchased', '2018-08-23', '2018-08-23 01:40:04', 1),
(26, '2w7x6BIb', 0, 0, '2018-08-23', '01:40:04', 'CM-5asloI1', 494700, 0, 'Sales Commission', '2018-08-23', '2018-08-23 01:40:04', 1),
(27, 'TBRqe2Iz', 300, 0, '2018-08-23', '01:40:04', 'CM-5asloI1', 6039, 0, 'OfferSold', '2018-08-23', '2018-08-23 01:40:04', 1),
(28, 'TBRqe2Iz', 0, 0, '2018-08-23', '01:40:04', 'CM-5asloI1', 6039, 0, 'Sales Commission', '2018-08-23', '2018-08-23 01:40:04', 1),
(29, '2w7x6BIb', 0, 2000, '2018-08-23', '01:57:34', 'CM-9WzPioy', 492700, 0, 'Purchased', '2018-08-23', '2018-08-23 01:57:34', 1),
(30, '2w7x6BIb', 0, 170, '2018-08-23', '01:57:34', 'CM-9WzPioy', 492530, 0, 'Sales Commission', '2018-08-23', '2018-08-23 01:57:34', 1),
(31, '8J6bOpua', 2000, 0, '2018-08-23', '01:57:34', 'CM-9WzPioy', 506200, 0, 'OfferSold', '2018-08-23', '2018-08-23 01:57:34', 1),
(32, '8J6bOpua', 0, 180, '2018-08-23', '01:57:34', 'CM-9WzPioy', 506020, 180, 'Sales Commission', '2018-08-23', '2018-08-23 01:57:34', 1),
(33, 'TBRqe2Iz', 0, 4000, '2018-08-23', '12:10:57', 'CM-2cJDQk3', 2039, 0, 'Purchased', '2018-08-23', '2018-08-23 12:10:57', 1),
(34, 'TBRqe2Iz', 0, 150, '2018-08-23', '12:10:57', 'CM-2cJDQk3', 1889, 0, 'Sales Commission', '2018-08-23', '2018-08-23 12:10:57', 1),
(35, '2w7x6BIb', 4000, 0, '2018-08-23', '12:10:57', 'CM-2cJDQk3', 496530, 0, 'OfferSold', '2018-08-23', '2018-08-23 12:10:57', 1),
(36, '2w7x6BIb', 0, 150, '2018-08-23', '12:10:57', 'CM-2cJDQk3', 496380, 150, 'Sales Commission', '2018-08-23', '2018-08-23 12:10:57', 1),
(37, '2w7x6BIb', 0, 1000, '2018-08-23', '15:43:30', 'CM-WH8b7Ac', 495380, 0, 'Purchased', '2018-08-23', '2018-08-23 15:43:30', 1),
(38, '2w7x6BIb', 0, 20, '2018-08-23', '15:43:30', 'CM-WH8b7Ac', 495360, 0, 'Sales Commission', '2018-08-23', '2018-08-23 15:43:30', 1),
(39, '8J6bOpua', 1000, 0, '2018-08-23', '15:43:30', 'CM-WH8b7Ac', 507020, 0, 'OfferSold', '2018-08-23', '2018-08-23 15:43:30', 1),
(40, '8J6bOpua', 0, 30, '2018-08-23', '15:43:30', 'CM-WH8b7Ac', 506990, 30, 'Sales Commission', '2018-08-23', '2018-08-23 15:43:30', 1),
(41, '8J6bOpua', 0, 1000, '2018-08-23', '15:50:39', 'CM-aRwUNdz', 505990, 0, 'Purchased', '2018-08-23', '2018-08-23 15:50:39', 1),
(42, '8J6bOpua', 0, 50, '2018-08-23', '15:50:39', 'CM-aRwUNdz', 505940, 0, 'Sales Commission', '2018-08-23', '2018-08-23 15:50:39', 1),
(43, 'TBRqe2Iz', 1000, 0, '2018-08-23', '15:50:39', 'CM-aRwUNdz', 2889, 0, 'OfferSold', '2018-08-23', '2018-08-23 15:50:39', 1),
(44, 'TBRqe2Iz', 0, 50, '2018-08-23', '15:50:39', 'CM-aRwUNdz', 2839, 50, 'Sales Commission', '2018-08-23', '2018-08-23 15:50:39', 1),
(45, '2w7x6BIb', 0, 300, '2018-08-23', '17:36:42', 'CM-loZt6Um', 495060, 0, 'Purchased', '2018-08-23', '2018-08-23 17:36:42', 1),
(46, '2w7x6BIb', 0, 0, '2018-08-23', '17:36:42', 'CM-loZt6Um', 495060, 0, 'Sales Commission', '2018-08-23', '2018-08-23 17:36:42', 1),
(47, 'TBRqe2Iz', 300, 0, '2018-08-23', '17:36:42', 'CM-loZt6Um', 3139, 0, 'OfferSold', '2018-08-23', '2018-08-23 17:36:42', 1),
(48, 'TBRqe2Iz', 0, 0, '2018-08-23', '17:36:42', 'CM-loZt6Um', 3139, 0, 'Sales Commission', '2018-08-23', '2018-08-23 17:36:42', 1),
(49, '8J6bOpua', 0, 400, '2018-08-23', '18:23:34', 'CM-RqA3mOu', 505540, 0, 'Purchased', '2018-08-23', '2018-08-23 18:23:34', 1),
(50, '8J6bOpua', 0, 0, '2018-08-23', '18:23:34', 'CM-RqA3mOu', 505540, 0, 'Sales Commission', '2018-08-23', '2018-08-23 18:23:34', 1),
(51, '2w7x6BIb', 400, 0, '2018-08-23', '18:23:34', 'CM-RqA3mOu', 495460, 0, 'OfferSold', '2018-08-23', '2018-08-23 18:23:34', 1),
(52, '2w7x6BIb', 0, 0, '2018-08-23', '18:23:34', 'CM-RqA3mOu', 495460, 0, 'Sales Commission', '2018-08-23', '2018-08-23 18:23:34', 1),
(53, '2w7x6BIb', 0, 7000, '2018-08-23', '22:40:33', 'CM-GbSdkAq', 488460, 0, 'Purchased', '2018-08-23', '2018-08-23 22:40:33', 1),
(54, '2w7x6BIb', 0, 300, '2018-08-23', '22:40:33', 'CM-GbSdkAq', 488160, 0, 'Sales Commission', '2018-08-23', '2018-08-23 22:40:33', 1),
(55, '8J6bOpua', 7000, 0, '2018-08-23', '22:40:33', 'CM-GbSdkAq', 512540, 0, 'OfferSold', '2018-08-23', '2018-08-23 22:40:33', 1),
(56, '8J6bOpua', 0, 300, '2018-08-23', '22:40:33', 'CM-GbSdkAq', 512240, 300, 'Sales Commission', '2018-08-23', '2018-08-23 22:40:33', 1),
(57, '8J6bOpua', 0, 1500, '2018-08-29', '02:45:37', 'CM-ZIubCRn', 510740, 0, 'Purchased', '2018-08-29', '2018-08-29 02:45:37', 1),
(58, '8J6bOpua', 0, 250, '2018-08-29', '02:45:37', 'CM-ZIubCRn', 510490, 0, 'Sales Commission', '2018-08-29', '2018-08-29 02:45:37', 1),
(59, '2w7x6BIb', 1500, 0, '2018-08-29', '02:45:37', 'CM-ZIubCRn', 489660, 0, 'OfferSold', '2018-08-29', '2018-08-29 02:45:37', 1),
(60, '2w7x6BIb', 0, 250, '2018-08-29', '02:45:37', 'CM-ZIubCRn', 489410, 250, 'Sales Commission', '2018-08-29', '2018-08-29 02:45:37', 1),
(61, '2w7x6BIb', 0, 500, '2018-08-29', '03:12:54', 'CM-WYRom4L', 488910, 0, 'Purchased', '2018-08-29', '2018-08-29 03:12:54', 1),
(62, '2w7x6BIb', 0, 50, '2018-08-29', '03:12:54', 'CM-WYRom4L', 488860, 0, 'Sales Commission', '2018-08-29', '2018-08-29 03:12:54', 1),
(63, '8J6bOpua', 500, 0, '2018-08-29', '03:12:54', 'CM-WYRom4L', 510990, 0, 'OfferSold', '2018-08-29', '2018-08-29 03:12:54', 1),
(64, '8J6bOpua', 0, 50, '2018-08-29', '03:12:54', 'CM-WYRom4L', 510940, 50, 'Sales Commission', '2018-08-29', '2018-08-29 03:12:54', 1),
(65, '8J6bOpua', 0, 1500, '2018-09-03', '03:11:34', 'CM-t1BLuCZ', 509440, 0, 'Purchased', '2018-09-03', '2018-09-03 03:11:34', 1),
(66, '8J6bOpua', 0, 150, '2018-09-03', '03:11:34', 'CM-t1BLuCZ', 509290, 0, 'Sales Commission', '2018-09-03', '2018-09-03 03:11:34', 1),
(67, '2w7x6BIb', 1500, 0, '2018-09-03', '03:11:34', 'CM-t1BLuCZ', 490360, 0, 'OfferSold', '2018-09-03', '2018-09-03 03:11:34', 1),
(68, '2w7x6BIb', 0, 150, '2018-09-03', '03:11:34', 'CM-t1BLuCZ', 490210, 150, 'Sales Commission', '2018-09-03', '2018-09-03 03:11:34', 1),
(69, '2w7x6BIb', 0, 5000, '2018-09-03', '03:31:23', 'CM-1RHQblF', 485210, 0, 'Purchased', '2018-09-03', '2018-09-03 03:31:23', 1),
(70, '2w7x6BIb', 0, 300, '2018-09-03', '03:31:23', 'CM-1RHQblF', 484910, 0, 'Sales Commission', '2018-09-03', '2018-09-03 03:31:23', 1),
(71, '8J6bOpua', 5000, 0, '2018-09-03', '03:31:23', 'CM-1RHQblF', 514290, 0, 'OfferSold', '2018-09-03', '2018-09-03 03:31:23', 1),
(72, '8J6bOpua', 0, 300, '2018-09-03', '03:31:23', 'CM-1RHQblF', 513990, 300, 'Sales Commission', '2018-09-03', '2018-09-03 03:31:23', 1),
(73, '8J6bOpua', 0, 1000, '2018-09-04', '03:12:07', 'CM-3yWparm', 512990, 0, 'Purchased', '2018-09-04', '2018-09-04 03:12:07', 1),
(74, '8J6bOpua', 0, 70, '2018-09-04', '03:12:07', 'CM-3yWparm', 512920, 0, 'Sales Commission', '2018-09-04', '2018-09-04 03:12:07', 1),
(75, '2w7x6BIb', 1000, 0, '2018-09-04', '03:12:07', 'CM-3yWparm', 485910, 0, 'OfferSold', '2018-09-04', '2018-09-04 03:12:07', 1),
(76, '2w7x6BIb', 0, 80, '2018-09-04', '03:12:07', 'CM-3yWparm', 485830, 80, 'Sales Commission', '2018-09-04', '2018-09-04 03:12:07', 1),
(77, '8J6bOpua', 0, 250, '2018-09-04', '03:14:25', 'CM-W0cUR5f', 512670, 0, 'Purchased', '2018-09-04', '2018-09-04 03:14:25', 1),
(78, '8J6bOpua', 0, 0, '2018-09-04', '03:14:25', 'CM-W0cUR5f', 512670, 0, 'Sales Commission', '2018-09-04', '2018-09-04 03:14:25', 1),
(79, '2w7x6BIb', 250, 0, '2018-09-04', '03:14:25', 'CM-W0cUR5f', 486080, 0, 'OfferSold', '2018-09-04', '2018-09-04 03:14:25', 1),
(80, '2w7x6BIb', 0, 0, '2018-09-04', '03:14:25', 'CM-W0cUR5f', 486080, 0, 'Sales Commission', '2018-09-04', '2018-09-04 03:14:25', 1),
(81, '8J6bOpua', 0, 1500, '2018-09-04', '04:16:20', 'CM-asImxkR', 511170, 0, 'Purchased', '2018-09-04', '2018-09-04 04:16:20', 1),
(82, '8J6bOpua', 0, 170, '2018-09-04', '04:16:20', 'CM-asImxkR', 511000, 0, 'Sales Commission', '2018-09-04', '2018-09-04 04:16:20', 1),
(83, '2w7x6BIb', 1500, 0, '2018-09-04', '04:16:20', 'CM-asImxkR', 487580, 0, 'OfferSold', '2018-09-04', '2018-09-04 04:16:20', 1),
(84, '2w7x6BIb', 0, 180, '2018-09-04', '04:16:20', 'CM-asImxkR', 487400, 180, 'Sales Commission', '2018-09-04', '2018-09-04 04:16:20', 1),
(85, '2w7x6BIb', 0, 34, '2018-11-21', '21:17:59', 'CM-cmA49CJ', 487366, 0, 'Purchased', '2018-11-21', '2018-11-21 21:17:59', 1),
(86, '2w7x6BIb', 0, 0, '2018-11-21', '21:17:59', 'CM-cmA49CJ', 487366, 0, 'Sales Commission', '2018-11-21', '2018-11-21 21:17:59', 1),
(87, '4nsRcQqe', 34, 0, '2018-11-21', '21:17:59', 'CM-cmA49CJ', 30151, 0, 'OfferSold', '2018-11-21', '2018-11-21 21:17:59', 1),
(88, '4nsRcQqe', 0, 0, '2018-11-21', '21:17:59', 'CM-cmA49CJ', 30151, 0, 'Sales Commission', '2018-11-21', '2018-11-21 21:17:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `credit_publish_offer`
--

CREATE TABLE `credit_publish_offer` (
  `id` int(11) NOT NULL,
  `userRefId` varchar(255) NOT NULL,
  `credits` double NOT NULL,
  `selling_price` double NOT NULL,
  `credit_network` int(11) NOT NULL,
  `credit_network_status` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `promotion_type` double NOT NULL,
  `internet_volume` varchar(255) NOT NULL,
  `offer_type` varchar(255) NOT NULL COMMENT '1.Credit 2. Internet',
  `status` int(11) NOT NULL COMMENT '1. Active 2. Inactive 3. Expire 4. Sold 5.Stand By 6. Unlimited',
  `addedondate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_publish_offer`
--

INSERT INTO `credit_publish_offer` (`id`, `userRefId`, `credits`, `selling_price`, `credit_network`, `credit_network_status`, `expiry_date`, `promotion_type`, `internet_volume`, `offer_type`, `status`, `addedondate`) VALUES
(1, 'TBRqe2Iz', 2000, 1000, 1, '1', '08/25/2018', 100, '', '1', 3, '2018-08-22'),
(2, 'TBRqe2Iz', 3000, 2000, 1, '1', '08/25/2018', 50, '', '1', 4, '2018-08-22'),
(3, 'TBRqe2Iz', 0, 234, 1, '1', '08/25/2018', 0, '25', '2', 4, '2018-08-22'),
(4, 'TBRqe2Iz', 10000, 2000, 1, '1', '08/25/2018', 233, '', '1', 4, '2018-08-22'),
(5, '8J6bOpua', 6000, 2000, 1, '1', '08/29/2018', 200, '', '1', 4, '2018-08-22'),
(6, 'TBRqe2Iz', 2000, 200, 1, '1', '08/25/2018', 900, '', '1', 3, '2018-08-22'),
(7, '4nsRcQqe', 2300, 1000, 1, '1', '08/25/2018', 130, '', '1', 3, '2018-08-22'),
(8, '4nsRcQqe', 2390, 2133, 1, '1', '08/25/2018', 12, '', '1', 4, '2018-08-22'),
(9, '4nsRcQqe', 0, 300, 1, '1', '08/25/2018', 0, '96', '2', 4, '2018-08-22'),
(10, '4nsRcQqe', 500, 134, 1, '1', '08/31/2018', 273, '', '1', 4, '2018-08-22'),
(11, 'TBRqe2Iz', 2000, 1000, 1, '1', '08/25/2018', 100, '', '1', 4, '2018-08-22'),
(12, 'TBRqe2Iz', 1000, 900, 1, '1', '08/25/2018', 11, '', '1', 3, '2018-08-22'),
(13, 'TBRqe2Iz', 0, 250, 1, '1', '08/25/2018', 0, '75', '2', 3, '2018-08-22'),
(14, 'TBRqe2Iz', 0, 300, 1, '1', '08/25/2018', 0, '75', '2', 4, '2018-08-22'),
(15, 'TBRqe2Iz', 0, 300, 1, '1', '08/25/2018', 0, '95', '2', 4, '2018-08-22'),
(16, '2w7x6BIb', 10000, 9000, 1, '0', '08/28/2018', 11, '', '1', 3, '2018-08-23'),
(17, '2w7x6BIb', 0, 400, 1, '1', '09/21/2018', 0, '150', '2', 4, '2018-08-23'),
(18, '2w7x6BIb', 4500, 4000, 1, '1', '08/31/2018', 13, '', '1', 4, '2018-08-23'),
(19, '8J6bOpua', 1500, 1000, 1, '1', '08/27/2018', 50, '', '1', 3, '2018-08-23'),
(20, '8J6bOpua', 0, 1000, 1, '1', '08/30/2018', 0, '500', '2', 4, '2018-08-23'),
(21, '8J6bOpua', 0, 666, 1, '1', '08/27/2018', 0, '200', '2', 3, '2018-08-23'),
(22, '8J6bOpua', 5000, 4499, 1, '0', '09/20/2018', 11, '', '1', 3, '2018-08-23'),
(23, '8J6bOpua', 9500, 7000, 1, '1', '08/26/2018', 36, '', '1', 4, '2018-08-23'),
(24, '2w7x6BIb', 10000, 1500, 1, '0', '09/22/2018', 567, '', '1', 4, '2018-08-29'),
(25, '8J6bOpua', 1500, 500, 1, '0', '09/14/2018', 200, '', '1', 4, '2018-08-29'),
(26, '2w7x6BIb', 5000, 1500, 1, '0', '09/21/2018', 233, '', '1', 4, '2018-09-03'),
(27, '8J6bOpua', 10000, 5000, 1, '1', '09/15/2018', 100, '', '1', 4, '2018-09-03'),
(28, '2w7x6BIb', 4500, 1500, 1, '0', '09/24/2018', 200, '', '1', 3, '2018-09-04'),
(29, '2w7x6BIb', 8500, 1500, 1, '1', '09/17/2018', 467, '', '1', 3, '2018-09-04'),
(30, '2w7x6BIb', 3000, 1000, 1, '0', '09/28/2018', 200, '', '1', 4, '2018-09-04'),
(31, '2w7x6BIb', 6500, 1500, 1, '0', '09/27/2018', 333, '', '1', 4, '2018-09-04'),
(32, '2w7x6BIb', 0, 250, 1, '1', '09/20/2018', 0, '150', '2', 4, '2018-09-04'),
(33, '2w7x6BIb', 0, 350, 1, '1', '09/10/2018', 0, '500', '2', 3, '2018-09-04'),
(34, '2w7x6BIb', 0, 350, 1, '1', '09/21/2018', 0, '400', '2', 3, '2018-09-04'),
(35, '8J6bOpua', 0, 500, 1, '1', '09/28/2018', 0, '300', '2', 3, '2018-09-04'),
(36, '2w7x6BIb', 4500, 1000, 1, '1', '09/20/2018', 350, '', '1', 3, '2018-09-05'),
(37, '2w7x6BIb', 5500, 1500, 1, '0', '09/30/2018', 267, '', '1', 3, '2018-09-05'),
(38, '2w7x6BIb', 0, 1500, 1, '1', '09/25/2018', 0, '500', '2', 3, '2018-09-05'),
(39, '4nsRcQqe', 2000, 1000, 1, '0', '', 100, '', '1', 1, '2018-09-11'),
(40, '4nsRcQqe', 2000, 1000, 1, '1', '09/14/2018', 100, '', '1', 3, '2018-09-11'),
(41, '4nsRcQqe', 0, 34, 1, '1', '', 0, '23', '2', 4, '2018-09-11'),
(42, '2w7x6BIb', 5000, 3500, 1, '1', '11/24/2018', 43, '', '1', 3, '2018-11-21');

-- --------------------------------------------------------

--
-- Table structure for table `credit_user_detail`
--

CREATE TABLE `credit_user_detail` (
  `id` int(11) NOT NULL,
  `userRefId` varchar(255) NOT NULL,
  `civility` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `residence` int(11) NOT NULL,
  `network_operator` int(11) NOT NULL,
  `age_range` int(11) NOT NULL,
  `created_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_user_detail`
--

INSERT INTO `credit_user_detail` (`id`, `userRefId`, `civility`, `first_name`, `last_name`, `residence`, `network_operator`, `age_range`, `created_date`) VALUES
(1, '1UariTbJ', 'Mr', 'gur', 'kaur', 1, 1, 1, '2018-08-10'),
(2, 'F42BXw6c', 'Mr', 'jeevan', 'gur', 1, 1, 1, '2018-08-10'),
(3, 'rm8xofVK', 'Mrs', 'g', 'tesr', 1, 1, 1, '2018-08-13'),
(4, 'a7dCzQ3G', 'Mrs', 'g', 'tesr', 1, 1, 1, '2018-08-13'),
(5, '29JqETma', 'Mrs', 'g', 'tesr', 1, 1, 1, '2018-08-13'),
(6, 'tFTOim1C', 'Mrs', 'g', 'tesr', 1, 1, 1, '2018-08-13'),
(7, 'xK4tNYl8', 'Mrs', 'g', 'tesr', 1, 1, 1, '2018-08-13'),
(8, 'ntGLBT7a', 'Mrs', 'g', 'tesr', 1, 1, 1, '2018-08-13'),
(9, 'PYUNs2G9', 'Mrs', 'g', 'tesr', 1, 1, 1, '2018-08-13'),
(10, 'HgQsIBL9', 'Mr', 'ff', 'ff', 1, 3, 1, '2018-08-13'),
(11, '5QF7DO40', 'Mrs', 'gur', 'kaur', 1, 1, 1, '2018-08-13'),
(12, 'lMkFVs1p', 'Mrs', 'fs', 'fdfds', 1, 1, 1, '2018-08-13'),
(13, 'zEu8M3fl', 'Mrs', 'fs', 'fdfds', 1, 1, 1, '2018-08-13'),
(14, 'aGDC5h8Y', 'Mrs', 'fs', 'fdfds', 1, 1, 1, '2018-08-13'),
(15, 'Im2vCZHk', 'Mrs', 'fs', 'fdfds', 1, 1, 1, '2018-08-13'),
(16, 'rl2TemVb', 'Mrs', 'gur', 'jeevan', 1, 1, 2, '2018-08-14'),
(17, 'TBRqe2Iz', 'Mrs', 'jeevan ', 'gur', 1, 1, 1, '2018-08-14'),
(18, '4nsRcQqe', 'Mr', 'sharan', 'singh', 1, 1, 2, '2018-08-14'),
(19, '8J6bOpua', 'Mr', 'Papi', 'DIALLO ', 1, 1, 2, '2018-08-17'),
(20, 'FQf4CXJU', 'Mr', 'Malick ', 'Ndoye ', 1, 1, 2, '2018-08-17'),
(21, '2w7x6BIb', 'Mr', 'Karim ', 'LY ', 1, 1, 2, '2018-08-17');

-- --------------------------------------------------------

--
-- Table structure for table `credit_wallet`
--

CREATE TABLE `credit_wallet` (
  `id` int(11) NOT NULL,
  `userRefId` varchar(255) NOT NULL,
  `wallet_amount` double NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_wallet`
--

INSERT INTO `credit_wallet` (`id`, `userRefId`, `wallet_amount`, `status`) VALUES
(1, 'TBRqe2Iz', 3139, 1),
(2, '4nsRcQqe', 30151, 1),
(3, '8J6bOpua', 511000, 1),
(4, 'FQf4CXJU', 500000, 1),
(5, '2w7x6BIb', 487366, 1);

-- --------------------------------------------------------

--
-- Table structure for table `credit_withdraw_amount`
--

CREATE TABLE `credit_withdraw_amount` (
  `id` int(11) NOT NULL,
  `userrefId` varchar(255) NOT NULL,
  `uniqueId` varchar(255) NOT NULL,
  `withdraw_amount` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_withdraw_amount`
--

INSERT INTO `credit_withdraw_amount` (`id`, `userrefId`, `uniqueId`, `withdraw_amount`, `request_date`, `status`) VALUES
(1, '4nsRcQqe', 'nDqlRu8E', '600', '2018-09-21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `network_operator`
--

CREATE TABLE `network_operator` (
  `id` int(11) NOT NULL,
  `operator` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `network_operator`
--

INSERT INTO `network_operator` (`id`, `operator`, `status`) VALUES
(1, 'Orange', 0),
(3, 'ab', 0),
(5, 'Tigo', 0),
(6, 'Expresso', 0),
(7, 'Kirène avec Orange', 0),
(19, 'vodafonewwwww', 1);

-- --------------------------------------------------------

--
-- Table structure for table `region_residence`
--

CREATE TABLE `region_residence` (
  `id` int(11) NOT NULL,
  `region` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `region_residence`
--

INSERT INTO `region_residence` (`id`, `region`, `status`) VALUES
(1, 'India', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `age_range`
--
ALTER TABLE `age_range`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_detail`
--
ALTER TABLE `chat_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_blog`
--
ALTER TABLE `credit_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_commission`
--
ALTER TABLE `credit_commission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_contact_form`
--
ALTER TABLE `credit_contact_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_faq`
--
ALTER TABLE `credit_faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_frontend_footer`
--
ALTER TABLE `credit_frontend_footer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_login_detials`
--
ALTER TABLE `credit_login_detials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_networks`
--
ALTER TABLE `credit_networks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_notification`
--
ALTER TABLE `credit_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_offer_purchased`
--
ALTER TABLE `credit_offer_purchased`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_offer_rating`
--
ALTER TABLE `credit_offer_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_payment`
--
ALTER TABLE `credit_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_publish_offer`
--
ALTER TABLE `credit_publish_offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_user_detail`
--
ALTER TABLE `credit_user_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_wallet`
--
ALTER TABLE `credit_wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_withdraw_amount`
--
ALTER TABLE `credit_withdraw_amount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `network_operator`
--
ALTER TABLE `network_operator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `region_residence`
--
ALTER TABLE `region_residence`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `age_range`
--
ALTER TABLE `age_range`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `chat_detail`
--
ALTER TABLE `chat_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `credit_blog`
--
ALTER TABLE `credit_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `credit_commission`
--
ALTER TABLE `credit_commission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `credit_contact_form`
--
ALTER TABLE `credit_contact_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `credit_faq`
--
ALTER TABLE `credit_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `credit_frontend_footer`
--
ALTER TABLE `credit_frontend_footer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `credit_login_detials`
--
ALTER TABLE `credit_login_detials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `credit_networks`
--
ALTER TABLE `credit_networks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `credit_notification`
--
ALTER TABLE `credit_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `credit_offer_purchased`
--
ALTER TABLE `credit_offer_purchased`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `credit_offer_rating`
--
ALTER TABLE `credit_offer_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `credit_payment`
--
ALTER TABLE `credit_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `credit_publish_offer`
--
ALTER TABLE `credit_publish_offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `credit_user_detail`
--
ALTER TABLE `credit_user_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `credit_withdraw_amount`
--
ALTER TABLE `credit_withdraw_amount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `network_operator`
--
ALTER TABLE `network_operator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `region_residence`
--
ALTER TABLE `region_residence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
