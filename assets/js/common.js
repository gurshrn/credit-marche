jQuery(document).on('click',function(){

/*========================trim space validation=======================*/

	jQuery('body').on('keyup blur', 'input[type = "number"],input[type = "number"],input[type = "text"],input[type = "email"],input[type = "password"]', function (eve) 
	{
		if ((eve.which != 37) && (eve.which != 38) && (eve.which != 39) && (eve.which != 40)) 
		{
			var text = jQuery(this).val();
			text = text.trim();
			if (text == '') {
				jQuery(this).val('');
			}
			var string = jQuery(this).val();
			if (string != "") {
				string = string.replace(/\s+/g, " ");
				jQuery(this).val(string);
			}
		}
	});

/*====================only Alphabet validation====================*/

	jQuery(document).on('keypress', '.alphabets', function (event) 
	{
		var inputValue = event.charCode;
		if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) 
		{
			event.preventDefault();
		}
	});

/*====================Alpha Numeric validation====================*/

 	jQuery('.alphaNumeric').keyup(function() 
 	{
        if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
            this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
        }
    });

/*================Numeric validation  and dot with tab working====================*/

	jQuery(document).on('keypress', '.validNumber', function (eve) 
	{
		if (eve.which == 0) {
			return true;
		}
		else
		{
			if (eve.which == '.') 
			{
				eve.preventDefault();
			}
			if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57)) 
			{
				if (eve.which != 8)
				{
					eve.preventDefault();
				}
			}
		}
	});

/*================only Numeric validation with tab working====================*/

	jQuery(document).on('keypress', '.onlyvalidNumber', function (eve) 
	{
		if (eve.which == 0) 
		{
			return true;
		}
		else
		{
			if ((eve.which != 46 || $(this).val().indexOf('.') == -1) && (eve.which < 48 || eve.which > 57))
			{
				if (eve.which != 8)
				{
					eve.preventDefault();
				}
			}
		}
	});
	//jQuery(".tel-number").intlTelInput();
	
	$(".tel-number").intlTelInput({
		autoHideDialCode: true,
		autoPlaceholder: true,
		separateDialCode: true,
		nationalMode: true,
		initialCountry: "auto",
		onlyCountries: ['in', 'sn'],
	});
	var countryData = $.fn.intlTelInput.getCountryData(),
	  telInput = $(".tel-number"),
	  addressDropdown = $(".tel-number");
	telInput.intlTelInput({
	  utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.5.0/js/utils.js" // just for formatting/placeholders etc
	});

	$.each(countryData, function(i, country) {
	  addressDropdown.append($("<option></option>").attr("value", country.iso2).text(country.name));
	});

	telInput.on("countrychange", function() {
		var countryCode = telInput.intlTelInput("getSelectedCountryData").iso2;
		var countrydialCode = telInput.intlTelInput("getSelectedCountryData").dialCode;
		$('.register-country-code').val('+'+countrydialCode);
	});
	telInput.trigger("countrychange");
	addressDropdown.change(function() {
	  var countryCode = $(this).val();
	  telInput.intlTelInput("setCountry", countryCode);
	});
	
	
	
	$(".tel-numbers").intlTelInput({
		autoHideDialCode: true,
		autoPlaceholder: true,
		separateDialCode: true,
		nationalMode: true,
		initialCountry: "auto",
		onlyCountries: ['in', 'sn'],
	});
	var countryData = $.fn.intlTelInput.getCountryData(),
	  telInput = $(".tel-numbers"),
	  addressDropdown = $(".tel-numbers");
	telInput.intlTelInput({
	  utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.5.0/js/utils.js" // just for formatting/placeholders etc
	});

	$.each(countryData, function(i, country) {
	  addressDropdown.append($("<option></option>").attr("value", country.iso2).text(country.name));
	});

	telInput.on("countrychange", function() {
		var countryCode = telInput.intlTelInput("getSelectedCountryData").iso2;
		var countrydialCode = telInput.intlTelInput("getSelectedCountryData").dialCode;
		$('.login-country-code').val('+'+countrydialCode);
	});
	telInput.trigger("countrychange");
	addressDropdown.change(function() {
	  var countryCode = $(this).val();
	  telInput.intlTelInput("setCountry", countryCode);
	});
		
		
/*================Check email already exist or not====================*/

	$(document).on('keyup change','.email',function(){
	var email = $(this).val();
	if(email == ''){
		$('.checkEmailValid').val('');
	}
	var tablename = $(this).attr('data-attr');
	var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    var valid = emailReg.test(email);
	if (!valid && email != '') 
	{

		$('#emailError').html('Invalid email address');
		$('label[for="email"]').css('display','none');
		$('#emailError').css('display','block');
		$('.disabledButton').prop('disabled',true);
    } 
	else
	{
		$('#emailError').html('');
		$('#emailError').css('display','none');
		$('.disabledButton').prop('disabled',false);
        
        if($('#email').val() != '')
        {
			jQuery.ajax({
				type: "POST",
				url: site_url + 'check-email',
				data:{email:email,tablename:tablename},
				dataType: "json",
				success: function (data)
				{
					if(data == true && email != '')
					{
						
						$('#emailError').html('Email already exist.');
						$('#emailError').css('display','block');
						$('.disabledButton').prop('disabled', true);
					}
					else
					{
						$('.checkEmailValid').val('gfd');
						/*if($('.checkEmailValid').val() == true && $('.checkNoValid').val() == true){
							$('.disabledButton').prop('disabled', false);
						}*/
						
						$('#emailError').html('');
						$('#emailError').css('display','none');
					}
				},
			});
		}
	}
});
jQuery(document).on('click','.frontend-search-offers',function(){		
	var type = $(this).attr('data-target');		
	if(type == 'offer-published')	
	{		
		var offerdate = $('.offer-date').val();		
		var offertype = $('.offer-type').val();		
		var networkOperator = $('.network-operator').val();				
		var offerStatus = $('.offer-published-status').val();				
	}		
	if(type == 'offer-purchased')	
	{		
		var offerdate = $('.offer-dates').val();					
		var offertype = $('.offer-types').val();		
		var networkOperator = $('.network-operators').val();
		var offerStatus = '';		
	}
	if(type == 'offer-sold')	
	{		
		var offerdate = $('.sold-offer-date').val();					
		var offertype = $('.sold-offer-type').val();		
		var networkOperator = $('.sold-network-operator').val();
		var offerStatus = '';		
	}	
	if(type == 'home-offers-search' || type == 'view-offers-search')	
	{		
		var offertype = $('.offers-type').val();		
		var networkOperator = $('.networks-operator').val();
		var offerStatus = '';		
		var offerdate = '';		
	}	
	jQuery.ajax({					
		type: "POST",					
		url: site_url + 'frontend-search-offers',					
		data:{offerStatus:offerStatus,offerdate:offerdate,offertype:offertype,networkOperator:networkOperator,type:type},					
		dataType: "html",					
		success: function (data)					
		{			
			if(type == 'offer-published')			
			{				
				$('.allOfferPublished').html(data);			
			}			
			if(type == 'offer-purchased')			
			{				
				$('.allPurchasedOffer').html(data);			
			}
			if(type == 'offer-sold')			
			{				
				$('.allSoldOffer').html(data);			
			}
			if(type == 'home-offers-search' || type == 'view-offers-search')			
			{				
				$('.allResult').html(data);			
			}				
		}			
	});	
});	

	
});

