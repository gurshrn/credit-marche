	jQuery(document).on('click','.offerStatus',function(){
		var status = $(this).attr('data-attr');
		var offerid = $(this).attr('data-id');
		jQuery('#updateStatus').modal('show');
		jQuery('.id').val(offerid);
		jQuery('.status').val(status);
		if(status == 2)
		{
			jQuery('#exampleModalLongTitle').html('Disable Offer');
			jQuery('.user-body').html('Are you sure you want to disable this offer ?');
		}
		if(status == 1)
		{
			jQuery('#exampleModalLongTitle').html('Enable Offer');
			jQuery('.user-body').html('Are you sure you want to enable this offer ?');
		}
		
	});

	jQuery(document).on('click','.updateStatus',function(){

		var id = jQuery('.id').val();
		var status = jQuery('.status').val();
		jQuery.ajax({

			type: "POST",

			url: site_url + 'update-offer-status',

			data:{id:id,status:status},

			dataType: "json",

			success: function (data)

			{

				if(data == true && status == 1)
				{
					$('.enable'+id).attr('data-attr',2);
					$('.enable'+id).addClass('enable-btn');
					$('.enable'+id).removeClass('disable-btn');
					$('.enable'+id).addClass('disable'+id);
					$('.disable'+id).removeClass('enable'+id);
					$('.disable'+id).html('Enabled');
					jQuery('#updateStatus').modal('hide');
				}
				if(data == true && status == 2)
				{
					$('.disable'+id).attr('data-attr',1);
					$('.disable'+id).addClass('disable-btn');
					$('.disable'+id).removeClass('enable-btn');
					$('.disable'+id).addClass('enable'+id);
					$('.enable'+id).removeClass('disable'+id);
					$('.enable'+id).html('Disabled');
					jQuery('#updateStatus').modal('hide');

				}
			}
		});
	});
	jQuery(document).on('click','.fund-modal',function(){
		$('#payFund').modal('show');
		var id = $('.offerId').val();
		var price = $('.offerSellingPrice').val();
		$('.sellingAmount').val(price);
		$('.offerids').val(id);
	});
	jQuery(document).on('click','.paynow-btn',function(){
	   jQuery('#paymentExpress').submit();
	});
	jQuery(document).on('click','.checkedAmount',function(){
		var price = $('.checkedAmount:checked').val();
		$('.sellingAmount').val(price);
	});
	jQuery(document).on('click','.credit-recieve',function()
	{
		var offerid = $(this).attr('data-id');
		var userRef = $(this).attr('data-target');
		$('.offers-id').val(offerid);
		$('.user-ref').val(userRef);
		$('#credit-recieved').modal('show');
	});
	jQuery(document).on('click','.offerrating',function(){
		var userRefId	= $('.user-ref').val();
		var rating 		= $('.rating').val();
		var offerId  	= $('.offers-id').val();
		jQuery.ajax({
			type: "POST",
			url: site_url + 'offer-rating',
			data:{userRefId:userRefId,rating:rating,offerId:offerId},
			dataType: "json",
			success: function (data)
			{
				if(data == true)
				{
					location.reload();
				}
			}
		});
	});
	
	/*====================Chat module==================*/
	
	$(document).on('submit','#chat_form', function(e){
	
		var frmdata = $("#chat_form").serialize();
		$.ajax({
			type: "POST",
			url: site_url + "/send-chat-message",
			data: frmdata,
			success: function(data){
				$('.overlay').hide();
						
				if(data.success)
				{
					$(".individual-chat").load(" .individual-chat");
				}
				$('#txtmsg').val('');
				$('.individual-chat').append(data);
				$('.individual-chat').scrollTop($('.individual-chat')[0].scrollHeight);
			}
		});
		e.preventDefault();
		return false;
	});
	
	$(document).on('click','.creditsClaim',function(){
		$('#claim-credits').modal('show');
		var msgfrom = $(this).attr('data-from');
		var msgto = $(this).attr('data-to');
		var datamsg = $(this).attr('data-msg');
		var offerId = $(this).attr('data-offer');
		$('.notifyFrom').val(msgfrom);
		$('.notifyTo').val(msgto);
		$('.notifyMsg').val(datamsg);
		$('.offerId').val(offerId);
	});
	$(document).on('click','.claimingCredits',function(){
		var notifyFrom = $('.notifyFrom').val();
		var notifyTo = $('.notifyTo').val();
		var notifyMsg = $('.notifyMsg').val();
		var offerId = $('.offerId').val();
		$.ajax({
			type: "POST",
			url: site_url + "/claim-credits",
			data: {notifyFrom:notifyFrom,notifyTo:notifyTo,notifyMsg:notifyMsg,offerId:offerId},
			dataType: "json",
			success: function(data){
				if(data)
				{
					location.reload();
				}
			}
		});
	});
	$(document).on('click','.withdraw-money',function(){
		jQuery('#withdrawMoneyRequest')[0].reset();
		var validator = $("#withdrawMoneyRequest").validate();
		validator.resetForm();
		$('#withdrawMoney').modal('show');
	});
	$(document).on('click','.withdray-amounts',function(){
		var isvalid = $("#withdrawMoneyRequest").valid();
		if (isvalid) 
		{
			var withdrawAmount = $('#withdraw-amount').val();
			var walletAmount = $('.wallet-amount').val();
			var totalwalletAmount = parseFloat(walletAmount)+parseFloat(5);
			if(withdrawAmount <= totalwalletAmount)
			{
				$.ajax({
					type: "POST",
					url: site_url +"/withdraw-money",
					data: {withdrawAmount:withdrawAmount},
					beforeSend: function() {
						$('.overlay').css('display','block');
					},
					dataType:"json",
					success: function(response) {
						if(response == true)
						{
							$('#withdrawMoney').modal('hide');
							toastr.success('Request send successfully', 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						}
					}
				});
			}
			else
			{
				toastr.error('Enter valid amount', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			}
		}
	});
	jQuery(document).on('click','.loadMoreOffers',function(){
		var oldpageNo = $('.pageNo').val();
		var limit = $('.limit').val();
		var count = $('.countOffers').val();
		var pageNo = parseInt(oldpageNo)+parseInt(1);
		var start = parseInt(oldpageNo)*parseInt(limit);
		$('.pageNo').val(pageNo);
		$('.offset').val(start);
		$.ajax({
			type: "POST",
			url: site_url +"/view-offer",
			data: {limit:limit,start:start},
			dataType:"html",
			success: function(response) {
				$('.allResult').append(response);
				if(pageNo*limit >= count)
				{
					$('.pagination').css('display','none');
				}
			}
		});
	});
