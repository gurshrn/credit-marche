jQuery(document).on('click','.approveuser',function(){
	var userRefId = $(this).attr('data-id');
	var status    = $(this).attr('data-status');
	var statusType = $(this).attr('data-attr');
	jQuery('#user-approved').modal('show');
	jQuery('.userid').val(userRefId);
	jQuery('.statusTypes').val(statusType);
	jQuery('.statusType').html(statusType);
	jQuery('.status').val(status);
});

jQuery(document).on('click','.updateUserStatus',function(){
	var userrefId = jQuery('.userid').val();
	var status = jQuery('.status').val();
	var statusType = jQuery('.statusTypes').val();
	jQuery.ajax({
		type: "POST",
		url: site_url + 'admin/update-status',
		data:{userrefId:userrefId,status:status,statusType:statusType},
		dataType: "json",
		success: function (data)
		{
			if(data == true && statusType == "Disable")
			{
				
				$('.Disable'+userrefId).attr('data-status',1);
				$('.Disable'+userrefId).attr('data-attr','Enable');
				$('.Disable'+userrefId).addClass('Enable'+userrefId);
				$('.Disable'+userrefId).removeClass('btn-success');
				$('.Disable'+userrefId).addClass('btn-danger');
				$('.Enable'+userrefId).removeClass('Disable'+userrefId);
				$('.Enable'+userrefId).html('Disabled');
				jQuery('#user-approved').modal('hide');
			}
			if(data == true && statusType == "Enable")
			{
				
				$('.Enable'+userrefId).attr('data-status',0);
				$('.Enable'+userrefId).attr('data-attr','Disable');
				$('.Enable'+userrefId).addClass('Disable'+userrefId);
				$('.Enable'+userrefId).removeClass('btn-danger');
				$('.Enable'+userrefId).addClass('btn-success');
				$('.Disable'+userrefId).removeClass('Enable'+userrefId);
				$('.Disable'+userrefId).html('Enabled');
				jQuery('#user-approved').modal('hide');
			}
			if(data == true && statusType == "Reject")
			{
				
				$('.Reject'+userrefId).attr('data-status',1);
				$('.Reject'+userrefId).attr('data-attr','Approve');
				$('.Reject'+userrefId).addClass('Approve'+userrefId);
				$('.Reject'+userrefId).removeClass('btn-success');
				$('.Reject'+userrefId).addClass('btn-danger');
				$('.Approve'+userrefId).removeClass('Reject'+userrefId);
				$('.Approve'+userrefId).html('Rejected');
				jQuery('#user-approved').modal('hide');
			}
			if(data == true && statusType == "Approve")
			{
				
				$('.Approve'+userrefId).attr('data-status',0);
				$('.Approve'+userrefId).attr('data-attr','Reject');
				$('.Approve'+userrefId).addClass('Reject'+userrefId);
				$('.Approve'+userrefId).removeClass('btn-danger');
				$('.Approve'+userrefId).addClass('btn-success');
				$('.Reject'+userrefId).removeClass('Approve'+userrefId);
				$('.Reject'+userrefId).html('Approved');
				jQuery('#user-approved').modal('hide');
			}
			
		}	
	});
});	
jQuery(document).on('click','.view-offers',function(){				
	var id = $(this).attr('data-id');		
	jQuery.ajax({			
		type: "POST",			
		url: site_url + 'admin/admin-view-offers',			
		data:{id:id},			
		dataType: "html",			
		success: function (data)			
		{				
			$('#viewOffers').modal('show');				
			$('.get-offer-detail').html(data);			
		}		
	});	
});		
jQuery(document).on('change','.offer-status',function(){	
	var statusid = $(this).val();
	var typeid = $('select[name=offerType] option:selected').val();			
	var status = $('option:selected', this).attr('data-type');
	var keyword = $('.search-offers').val();
	var type = $('select[name=offerType] option:selected').attr('data-type');	
		
	jQuery.ajax({			
		type: "POST",			
		url: site_url + 'admin/offer-detail-by-status',			
		data:{typeid:typeid,statusid:statusid,status:status,keyword:keyword},			
		dataType: "html",			
		success: function (data)			
		{				
			$('#allResult').html(data);			
		}		
	});			
});	
jQuery(document).on('change','.offer-type',function(){	
	var typeid = $(this).val();
	var statusid = $('select[name=offerStatus] option:selected').val();		
	var type = $('option:selected', this).attr('data-type');
	var keyword = $('.search-offers').val();
	var status = $('select[name=offerStatus] option:selected').attr('data-type');	
	jQuery.ajax({			
		type: "POST",			
		url: site_url + 'admin/offer-detail-by-status',			
		data:{statusid:statusid,typeid:typeid,type:type,status:status,keyword:keyword},			
		dataType: "html",			
		success: function (data)			
		{				
			$('#allResult').html(data);			
		}		
	});			
});	
jQuery(document).on('keyup','.search-offers',function(){		
	var keyword = $(this).val();		
	var statusid = $('select[name=offerStatus] option:selected').val();		
	var typeid = $('select[name=offerType] option:selected').val();		
	var status = $('select[name=offerStatus] option:selected').attr('data-type');
	var type = $('select[name=offerType] option:selected').attr('data-type');	
	jQuery.ajax({			
		type: "POST",			
		url: site_url + 'admin/offer-detail-by-status',			
		data:{statusid:statusid,typeid:typeid,type:type,keyword:keyword,status:status},			
		dataType: "html",			
		success: function (data)			
		{				
			$('#allResult').html(data);			
		}		
	});	
});	
jQuery(document).on('click','.delete-credits',function(){
	var id = $(this).attr('data-id');
	var tablename = $(this).attr('data-target');
	$('#deleteModalTitle').html('Delete Credits Network');
	$('.delete-body').html('Are you sure you want to delete this credit network ?');
	$('#deleteModal').modal('show');
	$('.id').val(id);
	$('.tablename').val(tablename);
});

/*=============common add modal=================*/

jQuery(document).on('click','.common-add',function(){
	$('#commonModal').modal('show');
	$('#modal-title').html('Add');
	$('.btn-value').html('Add');
	$('form').each(function() { this.reset() });
	var validator = $("#add-credit-network").validate();
	var validator1 = $("#add-network-operator").validate();
	var validator2 = $("#add-credit-commission").validate();
	validator.resetForm();
	validator1.resetForm();
	validator2.resetForm();
});

/*=============common edit function=================*/

	$.fn.edit = function() {
		$('#commonModal').modal('show');
		$('#modal-title').html('Update');
		$('.btn-value').html('Update');
		var editId = $(this).attr('data-id');
		var url = $(this).attr('data-rel');
		var tablename = $(this).attr('data-target');
		jQuery.ajax({
			type: "POST",
			url: site_url +'admin/' + url,
			data:{editId:editId,tablename:tablename},
			dataType: "json",
			success: function (data) 
			{
				$('.inputId').val(data.id);
				
				$('.network').val(data.network);
				$('.operator').val(data.operator);
				
				$('.commission').val(data.commission);
				$('.creditValue').val(data.credit_percent);
				$('.sellingValue').val(data.selling_percent);
				$('.paidSeller').val(data.commisioin_paid_seller);
				$('.paidBuyer').val(data.commisioin_paid_buyer);

			},
			error: function()
			{
        		
    		}
		});
	}

	$(document).on('click','.editInformation',function(){
		$(this).edit();
	});

/*=============common delete function=================*/
	jQuery(document).on('click','.delete',function(){
		var id = $('.id').val();
		var tablename = $('.tablename').val();
		jQuery.ajax({			
			type: "POST",			
			url: site_url + 'admin/delete-table-row',			
			data:{id:id,tablename:tablename},			
			dataType: "json",			
			success: function (data)			
			{				
				//$('#allResult').html(data);			
			}		
		});	
	});
	
	jQuery(document).on('click','.view-chat',function(){
		var userRefId = $(this).attr('data-id');
		jQuery.ajax({			
			type: "POST",			
			url: site_url + 'admin/getChatDetail',			
			data:{userRefId:userRefId},			
			dataType: "html",			
			success: function (data)			
			{				
					$('#chatdetail').html(data);	
					$('#notificationDetail').css('display','none');	
			}		
		});	
	});
	
	$(document).on('click','.searchTransactionDetail',function(){
		var paymentType = $('.payment-type').val();		
		var keyword = $('.keyword').val();
		var transactiondate = $('.transactiondate').val();		
		jQuery.ajax({					
			type: "POST",					
			url: site_url + 'admin/search-transaction',					
			data:{paymentType:paymentType,keyword:keyword,transactiondate:transactiondate},					
			dataType: "html",					
			success: function (data)					
			{			
				$('#allResult').html(data);			
			}			
		});	
	});
	
	$(document).on('keyup','.creditValue',function()
	{
		var creditvalue = $(this).val();
		var sellingValue = parseInt(100)-parseInt(creditvalue);
		if(creditvalue != '')
		{
			$('.sellingValue').val(sellingValue);
		}
		else
		{
			$('.sellingValue').val('');
		}
		
	});
	$(document).on('keyup','.paidSeller',function()
	{
		var paidSeller = $(this).val();
		var paidBuyer = parseInt(100)-parseInt(paidSeller);
		if(paidSeller != '')
		{
			$('.paidBuyer').val(paidBuyer);
		}
		else
		{
			$('.paidBuyer').val('');
		}
		
	});
	jQuery(document).on('click','.standByStatus',function(){
		var status = $(this).attr('data-attr');
		var offerid = $(this).attr('data-id');
		jQuery('#updateStatus').modal('show');
		jQuery('.id').val(offerid);
		jQuery('.status').val(status);
		$('.updateStatus').addClass('updateStandByStatus');
		$('.updateStatus').removeClass('updateStatus');
		if(status == 1)
		{
			jQuery('#exampleModalLongTitle').html('Enable Offer');
			jQuery('.user-body').html('Are you sure you want to enable this offer ?');
		}
		
	});
	
	jQuery(document).on('click','.updateStandByStatus',function(){

		var id = jQuery('.id').val();
		var status = jQuery('.status').val();
		jQuery.ajax({

			type: "POST",

			url: site_url + 'update-offer-status',

			data:{id:id,status:status},

			dataType: "json",

			success: function (data)

			{

				if(data == true && status == 1)
				{
					$('.standBy'+id).attr('data-attr',2);
					$('.standBy'+id).addClass('enable-btn');
					$('.standBy'+id).removeClass('btn-warning');
					$('.standBy'+id).addClass('disable'+id);
					$('.standBy'+id).addClass('buy-btn');
					$('.standBy'+id).addClass('offerStatus');
					$('.standBy'+id).removeClass('standByStatus');
					$('.standBy'+id).html('Enabled');
					$('.standBy'+id).removeClass('standBy6');
					$('.updateStandByStatus').addClass('updateStatus');
					$('.updateStandByStatus').removeClass('updateStandByStatus');
					jQuery('#updateStatus').modal('hide');
				}
				
			}
		});
	});