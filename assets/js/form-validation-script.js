jQuery(document).ready(function(){

/*===================common submit form function====================*/

	function submitHandler (form) 
	{
		$.ajax({
			url: form.action,
			type: form.method,
			data: new FormData(form),
			contentType: false,       
			cache: false,             
			processData:false, 
			dataType: "json",
			beforeSend: function() {
				$('.overlay').css('display','block');
			},
			success: function(response) {
               console.log(response.success);

				if(response.success == true)
				{
					toastr.success(response.success_message, 'Success Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					//$('.disabledButton').prop('disabled', true);
					
						setTimeout(
						  	function() 
						  	{
						  		if(response.userRefId)
						  		{
						  			jQuery('#userRefId').val(response.userRefId);
									$("#otpModal").modal({ backdrop: 'static',keyboard: false});
						  			jQuery('#login').modal('hide');
						  		}
						  		else if(response.otp)
						  		{
						  			jQuery('#otpModal').modal('hide');
						  			$("#login").modal({ backdrop: 'static',keyboard: false});
						  			jQuery('#pills-login-tab').attr('aria-selected',true);
									jQuery('#pills-register-tab').attr('aria-selected',false);
									jQuery('#pills-login-tab').addClass('active');
									jQuery('#pills-register-tab').removeClass('active');
									jQuery('#pills-login').addClass('active show');
									jQuery('#pills-register').removeClass('active show');
						  		}
						  		else if(response.url)
						  		{
						  			window.location = response.url;
						  		}
								else if(response.type == 'contact form')
								{
									location.reload();
								}
						  		
									
						  	}, 2000);
					
				}
				if(response.success == false)
				{
					toastr.error(response.error_message, 'Error!', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					$('.disabledButton').prop('disabled', false);
					
				}
			}
		});
	}

/*======================register user form===================*/

	jQuery("#register-form").validate({
		rules:
		{
			first_name: 
			{
				required: true,
			},
			last_name: 
			{
				required: true,
			},
			tel_number: 
			{
				required: true,
			},
			residence: 
			{
				required: true,
			},
			network_operator: 
			{
				required: true,
			},
			age_range: 
			{
				required: true,
			},
			access_pin: 
			{
				required: true,
			},
			confirm_access_pin: 
			{
				required: true,
				equalTo: '#accessPin'
			},
			nat_id:
			{
				required:true,
				min:5,
			},
			email:
			{
				required:true,
			},
			civility:
			{
				required:true,
			}
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});

/*======================login user form===================*/

	jQuery("#login-form").validate({
		rules:
		{
			tel_number: 
			{
				required: true,
			},
			password: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	
	jQuery("#admin-login").validate({
		rules:
		{
			tel_number: 
			{
				required: true,
				min:10,
			},
			password: 
			{
				required: true,
				min:4,
			}
		},

		messages: 
		{
		
			
		},
		submitHandler: function(form) 
		{
			$.ajax({
			url: form.action,
			type: form.method,
			data: new FormData(form),
			contentType: false,       
			cache: false,             
			processData:false, 
			dataType: "json",
			beforeSend: function() {
				$('.overlay').css('display','block');
			},
			success: function(response) {
               

				if(response.success == true)
				{
					toastr.success(response.success_message, 'Success Alert', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					
						setTimeout(
						  	function() 
						  	{
						  		if(response.url)
						  		{
						  			window.location = response.url;
						  		}
						  		
									
						  	}, 2000);
					
				}
				if(response.success == false)
				{
					toastr.error(response.error_message, 'Error!', {timeOut: 2500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					$('.disabledButton').prop('disabled', false);
					
				}
			}
		});

		}
	});

	jQuery("#check-otp").validate({
		rules:
		{
			otp: 
			{
				required: true,
			},
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});

	jQuery("#publish-credit-offer").validate({
		rules:
		{
			credits: 
			{
				required: true,
			},
			expiry_date: 
			{
				required: true,
			},
			promotion_type: 
			{
				required: true,
			},
			selling_price: 
			{
				required: true,
			},
			credit_network:
			{
				required: true,
			}
		},

		messages: 
		{
			
		}
	});
	
	jQuery("#internet-offer").validate({
		rules:
		{
			internet_volume: 
			{
				required: true,
			},
			network_expiry_date: 
			{
				required: true,
			},
			network_selling_price: 
			{
				required: true,
			},
			network_promotion_type: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		}
	});
	jQuery("#add-credit-network").validate({
		rules:
		{
			network: 
			{
				required: true,
			},
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#add-network-operator").validate({
		rules:
		{
			operator: 
			{
				required: true,
			},
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#add-credit-commission").validate({
		rules:
		{
			commission: 
			{
				required: true,
			},
			credit_percent: 
			{
				required: true,
			},
			selling_percent: 
			{
				required: true,
			},
			commisioin_paid_seller: 
			{
				required: true,
			},
			commisioin_paid_buyer: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	jQuery("#withdrawMoneyRequest").validate({
		rules:
		{
			withdraw_amount: 
			{
				required: true,
			},
		},

		messages: 
		{
			
		}
	});
	jQuery("#footerForm").validate({
		rules:
		{
			first_name: 
			{
				required: true,
			},
			last_name: 
			{
				required: true,
			},
			email: 
			{
				required: true,
			},
			phone: 
			{
				required: true,
			},
			message: 
			{
				required: true,
			}
		},

		messages: 
		{
			
		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});
	setTimeout(function(){ 
		$('.footersection').attr('disabled',false);
		jQuery("#add-footer-section").validate({
		rules:
		{
			title: 
			{
				required: true,
			},
			description: 
			{
				required: true,
			},
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});


	 }, 1000);
	 
	setTimeout(function(){ 
		$('.footersection').attr('disabled',false);
		jQuery("#add-blog").validate({
		rules:
		{
			image: 
			{
				required: true,
			},
			title: 
			{
				required: true,
			},
			description: 
			{
				required: true,
			},
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});


	 }, 1000);
	 setTimeout(function(){ 
		$('.footersection').attr('disabled',false);
		jQuery("#add-faq").validate({
		rules:
		{
			title: 
			{
				required: true,
			},
			description: 
			{
				required: true,
			},
		},

		messages: 
		{

		},
		submitHandler: function(form) 
		{
			submitHandler(form);

		}
	});


	 }, 1000);
	
});
