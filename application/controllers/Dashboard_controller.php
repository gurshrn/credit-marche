<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_controller extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('dashboard');
        $this->load->model('home');
		
        
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$userrefId = $this->session->userdata('userRefId');
		if($userrefId == '')
		{
			redirect(site_url('/'));
		}
		else
		{
			$output['parentUrl']= "";
			$output['title']    = "My Dashboard";
			$output['creditNetwork'] = $this->home->getCreditNetwork();
			$output['userOffers'] = $this->dashboard->getOffersByUserRef();
			$output['creditTransaction'] = $this->dashboard->getTransactionDetail();
			$output['offerPurchased'] = $this->dashboard->getOfferPurchaseDetail();
			$output['offerSold'] = $this->dashboard->getOfferSoldDetail();
			$output['chatHistory'] = $this->dashboard->getChatHistory();
			$output['notification'] = $this->dashboard->getNotificationDetail();
			$this->load->view('frontend/dashboard',$output);
		}
	}
	public function updateOfferStatus()
	{
		$output = $this->dashboard->updateStatus($_POST);
		echo json_encode($output);exit;
	}
	public function offerRating()
	{
		$result = $this->dashboard->saveOfferRating($_POST);
		echo json_encode($result);exit;
	}
	public function sendChatMsg()
	{
		$userrefId = $this->session->userdata('userRefId');
		$msg = $_POST['txtmsg'];
        $msgfrom = $_POST['msgfrom'];
		$msgto = $_POST['msgto'];
		if(!empty($msg)){
            $data =array(
                'referId' => generateRef(),
                'msg_from' => $msgfrom,
                'msg_to' => $msgto,
                'message' => $msg,
                'status' => 1,
				'msg_date'=>date('Y-m-d'),
				'msg_time'=>date('H:i:s'),
            );
			$result =	$this->dashboard->sendMessage($data);
			if($result->msg_from!='Admin')
			{
                $user = getUserDetail($result->msg_from);
                $username = ucwords($user->first_name);
            }
			else
			{
				$username = 'Admin';
            }
            // $u = Helper::getEmailByRefId($result->msg_from);
            // $senderMail = $u->email;
            // $r = Helper::getEmailByRefId($result->msg_to);
            // $recMail = $r->email;
            // $maildata = array(
              // 'msg' => $result->message,
              // 'create_date' => $result->create_date,
              // 'sent_by' => $username
            // );
            
            if($userrefId==$result->msg_from)
			{
				$cls = 'your-msg-block';
            }
			elseif($result->msg_from == 'Admin')
			{
				$cls = 'your-msg-block';
			}
			else
			{
				$cls = 'user-msg-block';
            }
			$date = new DateTime($result->msg_date);
            $html = '<div class="'.$cls.'">';
            $html .= '<div class="user-msg"><p><span>'.$result->message.'<time class="msgtime">'.$date->format('m-d-Y').' '.$result->msg_time.'</time></span></p></div>';
			$html .= '<span class="username">'.$username.'</span>';
            $html .= '</div>';

            // $noteData =array(
                // 'referId' => Helper::generateRef(),
                // 'itemId' => $quotid,
                // 'noteFrom' => $msgfrom,
                // 'noteTo' => $msgto,
                // 'status' => 1,
                // 'message' => $msg
            // );
            // Chat::notificationMsg($noteData);
             echo $html;
        }
    }
	public function creditsClaim()
	{
		$result = $this->dashboard->saveNotification($_POST);
		echo json_encode($result);exit;
	}
	public function withdrawMoney()
	{
		$result = $this->dashboard->withdrawMoneyRequest($_POST);
		echo json_encode($result);exit;
	}

	
	
	
	
}
