<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_controller extends CS_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('sms');
        $this->load->model('user');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function userSignup()
	{
		$length = 8;
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
		$length1 = 5;
		$randomString1 = substr(str_shuffle("0123456789"), 0, $length1);
		$postData = $this->input->post();
		$date = date('Y-m-d H:i:s');

        $currentDate = strtotime($date);
        $futureDate = $currentDate + (60 * 10);
		$formatDate = date("Y-m-d H:i:s", $futureDate);

		$salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
        $salt = base64_encode($salt);
        $salt = str_replace('+', '.', $salt);
        $hash = crypt($postData['access_pin'], '$2y$10$' . $salt . '$');
        
       
		
		$data['userRefId'] = $randomString;
		$data['access_pin'] = $hash;
		$data['user_active'] = 0;
		$data['otp'] = $randomString1;
		$data['expiry_time'] = $formatDate;
		$data['first_name'] = $postData['first_name'];
		$data['country_code'] = $postData['country_code'];

		$results = $this->user->addUser($data,$postData);

		if($results)
		{
			$phoneNumber = $data['country_code'].''.$postData['tel_number'];
			$this->sms->sendSms($phoneNumber,$randomString1);
			$result['success'] = $results['success'];
            $result['success_message'] = $results['success_message'];
            $result['userRefId'] = $results['userRefId'];
		}
		else
		{
			$output['success'] = $results['success'];
            $output['error_message'] = $results['error_message'];
		}
		echo json_encode($result);exit;
		
	}

	public function checkOtp()
	{
		$results = $this->user->checkUserOtp($_POST);
		

		if($results['success'] == true)
		{
			$result['success'] = $results['success'];
            $result['success_message'] = $results['success_message'];
            $result['otp'] = 'otp';
        }
		else
		{
			$result['success'] = $results['success'];
            $result['error_message'] = $results['error_message'];
            
		}
		echo json_encode($result);exit;
	}

	public function userLogin()
	{
		$results = $this->user->login($_POST);
		if (count($results) > 0) 
		{
			$this->session->set_userdata('userRefId', $results[0]->userRefId);
            $this->session->set_userdata('firstName', $results[0]->first_name);
			$this->session->set_userdata('logged_in_front', 'frontend');
            $result['success'] = true;
            $result['success_message'] = 'Login Successfully';
            $result['url'] = site_url('/dashboard');
       	}
		else
		{
			$result['success'] = false;
            $result['error_message'] = 'Username or Password is wrong,Please check';
		
		}
		echo json_encode($result);exit;
	}

	public function logout()
	{
		$this->session->unset_userdata('userRefId');
        $this->session->unset_userdata('firstName');
		if (!$this->session->userdata('logged_in_back')) 
		{
			$this->session->sess_destroy();
		}
        redirect(site_url());
	}
	
	
}
