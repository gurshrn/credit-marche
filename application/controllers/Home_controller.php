<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_controller extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('home');
        $this->home->updateExpiryStatus();
		$this->load->library('sms');
		updateStandByStatus();
	}
	

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		$output['parentUrl']= "Home";
        $output['title']    = "Home";
        $output['region']   = $this->home->getRegions();
        $output['ageRange']   = $this->home->getAgeRange();
        $output['operator']   = $this->home->getNetworkOperator();
		$output['creditNetwork'] = $this->home->getCreditNetwork();
		$output['getAllOffers'] = $this->home->getAllOffers(5);
		$output['purchaseOffer'] = $this->home->getPurOfferDetail();
		$this->load->view('frontend/home.php',$output);
	}
	public function viewOffer()
	{
		$output['limit'] = 5;
		$output['region']   = $this->home->getRegions();
		$output['ageRange']   = $this->home->getAgeRange();
		$output['operator']   = $this->home->getNetworkOperator();
		$output['creditNetwork'] = $this->home->getCreditNetwork();
		$output['purchaseOffer'] = $this->home->getPurOfferDetail();
		$output['count'] = count($this->home->getAllOffers());
		if ($this->input->is_ajax_request()) 
		{
			$output['getAllOffers'] = $this->home->getAllOffers($_POST['limit'],$_POST['start']);
			$this->load->view('frontend/home-offer-pagination.php',$output);
			
		}
		else
		{
			$output['parentUrl']= "View Offers";
			$output['title']    = "View Offers";
			$output['getAllOffers'] = $this->home->getAllOffers($output['limit'],0);
			$this->load->view('frontend/view-offers.php',$output);
		}
		
	}
	public function publishOffer($id = NULL)
	{
		$output['parentUrl']= "Publish Offers";
        $output['title']    = "Publish Offers";
        $output['region']   = $this->home->getRegions();
        $output['ageRange']   = $this->home->getAgeRange();
        $output['operator']   = $this->home->getNetworkOperator();
		$output['creditNetwork'] = $this->home->getCreditNetwork();
		if($id != NULL)
		{
			$output['offerdetail'] = $this->home->getofferdetail($id);
			$this->load->view('frontend/edit-publish-offer.php',$output);
		}
		else
		{
			$this->load->view('frontend/publish-offer.php',$output);
		}
	}
	public function checkEmailExist()
	{
		$results = $this->home->checEmail($_POST);
		if(count($results) > 0)
		{
			echo true;
		}
		else
		{
			echo false;
		}
		
	}
	public function publishCreditOffer()
	{
		$results = $this->home->createCreditOffer($_POST);
		if($results['success'])
		{
			$result['success'] = true;
			$result['type'] = $_POST['types'];
            $result['success_message'] = $results['success_message'];
        }
		else
		{
			$result['success'] = false;
            $result['error_message'] = $results['error_message'];
			$result['type'] = $_POST['types'];
        }
		echo json_encode($result);exit;
		
	}
	public function publishInternetOffer()
	{
		$results = $this->home->createInternetOffer($_POST);
		if($results['success'])
		{
			$result['success'] = true;
			$result['type'] = $_POST['types'];
            $result['success_message'] = $results['success_message'];
        }
		else
		{
			$result['success'] = false;
			$result['type'] = $_POST['types'];
            $result['error_message'] = $results['error_message'];
        }
		echo json_encode($result);exit;
		
	}
	public function paymentGateway()
	{
		$itemId = $_POST['offerid'];
		$item_price = $_POST['sellingPrice'];
		require_once APPPATH.'third_party/payexpress/payexpresse.php';
		
		$apiKey = '991b636b3a13a01f08323c952c2aacea4452a39310484d4c072711f33dae808f';
		
		$apiSecret = '4ec871e45642d95d253823241618b8fb90d9838952e66cd52b0150b0214efcbb';
		
		$jsonResponse = (new PayExpresse($apiKey, $apiSecret))->setQuery([
				'item_name' => 'hello',
				'item_price' => $item_price,
				//'item_price' => '200',
				'command_name' => "Payment via PayExpresse",
			])->setCustomeField([
				'item_id' => $itemId,
				//'item_id' => 'dfsa',
				'time_command' => time(),
				'ip_user' => $_SERVER['REMOTE_ADDR'],
				'lang' => $_SERVER['HTTP_ACCEPT_LANGUAGE']
			])
				->setTestMode(true)
				->setCurrency('XOF')
				->setRefCommand(uniqid())
				->setNotificationUrl([
					'ipn_url' => 'https://customer-devreview.com/creditmarche/payment-ipn', //only https
					'success_url' => 'https://customer-devreview.com/creditmarche/view-offer',
					'cancel_url' =>  'https://customer-devreview.com/creditmarche/view-offer'
				])->send();
		redirect($jsonResponse['redirect_url']);
	}
	public function getOfferDetailById()
	{
		$output['offers'] = $this->home->getofferdetail($_POST['id']);
		//$output['commission'] = $this->home->getCommissionDetail();
		$this->load->view('modal/offer-history.php',$output);
	}
	public function purchaseOffer()
	{
		$data['buyerrefId'] = $this->session->userdata('userRefId');
		$length = 7;
		$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
		if($data['buyerrefId'])
		{
			$data['transactionId'] = $randomString;
			$data['buyerTransactionType'] = 'Purchased';
			$data['buyerCm'] = $_POST['buyerCm'];
			$data['debit'] = $_POST['buyerDebit'];
			$data['offerId'] = $_POST['offerId'];
			$walletDetail = $this->home->getWalletDetail($data['buyerrefId']);
			$data['buyerWallet'] = $walletDetail->wallet_amount-$data['debit'];
			$buyer = getUserDetail($data['buyerrefId']);
		}
		if($_POST['sellerRefId'])
		{
			$data['transactionId'] = $randomString;
			$data['sellerTransactionType'] = 'OfferSold';
			$data['sellerCm'] = $_POST['sellerCm'];
			$data['credit'] = $_POST['sellerCredit'];
			$data['offerId'] = $_POST['offerId'];
			$data['sellerRefId'] = $_POST['sellerRefId'];
			$walletDetail = $this->home->getWalletDetail($data['sellerRefId']);
			$data['sellerWallet'] = $walletDetail->wallet_amount+$data['credit'];
			$seller = getUserDetail($_POST['sellerRefId']);
		}
		$result = $this->home->savePurchasePayment($data);
		if($result)
		{
			$senderemail = "gurjeevan942@gmail.com";
			$to = "gurjeevan.kaur@imarkinfotech.com";
			$subject = "My subject";
			$txt = "Hello world!";
			$headers = "From: Credit Marche <noreply@creditmarche.com>" . "\r\n";
			$headers .= "CC:".$senderemail. "\r\n";
			$headers .= "BCC:".$seller->email. "\r\n";
			

			mail($to,$subject,$txt,$headers);
			
			$sellerphoneNumber = $seller->country_code.''.$seller->tel_number;
			$smsMsg = $buyer->country_code.''.$buyer->tel_number;
			$this->sms->smssend($sellerphoneNumber,$smsMsg);
			
			
		}
		echo json_encode($result);exit;
		
	}
	public function searchOffers()
	{
		if($_POST['type'] == 'offer-published')
		{
			$output['userOffers'] = $this->home->getOffersDetailByKey($_POST);
			$this->load->view('frontend/offer-pagination.php',$output);
		}
		if($_POST['type'] == 'offer-purchased')
		{
			$output['offerPurchased'] = $this->home->getPurchaseOffersDetailByKey($_POST);
			$this->load->view('frontend/offer-purchased-pagination.php',$output);
		}
		if($_POST['type'] == 'offer-sold')
		{
			$output['offerSold'] = $this->home->getSoldOffersDetailByKey($_POST);
			$this->load->view('frontend/offer-sold-pagination.php',$output);
		}
		if($_POST['type'] == 'home-offers-search')
		{
			$output['getAllOffers'] = $this->home->getHomeOffersDetailByKey($_POST,5);
			$this->load->view('frontend/home-offer-pagination.php',$output);
		}
		if($_POST['type'] == 'view-offers-search')
		{
			$output['getAllOffers'] = $this->home->getHomeOffersDetailByKey($_POST);
			$this->load->view('frontend/home-offer-pagination.php',$output);
		}
		
	}
	public function contactForm()
	{
		$results = $this->home->addContactForm($_POST);
		if($results['success'])
		{
			$result['success'] = $results['success'];
			$result['type'] = 'contact form';
			$result['success_message'] = $results['success_message'];
        }
		else
		{
			$result['success'] = $results['success'];
			$result['error_message'] = $results['error_message'];
        }
		echo json_encode($result);exit;
	}
	public function getFooterDetail($id)
	{
		$output['parentUrl']= "";
        $output['title']    = "";
		$output['footer'] = $this->home->getFooterDetailById($id);
		$this->load->view('frontend/frontend-footer-detail.php',$output);
	}
			
	
	
}
