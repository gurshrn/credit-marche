<div class="modal fade login-reg" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></a>
                <ul class="nav nav-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-login-tab" data-toggle="pill" href="#pills-login" role="tab" aria-controls="pills-login" aria-selected="true">login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-register-tab" data-toggle="pill" href="#pills-register" role="tab" aria-controls="pills-register" aria-selected="false">register</a>
                    </li>
                </ul>
            </div>
            <div class="modal-body">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
                        <div class="login-form">
                            <form id="login-form" action="<?php echo site_url('/user-login');?>" method="POST">
                                <ul>
                                    <li>
                                        <label>Tel.Number<span>*</span></label>
										<input type="hidden" name="country_code" class="login-country-code">
                                        <input type="text" class="tel-numbers onlyvalidNumber"  placeholder="77 - 928 - 4589" name="tel_number">
                                    </li>
                                    <li>
                                        <label>Pin</label>
                                        <input type="password" placeholder="******" class="onlyvalidNumber" name="password" maxlength="6" minlength="6">
                                    </li>
                                </ul>
                                <input type="submit" class="disabledButton" value="login">
                                <input type="submit" value="pin reset">
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-register" role="tabpanel" aria-labelledby="pills-register-tab">
                        <div class="reg-form">
                            <form id="register-form" action="<?php echo site_url('/user-signup');?>" method="POST" autocomplete="off">
                                <ul>
                                    <li>
                                        <label>Civility</label>
                                        <div class="slct">
                                            <select name="civility">
                                                <option value="">Select Civility</option>
                                                <option value="Mr">Mr.</option>
                                                <option value="Mrs">Mrs.</option>
                                                <option value="Ms">Ms.</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li>
                                        <label>First Name<span>*</span></label>
                                        <input type="text" class="alphabets" placeholder="First Name" name="first_name">
                                    </li>
                                    <li>
                                        <label>last name</label>
                                        <input type="text" class="alphabets" placeholder="Last Name" name="last_name">
                                    </li>
                                    <li>
                                        <label>Tel.Number<span>*</span></label>
										<input type="hidden" name="country_code" class="register-country-code">
                                        <input type="text" class="tel-number onlyvalidNumber" placeholder="77 - 928 - 4589" name="tel_number">
                                    </li>
                                    <li>
                                        <label>Region of Residence</label>
                                        <div class="slct">
                                            <select name="residence">
                                                <option value="">Select region of residence...</option>
                                                <?php 
                                                    if(isset($region) && !empty($region)){
                                                        foreach($region as $value){
                                                ?>
                                                            <option value="<?php echo $value->id;?>"><?php echo $value->region;?></option>

                                                <?php } } ?>
                                            </select>
                                        </div>
                                    </li>
                                    <li>
                                        <label>Network Operator:</label>
                                        <div class="slct">
                                            <select name="network_operator">
                                                <option value="">Select network opertaor...</option>
                                                <?php 
                                                    if(isset($operator) && !empty($operator)){
                                                        foreach($operator as $values){
                                                ?>
                                                            <option value="<?php echo $values->id;?>"><?php echo $values->operator;?></option>

                                                <?php } } ?>
                                            </select>
                                        </div>
                                    </li>
                                    <li>
                                        <label>Age range</label>
                                        <div class="slct">
                                            <select name="age_range">
                                                <option value="">Select age range...</option>
                                                <?php 
                                                    if(isset($ageRange) && !empty($ageRange)){
                                                        foreach($ageRange as $val){
                                                ?>
                                                            <option value="<?php echo $val->id;?>"><?php echo $val->range;?></option>

                                                <?php } } ?>
                                            </select>
                                        </div>
                                    </li>
                                    <li>
                                        <label>Choose Access PIN (6 Digits):</label>
                                        <input type="password" class="onlyvalidNumber" placeholder="******" id="accessPin" name="access_pin" maxlength="6">
                                    </li>
                                    <li>
                                        <label>Confirm Access PIN:</label>
                                        <input type="password" class="onlyvalidNumber" name="confirm_access_pin" placeholder="******" maxlength="6">
                                    </li>
                                    <li>
                                        <label>Last 5 Digits of Nat ID or Passport:</label>
                                        <input type="text" placeholder="12345678" name="nat_id" maxlength="5">
                                    </li>
                                    <li>
                                        <label>Email Adress:</label>
                                        <input type="hidden" class="checkEmailValid">
                                        <input type="text" data-attr="credit_user_detail" class="email" placeholder="karimly@hotmail.com" name="email">
                                        <span clas="error" id="emailError"></span>
                                    </li>
                                </ul>
                                <div id="loadingspinner" >
                                <input type="submit" class="disabledButton" value="register">
                                <!--<input type="submit"><i class="fa fa-refresh fa-spin">Loading</i>-->
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>