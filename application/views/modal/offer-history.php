	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close">
					×
				</button>
				<h4 class="modal-title" id="classModalLabel">
					Fund Detail
				</h4>
			</div>
			<div class="modal-body offerHistory">
				<input type="hidden" class="offerId" value="<?php echo $offers->id;?>">
				<?php
					$userrefId = $this->session->userdata('userRefId');	
					$walletDetail = getWalletDetail($userrefId);
					if(!empty($walletDetail))
					{
						$walletAmount = $walletDetail[0]->wallet_amount;
					}
					else
					{
						$walletAmount = 0;
					}
					
					$commissiondetail = getCommissionDetail();
					if(!empty($commissiondetail))
					{
						$creditPerVal = $commissiondetail->credit_percent;
						$sellingPerVal = $commissiondetail->selling_percent;
						$commissionVal = $commissiondetail->commission;
						$commissionPaidSeller = $commissiondetail->commisioin_paid_seller;
					}
					else
					{
						$creditPerVal = 70;
						$sellingPerVal = 30;
						$commissionVal = 7;
						$commissionPaidSeller = 30;
					}
					
					if($offers->offer_type == 1)
					{
						$credits = $offers->credits;
					}
					else
					{
						$credits = $offers->internet_volume;
					}
					
					
					$sellingPrice = $offers->selling_price;
					
					$calCreditValue = $creditPerVal/100*$commissionVal/100;
					$calSellingVal = $sellingPerVal/100*$commissionVal/100;
					
					$creditVal = $credits*$calCreditValue;
					$sellingVal = $sellingPrice*$calSellingVal;
					
					
					
					$commissionVal = round(($creditVal+$sellingVal)/50)*50;
					
					$sellerCm = round(($commissionPaidSeller/100*$commissionVal)/10)*10;
					$buyerCm = $commissionVal-$sellerCm;
					
					$sellerCredit 	= $sellingPrice;
					$sellerRefId 	= $offers->userRefId;
					
					$buyerDebit = $sellingPrice;
					
					$buyerDebitCheck = $sellingPrice+$buyerCm+5;
	
				?>
				<input type="hidden" class="sellerCm" value="<?php echo $sellerCm; ?>">
				<input type="hidden" class="buyerCm" value="<?php echo $buyerCm; ?>">
				<input type="hidden" class="sellerCredit" value="<?php echo $sellerCredit; ?>">
				<input type="hidden" class="sellerRefId" value="<?php echo $sellerRefId; ?>">
				<input type="hidden" class="buyerDebit" value="<?php echo $buyerDebit; ?>">
				<table id="classTable" class="table table-bordered">
					  <thead>
					  </thead>
					  <tbody>
						<tr>
							<td>Offer Type</td>
							<?php if($offers->offer_type == 1) { ?>
								<td>Value Of Credits(F)</td>
							<?php } else{ ?>
								<td>Internet Volume(MB)</td>
							<?php } ?>
							
							<?php
								if($offers->offer_type == 1)
								{
									echo '<td>Promotion Type</td>';
								}
							?>
							<td>Selling Price(F)</td>
							<td>Sales Commission</td>
							<td>Total Fund Debit</td>
						</tr>
						<tr>
							<td>
								<?php if($offers->offer_type == 1){ echo 'PHONE CREDIT';} else{ echo 'INTERNET VOLUME';}?>
							</td>
							<td>
								<?php if($offers->offer_type == 1){ echo $offers->credits;} else{ echo $offers->internet_volume;}?>
							</td>
							
							<?php
								if($offers->offer_type == 1)
								{
									echo '<td>'.$offers->promotion_type.'%</td>';
								}
							?>
							<td><?php echo $offers->selling_price;?></td>
							<td><?php echo $buyerCm;?></td>
							<td><?php echo $buyerDebit+$buyerCm;?></td>
						</tr>
					  </tbody>
					</table>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-danger close">Close</button>
									<?php if($walletAmount >= $buyerDebitCheck) { ?>
											<button type="button" class="btn btn-primary purchaseOffers">Purchase</button>
									<?php } else { ?>
											<a href="<?php echo site_url('dashboard/'.$offers->id.'/'.$buyerDebitCheck);?>"><button class="btn buy-primary">Purchase</button></a>
									<?php } ?>
								</div>
							</div>
</div>


	
			
