<div class="modal fade" id="otpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="javascript:void(0)">OTP</a>
                </li>
                
            </ul>
      </div>
      <div class="modal-body">
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
            <div class="login-form">
              <form id="check-otp" action="<?php echo site_url('/check-otp');?>" method="POST" autocomplete="off">
                <ul>
                    <li>
                        <label>OTP<span>*</span></label>
                        <input type="hidden" id="userRefId" name="userRefId">
                        <input type="text" placeholder="12345" class="onlyvalidNumber" name="otp" minlength="5" maxlength="5">
                    </li>
                   
                </ul>
                
              
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary disabledButtons">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>