	<?php 
		$expiry_time = date('Y-m-d H:i:s', strtotime($offers->expiry_date));
		$expiry_date = date('Y-m-d H:i:s',strtotime('-6 hour',strtotime($expiry_time)));
		$currentdatetime = date('Y-m-d H:i:s');																										
		$now = date('Y-m-d');														
		$your_date = date('Y-m-d',strtotime($offers->expiry_date));	
		if($your_date >= $now)
		{
			$datetime1 = new DateTime($now);

			$datetime2 = new DateTime($your_date);

			$difference = $datetime1->diff($datetime2);

			$expiryday = $difference->d.' days';
		}
		else
		{
			if($currentdatetime >= $expiry_date)
			{
				$expiryday = 'Expired';
			}
		}	
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="form-group col-md-6">
				<label for="recipient-name" class="col-form-label"><?php if($offers->offer_type == 1){ echo 'CREDIT(F) : '.$offers->credits;} else{ echo 'INTERNET VOLUME (MB) : '.$offers->internet_volume;}?> </label> 
			</div>
			<div class="form-group col-md-6">
				<label for="recipient-name" class="col-form-label">EXPIRATION DATE : <?php echo $offers->expiry_date;?></label>
			</div>
			
		</div>
		<div class="row">
			<div class="form-group col-md-6">
				<label for="recipient-name" class="col-form-label">SELLING PRICE(F) : <?php echo $offers->selling_price;?></label>
			</div>
			<div class="form-group col-md-6">
				<label for="recipient-name" class="col-form-label"><?php if($offers->offer_type == 1){ echo 'PROMOTION TYPE : '.$offers->promotion_type.'%';} else{ echo 'RATIO (F/MB) : '.number_format($offers->selling_price/$offers->internet_volume,2).'%';}?> </label>
			</div>
			
		</div>
		<div class="row">
			<div class="form-group col-md-6">
				<label for="recipient-name" class="col-form-label">DAY OF EXPIRATION : <?php echo $expiryday;?></label> 
			</div>
			<div class="form-group col-md-6">
				<label for="recipient-name" class="col-form-label">DATE OF PUBLICATION : <?php echo date('m/d/Y',strtotime($offers->addedondate));?></label> 
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-6">
				<label for="recipient-name" class="col-form-label">OFFER TYPE : <?php if($offers->offer_type == 1){ echo 'PHONE CREDIT';}else{ echo 'INTERNET VOLUME';}?></label> 
			</div>
			<div class="form-group col-md-12">
				<label for="recipient-name" class="col-form-label"><?php if($offers->offer_type == 1){ echo 'BONUS CREDITS VALID ON NETWORK : '.$offers->network; }?></label> 
			</div>
			
		</div>
		<div class="row">
			<div class="form-group col-md-6">
				<label for="recipient-name" class="col-form-label">First Name : <?php echo $offers->first_name;?></label> 
			</div>
			<div class="form-group col-md-12">
				<label for="recipient-name" class="col-form-label">Last Name : <?php echo $offers->last_name;?></label> 
			</div>
			<div class="form-group col-md-12">
				<label for="recipient-name" class="col-form-label">Tel Number : <?php echo $offers->tel_number;?></label> 
			</div>
			
		</div>
		
		
	</div>
