<footer>
	<?php $getFooterList = getFooterList();  ?>
    <div class="container">
        <div class="wrap">
            <div class="footer-nav">
                <div class="row">
                    <div class="col-md-6">
                        <div class="foot-block">
                            <h3>Quick Links</h3>
                            <ul>
								<?php 
									if(!empty($getFooterList))
									{ 
										foreach($getFooterList as $val){
								?>
											<li><a href="<?php echo site_url('footer-detail/'.$val->id);?>"><?php echo $val->title;?></a></li>
								
								<?php } } ?>
								
								<li><a href="<?php echo site_url('faq-detail');?>">FAQ</a></li>
								<li><a href="<?php echo site_url('blog-detail');?>">Blog</a></li>
                                
                             </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="foot-block">
                            <h3>contact us</h3>
                            <div class="contct-info">
                                <div class="icon">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </div>
                                <div class="dtl">
                                    <p>
                                        <span>Street Address</span>123 lorem ipsu, South Africa
                                    </p>
                                </div>
                            </div>
                            <div class="contct-info">
                                <div class="icon">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                </div>
                                <div class="dtl">
                                    <p>
                                        <span>Fax phone Number</span>123 456 7899
                                    </p>
                                </div>
                            </div>
                            <div class="contct-info">
                                <div class="icon">
                                    <i class="fa fa-mobile" aria-hidden="true"></i>
                                </div>
                                <div class="dtl">
                                    <p>
                                        <span>Mobile Number</span><a href="tel:1234567899">123 456 7899</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-nav">
                <div class="get-in-touch">
                    <h2>get in touch</h2>
                    <form id="footerForm" method="post" action="<?php echo site_url('contact-form');?>">
                        <div class="form-group">
                            <input class="form-control alphabets" name="first_name" type="text" placeholder="First Name">
                        </div>
                        <div class="form-group">
                            <input class="form-control alphabets" name="last_name" type="text" placeholder="Last Name">
                        </div>
                        <div class="form-group">
                            <input class="form-control" name="email" type="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input class="form-control onlyvalidNumber" name="phone" type="text" placeholder="Phone">
                        </div>
                        <div class="form-group txt">
                            <textarea class="form-control" name="message" placeholder="Message"></textarea>
                        </div>
                        <input class="sbmt-btn" type="submit" value="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap">
        <div class="footer-nav">
            <div class="copy-right-sec">
                <p>Copyright © 2018 CreditMarche. Powered by: <a href="https://www.imarkinfotech.com/" target="_blank">iMarkInfotech</a></p>
            </div>
        </div>
    </div>
</footer>