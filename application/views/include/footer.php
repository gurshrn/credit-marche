	<script src="<?php echo site_url(); ?>assets/js/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
	<script src="<?php echo site_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/form-validation-script.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/jquery.mask.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/intlTelInput.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/wow.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/custom.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/home.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/common.js"></script>    
	<script src="<?php echo site_url(); ?>assets/js/dashboard.js"></script>
	<script src="<?php echo site_url(); ?>assets/js/admin.js"></script>
	<script src="<?php echo site_url(); ?>assets/js/starrr.js"></script>
</body>