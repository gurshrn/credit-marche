<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>
	<div id="chatdetail"></div>
	<div class="content-wrapper" id="notificationDetail">
		<section class="content-header">
			<div class="col-md-5">
				<h3>
					Chat Messages Detail
				</h3>
				
			</div>
		</section>
		
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
									  <th>Message From</th>
									  <th>First Name</th>
									  <th>Action</th>
									</tr>
								</thead>
								<tbody id="allResult">
								<?php 
									if(isset($chat) && !empty($chat)) {
										foreach($chat as $value){
											$userDetail = getUserDetail($value->msg_from);
								?>
										<tr>
											<td><?php echo $userDetail->country_code.''.$userDetail->tel_number;?></td>
											<td><?php echo ucfirst($userDetail->first_name);?></td>
											<td><a href="javascript:void(0)" class="view-chat" data-id="<?php echo $value->msg_from; ?>"><i class="fa fa-eye"></i></a></td>
										</tr>
											
											
									<?php } }  else{ ?>
									<tr>
										<td colspan="3">No New Message found...</td>
									</tr>

								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	

<?php $this->load->view('backend/include/footer.php');?>
<script src="<?php echo site_url(); ?>assets/js/dashboard.js"></script>


