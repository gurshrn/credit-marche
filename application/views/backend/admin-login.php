<?php $this->load->view('backend/include/header.php');?>

	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href="javascript:void(0)"><b>Credit Marche</a>
			</div>
			<div class="login-box-body">
				<p class="login-box-msg">Sign in to start your session</p>

				<form action="<?php echo site_url('/admin/login');?>" id="admin-login" method="post" autocomplete="off">
					<div class="form-group has-feedback">
						<input type="text" class="form-control" name="tel_number" placeholder="Tel Number">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" name="password" class="form-control" placeholder="Password" maxlength="6">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-8">
							<div class="checkbox icheck">
						
							</div>
						</div>
						<div class="col-xs-4">
						  <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
						</div>
					</div>
				</form>

				<a href="#">I forgot my password</a><br>
			</div>

		</div>
		
<?php $this->load->view('include/footer.php');?>

