<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>
	<div class="content-wrapper">
		<section class="content-header">
			<div class="pull-left">
				<h1>
					Offers
				</h1>
			</div>
			<div class="pull-right form-group clearfix">
				<input type="text" class="form-control search-offers" placeholder="Search...">
				<select class="form_control offer-status" name="offerStatus">
					<option value="">Select...</option>
					<option value="1" data-type="status">Enabled</option>
					<option value="2" data-type="status">Disabled</option>
					<option value="3" data-type="status">Archieved</option>
					<option value="4" data-type="status">Excecuted</option>
				</select>
				<select class="form_control offer-type" name="offerType">
					<option value="">Select...</option>
					<option value="1" data-type="offer-type">Phone Credit</option>
					<option value="2" data-type="offer-type">Internet Volume</option>
				</select>
			</div>
		</section>
		
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
									  <th>Credits(F) Or Internet volume(MB)</th>
									  <th>Expiration date</th>
									  <th>Selling price</th>
									  <th>Date of publication</th>
									  <th>Days to expiration</th>
									  <th>Offer type</th>
									  <th>Published Tel Number</th>
									  <th>Status</th>
									  <th>Action</th>
									</tr>
								</thead>
								<tbody id="allResult">
								<?php 
									if(isset($offers) && !empty($offers)) {
										foreach($offers as $value){
											$expiry_time = date('Y-m-d H:i:s', strtotime($value->expiry_date));
											$expiry_date = date('Y-m-d H:i:s',strtotime('-6 hour',strtotime($expiry_time)));
											$currentdatetime = date('Y-m-d H:i:s');	
								?>
											<tr>
												<td><?php if($value->offer_type == 1){ echo $value->credits.'(F)';}else{ echo $value->internet_volume.'(MB)';}?></td>
												<td>
													<?php															
														$expirydate= strtotime($value->expiry_date);															
														echo date('d M Y', $expirydate);														
													?>	
												</td>
												<td><?php echo $value->selling_price;?></td>
												<td>
													<?php
														$addedondate= strtotime($value->addedondate);															
														echo date('d M Y', $addedondate);
													?>
												</td>
												<td>
													<?php 
														$now = date('Y-m-d');														
														$your_date = date('Y-m-d',strtotime($value->expiry_date));	
														if($your_date >= $now)
														{
															$datetime1 = new DateTime($now);

															$datetime2 = new DateTime($your_date);

															$difference = $datetime1->diff($datetime2);

															echo $difference->d.' days';
														}
														else
														{
															if($currentdatetime >= $expiry_date)
															{
																echo 'Expired';
															}
														}	
													?>	
												</td>
												<td>
													<?php 															
														if($value->offer_type == 1) { echo 'PHONE CREDIT';} 															
														if($value->offer_type == 2) { echo 'INTERNET VOLUME';} 														
													?>	
												</td>
												<td><?php echo $value->tel_number;?></td>
												<td>
													<?php 
														if($value->status == 3)
														{ 
															echo '<a href="javascript:void(0)" class="btn buy-btn red-btn">Archived</a>';
														}
														if($value->status == 1) 
														{ 
															echo '<a class="btn buy-btn enable-btn offerStatus disable'.$value->id.'" data-attr="2" data-id="'.$value->id.'">Enabled</a>';
														}
														if($value->status == 2)
														{
															echo '<a class="btn buy-btn disable-btn offerStatus enable'.$value->id.'" data-attr="1" data-id="'.$value->id.'">Disabled</a>';
														}
														if($value->status == 4)
														{
															echo '<a href="javascript:void(0)" class="btn btn-success">Excecuted</a>';
														}
														if($value->status == 5)
														{
															echo '<a href="javascript:void(0)" class="btn btn-warning standByStatus standBy'.$value->id.'" data-attr="1" data-id="'.$value->id.'">StandBy</a>';
														}
													?>
												</td>
												<td>
													<a href="javascript:void(0)" class="view-offers" data-id="<?php echo $value->id; ?>"><i class="fa fa-eye"></i></a>
													<?php if($value->status != 4) { ?>
														<a href="<?php echo site_url('/admin/admin-edit-offers/'.$value->id);?>" class="edit-offer" data-id="<?php echo $value->id; ?>"><i class="fa fa-edit"></i></a>
													<?php } ?>
												</td>
											</tr>
								<?php } } else{ ?>
									<tr>
										<td colspan="8">No matching record found...</td>
									</tr>

								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="control-sidebar-bg"></div>
	<div class="modal fade" id="viewOffers" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title">Offer Detail</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body get-offer-detail">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

<!-- View Offer Modal -->



<!-- Modal2 -->

<?php $this->load->view('modal/modal.php');?>

<?php $this->load->view('backend/include/footer.php');?>
<script src="<?php echo site_url(); ?>assets/js/dashboard.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>



