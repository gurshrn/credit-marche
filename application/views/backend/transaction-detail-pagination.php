<?php 
	if(isset($transaction) && !empty($transaction)) {
		foreach($transaction as $value){
?>
			<tr>
			
				<td><?php echo ucfirst($value->first_name).' '.$value->last_name;?></td>
				<td><?php echo $value->transaction_type;?></td>
				<td>
					<?php 
						if($value->transaction_type == 'Sales Commission'){ echo 'Debit';}
						if($value->transaction_type == 'OfferSold'){ echo 'Credit';}
						if($value->transaction_type == 'Purchased'){ echo 'Debit';}
					?>
				</td>
				<td>
					<?php 
						if($value->transaction_type == 'Sales Commission'){ echo $value->debit;}
						if($value->transaction_type == 'OfferSold'){ echo $value->credit;}
						if($value->transaction_type == 'Purchased'){ echo $value->debit;}
					?>
				</td>
				<td><?php echo $value->running_balance;?></td>
				<td>
					<?php									
						$date= strtotime($value->date);		
						echo date('d M Y', $date);				
					?>	
				</td>
				<td><?php echo $value->time;?></td>
				<td><?php echo $value->transaction_id;?></td>
			</tr>
			
	<?php } } else{ ?>
	<tr>
		<td colspan="8">No transaction found...</td>
	</tr>

<?php } ?>