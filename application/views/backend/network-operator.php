<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>
	<div class="content-wrapper">
		<section class="content-header">
			<div class="pull-left">
				<h1>
					Network Operator
				</h1>
			</div>
			<div class="pull-right">
				<button type="button" class="btn btn-primary addCredit common-add">Add Network Operator</button>
			</div>
		</section>
		
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
									  <th>Sr.no</th>
									  <th>Operator</th>
									  <th>Action</th>
									</tr>
								</thead>
								<tbody id="allResult">
								<?php 
									if(isset($network) && !empty($network)) {
										foreach($network as $value){
								?>
											<tr>
												<td><?php echo $value->id;?></td>
												<td><?php echo ucfirst($value->operator);?></td>
												<td>
													<a href="javascript:void(0)" class="editInformation" data-target="network_operator" data-rel="edit-credit-network" data-id="<?php echo $value->id; ?>"><i class="fa fa-edit"></i></a>
													<!--a href="javascript:void(0)" class="delete-credits" data-target="credit_networks" data-id="<?php echo $value->id; ?>"><i class="fa fa-trash"></i></a-->
												</td>
											</tr>
								<?php } } else{ ?>
									<tr>
										<td colspan="2">No matching record found...</td>
									</tr>

								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="control-sidebar-bg"></div>
	<div class="modal fade" id="commonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title"><span id="modal-title"></span> Network Operator</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body get-offer-detail">
					<form id="add-network-operator" method="post" action="<?php echo site_url('admin/add-network-operator');?>">
						<input type="hidden" name="id" class="inputId">
						<div class="container-fluid">
							<div class="row">
								<div class="form-group col-md-6">
									<label for="recipient-name" class="col-form-label">Network Operator</label> 
									<input type="text" name="operator" class="form-control operator" placeholder="Operator">
								</div>
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-value"></button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
		</div>
	</div>

<!-- View Offer Modal -->



<!-- Modal2 -->

<?php $this->load->view('modal/delete-modal.php');?>

<?php $this->load->view('backend/include/footer.php');?>
<script src="<?php echo site_url(); ?>assets/js/dashboard.js"></script>
<script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>



