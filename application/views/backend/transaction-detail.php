<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

	<div class="content-wrapper">
		<section class="content-header">
			<div class="col-md-5">
				<h3>
					Transaction Detail
				</h3>
				
			</div>
			<div class="col-md-2 pull-right form-group clearfix">
				
				<button type="button" class="btn btn-primary searchTransactionDetail">Search</button>
				
			 </div>
			 <div class="col-md-2 pull-right form-group clearfix">
				
				<input type="text" class="form-control transactiondate" id="datepicker1" placeholder="search transaction date...">
				
			 </div>
			 <div class="col-md-2 pull-right form-group clearfix">
				
				<input type="text" class="form-control keyword"  placeholder="search...">
				
			 </div>
			 <div class="col-md-1 pull-right form-group clearfix">
				
				<select class="form_control payment-type">
					<option value="">Select</option>
					<option value="Purchased">Purchased</option>
					<option value="Sales Commission">Sales Commission</option>
					<option value="OfferSold">Offer Sold</option>
				</select>
				
			 </div>
			 
		</section>
		
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
									  <th>Username</th>
									  <th>Transaction Type</th>
									  <th>Payment Type</th>
									  <th>Amount</th>
									  <th>Running Balance</th>
									  <th>Date</th>
									  <th>Time</th>
									  <th>Transaction-ID</th>
									</tr>
								</thead>
								<tbody id="allResult">
								<?php 
									if(isset($transaction) && !empty($transaction)) {
										foreach($transaction as $value){
								?>
											<tr>
											
												<td><?php echo ucfirst($value->first_name).' '.$value->last_name;?></td>
												<td><?php echo $value->transaction_type;?></td>
												<td>
													<?php 
														if($value->transaction_type == 'Sales Commission'){ echo 'Debit';}
														if($value->transaction_type == 'OfferSold'){ echo 'Credit';}
														if($value->transaction_type == 'Purchased'){ echo 'Debit';}
													?>
												</td>
												<td>
													<?php 
														if($value->transaction_type == 'Sales Commission'){ echo $value->debit;}
														if($value->transaction_type == 'OfferSold'){ echo $value->credit;}
														if($value->transaction_type == 'Purchased'){ echo $value->debit;}
													?>
												</td>
												<td><?php echo $value->running_balance;?></td>
												<td>
													<?php									
														$date= strtotime($value->date);		
														echo date('d M Y', $date);				
													?>	
												</td>
												<td><?php echo $value->time;?></td>
												<td><?php echo $value->transaction_id;?></td>
											</tr>
											
									<?php } } else{ ?>
									<tr>
										<td colspan="8">No transaction found...</td>
									</tr>

								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="control-sidebar-bg"></div>
	<div class="modal fade" id="viewOffers" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title">Offer Detail</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body get-offer-detail">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

<!-- View Offer Modal -->



<!-- Modal2 -->

<?php $this->load->view('modal/modal.php');?>

<?php $this->load->view('backend/include/footer.php');?>
<script src="<?php echo site_url(); ?>assets/js/dashboard.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>


