<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>
	<div class="content-wrapper">
		<section class="content-header">
			<div class="pull-left">
				<h1>
					Commission
				</h1>
			</div>
		</section>
		
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
									  <th>Commission</th>
									  <th>Credit Percent Value</th>
									  <th>Selling Percent Value</th>
									  <th>Commission Paid Seller</th>
									  <th>Commission Paid Buyer</th>
									  <th>Action</th>
									</tr>
								</thead>
								<tbody id="allResult">
								<?php 
									if(isset($commission) && !empty($commission)) {
										foreach($commission as $value){
								?>
											<tr>
												<td><?php echo $value->commission;?></td>
												<td><?php echo $value->credit_percent;?></td>
												<td><?php echo $value->selling_percent;?></td>
												<td><?php echo $value->commisioin_paid_seller;?></td>
												<td><?php echo $value->commisioin_paid_buyer;?></td>
												<td>
													<a href="javascript:void(0)" class="editInformation" data-target="credit_commission" data-rel="edit-credit-commission" data-id="<?php echo $value->id; ?>"><i class="fa fa-edit"></i></a>
												</td>
											</tr>
								<?php } } else{ ?>
									<tr>
										<td colspan="6">No matching record found...</td>
									</tr>

								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="control-sidebar-bg"></div>
	<div class="modal fade" id="commonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title"><span id="modal-title"></span> Credit Commission</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body get-offer-detail">
					<form id="add-credit-commission" method="post" action="<?php echo site_url('admin/add-credit-commission');?>">
						<input type="hidden" name="id" class="inputId">
						<div class="container-fluid">
							<div class="row">
								
								<div class="form-group col-md-6">
									<label for="recipient-name" class="col-form-label">Commission</label> 
									<input type="number" name="commission" class="form-control commission" placeholder="Commission" min="1" max="30">
								</div>
								
								<div class="form-group col-md-6">
									<label for="recipient-name" class="col-form-label">Credit Percent Value</label> 
									<input type="number" name="credit_percent" class="form-control creditValue" placeholder="Credit Percent Value" min="1" max="99">
								</div>
								
								<div class="form-group col-md-6">
									<label for="recipient-name" class="col-form-label">Selling Percent Value</label> 
									<input type="number" name="selling_percent" class="form-control sellingValue" min="1" max="99" placeholder="Selling Percent Value" readonly>
								</div>
								
								<div class="form-group col-md-6">
									<label for="recipient-name" class="col-form-label">Commission Paid Seller</label> 
									<input type="number" name="commisioin_paid_seller" class="form-control paidSeller" min="1" max="99" placeholder="Commission Paid Seller">
								</div>
								
								<div class="form-group col-md-6">
									<label for="recipient-name" class="col-form-label">Commission Paid Buyer</label> 
									<input type="number" name="commisioin_paid_buyer" class="form-control paidBuyer" min="1" max="99" placeholder="Commission Paid Buyer" readonly>
								</div>
								
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-value"></button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
		</div>
	</div>

<!-- View Offer Modal -->



<!-- Modal2 -->

<?php $this->load->view('modal/delete-modal.php');?>

<?php $this->load->view('backend/include/footer.php');?>
<script src="<?php echo site_url(); ?>assets/js/dashboard.js"></script>


