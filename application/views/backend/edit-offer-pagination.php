<?php 
									if(isset($offers) && !empty($offers)) {
										foreach($offers as $value){
											$expiry_time = date('Y-m-d H:i:s', strtotime($value->expiry_date));
											$expiry_date = date('Y-m-d H:i:s',strtotime('-6 hour',strtotime($expiry_time)));
											$currentdatetime = date('Y-m-d H:i:s');	
								?>
											<tr>
												<td><?php if($value->offer_type == 1){ echo $value->credits.'(F)';}else{ echo $value->internet_volume.'(MB)';}?></td>
												<td>
													<?php															
														$expirydate= strtotime($value->expiry_date);															
														echo date('d M Y', $expirydate);														
													?>	
												</td>
												<td><?php echo $value->selling_price;?></td>
												<td>
													<?php
														$addedondate= strtotime($value->addedondate);															
														echo date('d M Y', $addedondate);
													?>
												</td>
												<td>
													<?php 
														$now = date('Y-m-d');														
														$your_date = date('Y-m-d',strtotime($value->expiry_date));	
														if($your_date >= $now)
														{
															$datetime1 = new DateTime($now);

															$datetime2 = new DateTime($your_date);

															$difference = $datetime1->diff($datetime2);

															echo $difference->d.' days';
														}
														else
														{
															if($currentdatetime >= $expiry_date)
															{
																echo 'Expired';
															}
														}	
													?>	
												</td>
												<td>
													<?php 															
														if($value->offer_type == 1) { echo 'PHONE CREDIT';} 															
														if($value->offer_type == 2) { echo 'INTERNET VOLUME';} 														
													?>	
												</td>
												<td><?php echo $value->tel_number;?></td>
												<td>
													<?php 
														if($value->status == 3)
														{ 
															echo '<a href="javascript:void(0)" class="btn buy-btn red-btn">Archived</a>';
														}
														if($value->status == 1) 
														{ 
															echo '<a class="btn buy-btn enable-btn offerStatus disable'.$value->id.'" data-attr="2" data-id="'.$value->id.'">Enabled</a>';
														}
														if($value->status == 2)
														{
															echo '<a class="btn buy-btn disable-btn offerStatus enable'.$value->id.'" data-attr="1" data-id="'.$value->id.'">Disabled</a>';
														}
														if($value->status == 4)
														{
															echo '<a href="javascript:void(0)" class="btn btn-success">Excecuted</a>';
														}
														if($value->status == 5)
														{
															echo '<a href="javascript:void(0)" class="btn btn-warning standByStatus standBy'.$value->id.'" data-attr="1" data-id="'.$value->id.'">StandBy</a>';
														}
													?>
												</td>
												<td>
													<a href="javascript:void(0)" class="view-offers" data-id="<?php echo $value->id; ?>"><i class="fa fa-eye"></i></a>
													<?php if($value->status != 4) { ?>
														<a href="<?php echo site_url('/admin/admin-edit-offers/'.$value->id);?>" class="edit-offer" data-id="<?php echo $value->id; ?>"><i class="fa fa-edit"></i></a>
													<?php } ?>
												</td>
											</tr>
								<?php } } else{ ?>
									<tr>
										<td colspan="8">No matching record found...</td>
									</tr>

								<?php } ?>