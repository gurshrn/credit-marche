<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>
	<div class="content-wrapper">
		<section class="content-header">
			<div class="col-md-5">
				<h3>
					Commission by User
				</h3>
				
			</div>
		</section>
		
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
									  <th>UserRefId</th>
									  <th>Username</th>
									  <th>Commission</th>
									  <th>Action</th>
									</tr>
								</thead>
								<tbody id="allResult">
								<?php 
									if(isset($commission) && !empty($commission)) {
										foreach($commission as $value){
								?>
											<tr>
												<td><?php echo $value->userRefId;?></td>
												<td><?php echo ucfirst($value->first_name).' '.$value->last_name;?></td>
												<td><?php echo $value->totalCommission;?></td>
												<td>
													<a href="<?php echo site_url('admin/transaction-history/'.$value->userRefId);?>"><i class="fa fa-eye"></i></a>
												</td>
											</tr>
											
								<?php } } else{ ?>
									<tr>
										<td colspan="4">No matching record found...</td>
									</tr>

								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="control-sidebar-bg"></div>
	<div class="modal fade" id="viewOffers" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title">Offer Detail</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body get-offer-detail">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

<!-- View Offer Modal -->



<!-- Modal2 -->

<?php $this->load->view('modal/modal.php');?>

<?php $this->load->view('backend/include/footer.php');?>
<script src="<?php echo site_url(); ?>assets/js/dashboard.js"></script>
<script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>



