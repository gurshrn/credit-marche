<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

	<div class="content-wrapper">
		<section class="content-header productTitle">
			<h1>Update Offer Detail</h1>
		</section>
		
		

		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
						<?php //echo "<pre>"; print_r($offers);?>
							
							<form id="<?php if($offers->offer_type == 1){ echo 'publish-credit-offer';} else{ echo 'internet-offer';}?>" method="POST" action="<?php if($offers->offer_type == 1){ echo site_url('/publish-credit-offer');} else{ echo site_url('/publish-internet-offer');}?>" autocomplete="off">
								
								<input type="hidden" name="id" value="<?php echo $offers->id;?>">
								<input type="hidden" name="types" value="<?php echo 'backend';?>">
								
								<div class="row">
									<div class="col-md-4">
										<div class="form-group clearfix">
											<label for="exampleInputPassword1"><?php if($offers->offer_type == 1){ echo 'Value Of Credits (F):';}else{ echo 'Internet volume:';}?></label>
									  
											<input type="text" class="form-control validNumber <?php if($offers->offer_type == 1){ echo 'calculateVal creditVal';}?>" placeholder="<?php if($offers->offer_type == 1){ echo 'Value of credits';}else{ echo 'Enter the internet volume';}?>" name="<?php if($offers->offer_type == 1){ echo 'credits';}else{ echo 'internet_volume';}?>" value="<?php if($offers->offer_type == 1){ echo $offers->credits;}else{ echo $offers->internet_volume;}?>">
									
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group clearfix">
											<label for="exampleInputPassword1">Bonus Expiration Date:</label>
									  
											<input type="text" id="datepicker1" class="form-control" placeholder="Enter Date" name="<?php if($offers->offer_type == 1){ echo 'expiry_date'; } else{ echo 'network_expiry_date';}?>" value="<?php echo $offers->expiry_date;?>" readonly>
											
										</div>
									</div>
								<div class="col-md-4">
									<div class="form-group clearfix">
										<label for="exampleInputPassword1">Selling Price (F):</label>
								  
										<input type="text" class="form-control validNumber calculateVal sellingPrice" placeholder="Enter Value" name="<?php if($offers->offer_type == 1){ echo 'selling_price'; } else{ echo 'network_selling_price';}?>" value="<?php echo $offers->selling_price;?>">
										<label class="error sellingPriceError"></label>
										
									</div>
								</div>
							</div>
							<?php if($offers->offer_type == 1){?>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group clearfix">
											
											<label for="exampleInputPassword1">Promotion Type:</label>
											<input type="text" id="datepicker" class="form-control promotionType" placeholder="Calculated" name="promotion_type" value="<?php echo $offers->promotion_type;?>" readonly>
										
										</div>
									</div>
									
									
									 
									<div class="col-md-4">
										<div class="form-group clearfix">
										  <label>Bonus Crédits Valid On Network:</label>
											<select class="form-control" name="credit_network">
												<option value="">Select....</option>
												<?php 
													if(isset($creditNetwork) && !empty($creditNetwork)){
													foreach($creditNetwork as $val){
														if(!empty($offers->network_id) && $val->id == $offers->network_id)
														{
															$sel = 'selected="selected"';
														}
														else
														{
															$sel = '';
														}
												?>
													
													<option value="<?php echo $val->id?>" <?php echo $sel;?>><?php echo $val->network;?></option>
											  
												
												<?php } }?>
											</select>
										</div>
									</div>
								</div>
							<?php } ?>
							<div class="btn-group pull-right row">
							   <div class="col-md-3"><button class="btn btn-primary <?php if($offers->offer_type == 1){ echo 'validate-offer';} else{ echo 'internet-validate-offer';}?>" type="button">Validate Offer</button></div>
							   <div class="col-md-2"></div>
							   <div class="col-md-3"><button class="btn btn-primary <?php if($offers->offer_type == 1){ echo 'publish-credit-offer';} else{ echo 'internet-publish-offer';}?>" type="button" disabled>Publish Offer</button></div>
							</div>
						</form>
					
					</div>
					
	            	
	        	</div>
	    	</div>
		</div>
	</section>
</div>
	
<?php $this->load->view('include/footer.php');?>
<script>
jQuery("#datepicker1").datepicker({

            minDate: "+3",

            dateFormat: 'mm/dd/yy'

        });
</script>








