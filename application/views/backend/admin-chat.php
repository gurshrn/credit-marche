<?php $this->load->view('backend/include/header.php');?>

	<div class="content-wrapper">
		<section class="content-header">
			<div class="col-md-5">
				<h3>
					Chat Online
				</h3>
				
			</div>
		</section>
		
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<table id="example2" class="table table-bordered table-hover">
								<section class="home_main_wrap">
									<div class="home_wrap_mid_cntnt">
										<div class="message-chat-block">
											<div class="users-detail-block">
											   

											</div>
											<div class="clr"></div>
											<div class="overall-chat-block individual-chat">
												<?php if(isset($chatHistory) && !empty($chatHistory)) {
														foreach($chatHistory as $val){?>
												<div class="<?php if($val->msg_from != 'Admin'){ echo 'user-msg-block'; }else{ echo 'your-msg-block'; } ?>">
													<?php 
														if($val->msg_from != 'Admin'){ 
															if($val->msg_from!='Admin')
															{
																$user = getUserDetail($val->msg_from);
																$username = ucwords($user->first_name);
															}
															else
															{
																$username = 'Admin';
															} 
													?>
														<span class="username"><?php echo $username;?></span>
													
													<?php } ?>
													
													<div class="user-msg">
														<p>
															<span><?php echo $val->message; ?><time class="msgtime"><?php echo $val->msg_date.' '.$val->msg_time;?></time></span>
														</p>
													</div>
													<?php if($val->msg_from == 'Admin'){ 
															if($val->msg_from!='Admin')
															{
																$user = getUserDetail($val->msg_from);
																$username = ucwords($user->first_name);
															}
															else
															{
																$username = 'Admin';
															} 
													
													?>
														<span class="username"><?php echo $username;?></span>
													<?php } ?>
												</div>
												<?php } } ?>
												
											</div>
										</div>
										<div class="type-section">
											<form method="post" id="chat_forms">
												<div class="enter-txt-msg">
													<input type="text" name="txtmsg" id="txtmsg" class="contactBtnQuote" placeholder="Add your text. . .">
													<input type="hidden" name="msgfrom" id="msgfrom" value="<?php echo 'Admin'; ?>">
													<input type="hidden" name="msgto" id="msgto" value="<?php echo $userRefId; ?>">
												</div>
											</form>
											
										</div>
									</div>

								</section>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="clearfix"></div>
<script>
	$('body').on('change','#txtmsg', function(e){
		var msgfrom =$('#msgfrom').val();
		var msgto =$('#msgto').val();
		var txtmsg =$('#txtmsg').val();
		$.ajax({
			type: "POST",
			url: site_url + "/send-chat-message",
			data: {msgfrom:msgfrom,msgto:msgto,txtmsg:txtmsg},
			success: function(data){
				$('.overlay').hide();
						
				if(data.success)
				{
					$(".individual-chat").load(" .individual-chat");
				}
				$('#txtmsg').val('');
				$('.individual-chat').append(data);
				$('.individual-chat').scrollTop($('.individual-chat')[0].scrollHeight);
			}
		});
		e.preventDefault();
		return false;
	});

</script>
<script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>



