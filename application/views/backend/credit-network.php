<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>
	<div class="content-wrapper">
		<section class="content-header">
			<div class="pull-left">
				<h1>
					Credits Networks
				</h1>
			</div>
			<!--div class="pull-right">
				<button type="button" class="btn btn-primary addCredit common-add">Add Credit Network</button>
			</div-->
		</section>
		
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
									  <th>Sr.no</th>
									  <th>Network</th>
									  <th>Action</th>
									</tr>
								</thead>
								<tbody id="allResult">
								<?php 
									if(isset($credits) && !empty($credits)) {
										foreach($credits as $value){
								?>
											<tr>
												<td><?php echo $value->id;?></td>
												<td><?php echo ucfirst($value->network);?></td>
												<td>
													<a href="javascript:void(0)" class="editInformation" data-target="credit_networks" data-rel="edit-credit-network" data-id="<?php echo $value->id; ?>"><i class="fa fa-edit"></i></a>
													<!--a href="javascript:void(0)" class="delete-credits" data-target="credit_networks" data-id="<?php echo $value->id; ?>"><i class="fa fa-trash"></i></a-->
												</td>
											</tr>
								<?php } } else{ ?>
									<tr>
										<td colspan="2">No matching record found...</td>
									</tr>

								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="control-sidebar-bg"></div>
	<div class="modal fade" id="commonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title"><span id="modal-title"></span> Credit Network</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body get-offer-detail">
					<form id="add-credit-network" method="post" action="<?php echo site_url('admin/add-credit-network');?>">
						<input type="hidden" name="id" class="inputId">
						<div class="container-fluid">
							<div class="row">
								<div class="form-group col-md-6">
									<label for="recipient-name" class="col-form-label">Credit Network</label> 
									<input type="text" name="network" class="form-control network" placeholder="Credit Network">
								</div>
								
								
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-value"></button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
				</form>
			</div>
		</div>
	</div>

<!-- View Offer Modal -->



<!-- Modal2 -->

<?php $this->load->view('modal/delete-modal.php');?>

<?php $this->load->view('backend/include/footer.php');?>
<script src="<?php echo site_url(); ?>assets/js/dashboard.js"></script>
<script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>



