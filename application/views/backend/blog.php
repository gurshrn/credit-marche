<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>
	<div class="content-wrapper">
		<section class="content-header">
			<div class="pull-left">
				<h1>
					Blog
				</h1>
				<div class="pull-right">
				<a href="<?php echo site_url('admin/add-blog');?>"><button type="button" class="btn btn-primary">Add Blog</button></a>
			</div>
			</div>
		</section>
		
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
									  <th>Title</th>
									  <th>Action</th>
									</tr>
								</thead>
								<tbody id="allResult">
								<?php 
									if(isset($footer) && !empty($footer)) {
										foreach($footer as $value){
								?>
											<tr>
												<td><?php echo ucfirst($value->title);?></td>
												<td>
													<a href="<?php echo site_url('admin/edit-blog/'.$value->id); ?>"><i class="fa fa-edit"></i></a>
												</td>
											</tr>
								<?php } } else{ ?>
									
									<tr>
										<td colspan="2">No matching record found...</td>
									</tr>

								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="control-sidebar-bg"></div>
	
<!-- View Offer Modal -->



<!-- Modal2 -->



<?php $this->load->view('backend/include/footer.php');?>
<script src="<?php echo site_url(); ?>assets/js/dashboard.js"></script>


