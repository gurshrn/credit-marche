<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>

	<div class="content-wrapper">
		<section class="content-header productTitle">
			<h1>Add Footer Detail</h1>
		</section>
		<section class="content backend-sec">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
						<form id="add-footer-section" action="<?php echo site_url('admin/add-footer-detail'); ?>" method="POST" autocomplete="off" enctype="multipart/form-data">
								
								<input type="hidden" name="id" value="<?php if(isset($editTitle) && !empty($editTitle)){ echo $editTitle->id;}?>">
								
								
								<div class="row">
									<div class="col-md-6">
										<?php 
											if(isset($editTitle) && !empty($editTitle)){
												if($editTitle->id == 3){
											
										?>
												<div class="form-group clearfix">
											
													<label for="exampleInputPassword1">Image</label>
													
													<input type="file" class="form-control blogImage" name="<?php if(isset($editTitle) && !empty($editTitle)){ echo '';}else{ echo 'image'; }?>" onchange="readURL(this)";>
													<img id="blah" src="<?php if(isset($editTitle) && !empty($editTitle)){ echo site_url('assets/upload/images/'.$editTitle->image);}?>" alt="your image" height="250px" width="300px" <?php if(isset($editTitle) && !empty($editTitle)){ echo "style='display:block'"; } else { echo "style='display:none'";}?>>
										
										</div>
										
										<?php } } ?>
										
									</div>
									<div class="col-md-6">
									<div class="form-group clearfix">
										
										<label for="exampleInputPassword1">Title</label>
										
										<input type="text" id="" class="form-control" placeholder="Title" name="title" value="<?php if(isset($editTitle) && !empty($editTitle)){ echo $editTitle->title;}?>">
									
									</div>
								</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group clearfix">
											
											<label for="exampleInputPassword1">Description</label>
											
											<textarea name="description"><?php if(isset($editTitle) && !empty($editTitle)){ echo $editTitle->description;}?></textarea>
										
										</div>
									</div>
								</div>
							
								<div class="btn-group pull-right row">
								   <div class="col-md-3"><button class="btn btn-primary footersection" type="submit">Save</button></div>
								</div>
							</form>
					
					</div>
					
	            	
	        	</div>
	    	</div>
		</div>
	</section>
</div>

	
<?php $this->load->view('include/footer.php');?>
<script src="<?php echo site_url()  ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		plugins: "link image",
	});
	 
	</script>
<script>
jQuery("#datepicker1").datepicker({

            minDate: "+3",

            dateFormat: 'mm/dd/yy'

        });
</script>
<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
				$('.blogImage').attr('name','image');
				$('#blah').css('display','block');
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>








