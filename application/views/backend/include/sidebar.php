<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php 
        if($this->session->userdata('admin-userRefId'))
        {
            $adminUserrefId = $this->session->userdata('admin-userRefId');
            $adminFirstname = $this->session->userdata('admin-firstName');
            $userdetail = getUserDetail($adminUserrefId);
			
		}
?>

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Credit Marche</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Credit</b>  Marche</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><?php echo ucfirst($adminFirstname); ?></span>
            </a>
			<ul class="dropdown-menu">
              <li class="user-footer">
                <div class="pull-left">
                  <!--a href="#" class="btn btn-default btn-flat">Profile</a-->
                </div>
                <div class="pull-right">
                  <a href="<?php echo site_url('admin/logout');?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
            
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
     
      <ul class="sidebar-menu" data-widget="tree">
        
        <li class="<?php if($parenturl == 'Dashboard'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/dashboard');?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
         
        </li>
		<li class="<?php if($parenturl == 'User'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/users');?>">
            <i class="fa fa-user-circle-o"></i> <span>Users</span>
          </a>
         
        </li>
		<li class="<?php if($parenturl == 'Offer'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/offers');?>">
            <i class="fa fa-gift"></i> <span>Offers</span>
          </a>
         
        </li>
		<li class="<?php if($parenturl == 'Credit network'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/credits-network');?>">
            <i class="fa fa-credit-card-alt"></i> <span>Credits Network</span>
          </a>
        </li>
		<li class="<?php if($parenturl == 'Network operator'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/network-operator');?>">
            <i class="fa fa-rss"></i> <span>Network Operator</span>
          </a>
        </li>
		<li class="<?php if($parenturl == 'Transaction History'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/transaction-history');?>">
            <i class="fa fa-history"></i> <span>Transaction History</span>
          </a>
        </li>
		<li class="<?php if($parenturl == 'Commission by User'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/commission-by-user');?>">
            <i class="fa fa-percent"></i> <span>Commission by User</span>
          </a>
        </li>
		<li class="<?php if($parenturl == 'caht Messages'){ echo 'active';}?>">
          <a href="<?php echo site_url('/admin/chat-messages');?>">
            <i class="fa fa-percent"></i> <span>Chat Messages</span>
          </a>
        </li>
		<li class="treeview <?php if($parenturl == 'Setting'){ echo 'menu-open';}?>">
          <a href="javascript:void(0)">
            <i class="fa fa-cog"></i>
            <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" <?php if($parenturl == 'Setting'){ echo 'style="display:block"';}?>>
            <li class=""><a href="<?php echo site_url('/admin/commission-percent'); ?>"><i class="fa fa-circle-o"></i> Commission</a></li>
			<li class=""><a href="<?php echo site_url('/admin/footer-section'); ?>"><i class="fa fa-circle-o"></i> Footer Section</a></li>
			<li class=""><a href="<?php echo site_url('/admin/blog'); ?>"><i class="fa fa-circle-o"></i> Blog</a></li>
			<li class=""><a href="<?php echo site_url('/admin/faq'); ?>"><i class="fa fa-circle-o"></i> FAQ</a></li>
          </ul>
		</li>
     </ul>
    </section>
   
  </aside>
