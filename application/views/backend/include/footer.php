		<script src="<?php echo site_url(); ?>assets/backend/bower_components/jquery/dist/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
		<script>
		  $.widget.bridge('uibutton', $.ui.button);
		</script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/js/jquery.validate.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/js/form-validation-script.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/raphael/raphael.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/morris.js/morris.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/moment/min/moment.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/bower_components/fastclick/lib/fastclick.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/dist/js/adminlte.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/js/home.js"></script>
		<script src="<?php echo site_url(); ?>assets/js/custom.js"></script>
		<script src="<?php echo site_url(); ?>assets/js/dashboard.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/dist/js/pages/dashboard.js"></script>
		<script src="<?php echo site_url(); ?>assets/backend/dist/js/demo.js"></script>
		<script src="<?php echo site_url(); ?>assets/js/admin.js"></script>
	</body>
</html>