<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>AdminLTE 2 | Dashboard</title>
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/bower_components/bootstrap/dist/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
      <!--link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/bower_components/font-awesome/css/font-awesome.min.css"-->
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/bower_components/Ionicons/css/ionicons.min.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/dist/css/AdminLTE.min.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/dist/css/skins/_all-skins.min.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/bower_components/morris.js/morris.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
      <!--link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/bower_components/jvectormap/jquery-jvectormap.css"-->
      
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/bower_components/bootstrap-daterangepicker/daterangepicker.css">
      <link rel="stylesheet" href="<?php echo site_url(); ?>assets/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">	  	  
	  <link href="<?php echo site_url(); ?>assets/backend/style.css" rel="stylesheet">
      <script>
        var site_url = '<?php echo site_url(); ?>';
    </script>
    </head>