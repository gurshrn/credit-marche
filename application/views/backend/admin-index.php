<?php $this->load->view('backend/include/header.php');?>
<?php $this->load->view('backend/include/sidebar.php');?>
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Users
			</h1>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
									  <th>Civility</th>
									  <th>First Name</th>
									  <th>Last Name</th>
									  <th>Email</th>
									  <th>Tel Number</th>
									  <th>Residence</th>
									  <th>Network Operator</th>
									  <th>Age Range</th>
									  <th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php 
									if(isset($userdetail) && !empty($userdetail)) {
										foreach($userdetail as $value){
								?>
											<tr>
												<td><?php echo $value->civility.'.';?></td>
												<td><?php echo ucfirst($value->first_name);?></td>
												<td><?php echo ucfirst($value->last_name);?></td>
												<td><?php echo $value->email;?></td>
												<td><?php echo $value->tel_number;?></td>
												<td><?php echo $value->residence;?></td>
												<td><?php echo $value->network_operator;?></td>
												<td><?php echo $value->age_range;?></td>
												<td>
													<?php 
													  if($value->user_active == 1) 
													  {
													?>
														<button type="button" class="btn btn-success approveuser Disable<?php echo $value->userRefId;?>" data-attr="Disable" data-status="0" data-id="<?php echo $value->userRefId;?>">Enabled</button>
														
													
													<?php } if($value->user_active == 0) { ?>

																<button type="button" class="btn btn-danger approveuser Enable<?php echo $value->userRefId;?>" data-attr="Enable" data-status="1" data-id="<?php echo $value->userRefId;?>">Disabled</button>
													
													<?php } if($value->status == 0) { ?>

																<button type="button" class="btn btn-danger approveuser Approve<?php echo $value->userRefId;?>" data-attr="Approve" data-status="1" data-id="<?php echo $value->userRefId;?>">Rejected</button>
													<?php  } ?>
												</td>
											</tr>
								<?php } } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="control-sidebar-bg"></div>

	<!-- Modal1 -->

	<div class="modal fade" id="user-approved" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title" id="exampleModalLongTitle"><span class="statusType"></span> User</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input type="hidden" class="userid">
					<input type="hidden" class="status">
					<input type="hidden" class="statusTypes">
					<h4><span class="user-body">Are you sure you want to <span class="statusType"></span> this user?</span></h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary updateUserStatus">Yes</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal2 -->
	
	<div class="modal fade" id="user-approved" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="exampleModalLongTitle">Approve User</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" class="userid">
				<h4><span class="user-body">Are you sure you want to enable this user?</span></h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary updateUserStatus">Yes</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
			</div>
		</div>
	  </div>
	</div>

<?php $this->load->view('backend/include/footer.php');?>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>


