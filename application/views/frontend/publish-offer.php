<?php $this->load->view('include/header.php'); ?>
<?php 
	$this->load->view('include/frontend-header.php'); 
	$userRefId = $this->session->userdata('userRefId');
	$networkDetail = getUserNetworkDetail($userRefId);
	$getNetworkName = getNetworkName($networkDetail->network_operator);
?>

<section class="internal-banner" style="background:url('<?php echo site_url(); ?>assets/images/banner-image.jpg');">
        <div class="container">
            <h1>Phone Credit Trading Place</h1>
        </div>
    </section>
    <section class="market-rates-section" style="background:url('<?php echo site_url(); ?>assets/images/market-rate-bg.jpg');">
         <?php $this->load->view('frontend/home-promotion-sidebar.php');?>
    </section>
    <section class="publish-offer">
        <div class="container">
            <h2 class="title"><span>Publish</span> an offer</h2>
            <div class="publish-window">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-PublishOffer-tab" data-toggle="pill" href="#pills-PublishOffer" role="tab" aria-controls="pills-PublishOffer" aria-selected="true">Publish an Offer</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-internetOffer-tab" data-toggle="pill" href="#pills-internetOffer" role="tab" aria-controls="pills-internetOffer" aria-selected="false">Publish an Internet Offer</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-PublishOffer" role="tabpanel" aria-labelledby="pills-PublishOffer-tab">
                        <form id="publish-credit-offer" method="POST" action="<?php echo site_url('/publish-credit-offer');?>" autocomplete="off">
                            <input type="hidden" name="id">
                            <input type="hidden" name="types" value="frontend">
                            <input type="hidden" name="credit_network" class="credit-networks" value="<?php echo $networkDetail->network_operator;?>">
							
							<div class="form-group">
                                <div class="form-control">
                                    <label>Value of Credits (F):</label>
                                    <input type="text" placeholder="Enter Value of Credit" name="credits" class="validNumber calculateVal creditVal">
									<label class="error creditPriceError"></label>
                                </div>
								<div class="form-control">
                                    <label>Selling Price (F):</label>
                                    <input type="text" placeholder="Enter Value" name="selling_price" class="validNumber calculateVal sellingPrice">
									<label class="error sellingPriceError"></label>
                                </div>
                                
                            </div>
                            <div class="form-group">
                                
                                <div class="form-control">
                                    <label>Promotion Type:</label>
                                    <input type="text" class="promotionType" placeholder="Calculated" name="promotion_type" readonly>
                                </div>
								<div class="form-control slct">
                                    <label>Bonus Crédits valid on Network:</label>
                                    <select name="credit_network_status" id="creditNetwork">
										<option value="">Select...</option>
                                        <option value="0">Any Network</option>
                                        <option value="<?php echo $networkDetail->network_operator;?>"><?php echo $getNetworkName->network;?></option>
										
									</select>
                                </div>
                            </div>
                            <div class="form-group">
                                
								<div class="form-control offerExpiryDate">
                                    <label>Expiration Date:</label>
                                    <input id="datepicker" type="text" placeholder="Enter Date" name="expiry_date">
                                </div>
                                <div class="form-control">
								<input type="checkbox"  value="IsUnlimited" class="offerUnlimited">Is Unlimited
                                </div>
							</div>
                            <input class="validate-btn validate-offer" type="button" value="validate offer">
                            <input class="publish-btn publish-credit-offer" type="button" value="publish offer" disabled>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="pills-internetOffer" role="tabpanel" aria-labelledby="pills-internetOffer-tab">

                        <form id="internet-offer" method="POST" action="<?php echo site_url('/publish-internet-offer');?>" autocomplete="off">
                            <input type="hidden" name="types" value="frontend">
							<input type="hidden" name="id">
							<input type="hidden" name="internet_network" value="<?php echo $networkDetail->network_operator;?>">
							<input type="hidden" name="internet_credit_status" value="<?php echo $networkDetail->network_operator;?>">
							<div class="form-group">
                                <div class="form-control">
                                    <label>Internet volume</label>
                                    <input type="text" name="internet_volume" class="validNumber netCalculateVal internetVol" placeholder="Enter the Internet Volume">
									<label class="error volumePriceError"></label>
                                </div>
								<div class="form-control">
                                    <label>Selling Price (F):</label>
                                    <input type="text" name="network_selling_price" class="validNumber netCalculateVal internetSellingPrice" placeholder="Enter Value">
									<label class="error networkSellingPriceError"></label>
								</div>
                                
                            </div>
                            <div class="form-group">
                                
								<div class="form-control publishExpiryDate">
                                    <label>Expiration Date:</label>
                                    <input id="datepicker1" type="text" name="network_expiry_date" placeholder="Enter Date">
                                </div>
                                <div class="form-control">
								<input type="checkbox"  value="IsUnlimited" class="publishUnlimited">Is Unlimited
                                </div>
                            </div>
							
							
                            <input class="validate-btn internet-validate-offer" type="button" value="validate offer">
                            <input class="publish-btn internet-publish-offer" type="button" value="publish offer" disabled>
                        </form>

                    </div>
                </div>
            </div>
            <div class="person-image wow zoomIn">
                <img src="<?php echo site_url(); ?>assets/images/dummy-person-img.png" alt="person-image">
            </div>
        </div>
    </section>


<?php $this->load->view('include/frontend-footer.php'); ?>
<?php $this->load->view('modal/login-modal.php');?>
<?php $this->load->view('include/footer.php'); ?>
<script>
setTimeout(function() {
    document.documentElement.scrollTop =
        document.body.scrollTop = -500;
}, 0);
</script>