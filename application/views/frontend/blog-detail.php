<?php $this->load->view('include/header.php'); ?>
<?php $this->load->view('include/frontend-header.php'); ?>
<?php 
	if($this->session->userdata('userRefId'))
	{
		$userrefId = $this->session->userdata('userRefId');
		$networkDetailUser = 	getUserNetworkDetail($userrefId);
		
	}
	else
	{
		$userrefId = '';
	}
	
	$recentBlogDetail = getRecentBlogDetail();


?>

    <!-- Header Ends Here -->
    <section class="internal-banner" style="background:url(<?php echo site_url();?>assets/images/banner-image.jpg);">
        <div class="container">
            <h1>Blog</h1>
        </div>
    </section>
	<?php //print_r($blogDetail);?>
    <section class="blog-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="main-blogs">
						<?php 
							foreach($blogDetail as $val) {
								$string = substr($val->description, 0, 800);
						
						?>
							<div class="blog-box">
								<figure><img src="<?php echo site_url('assets/upload/images/'.$val->image);?>" alt="blog image"></figure>
								<h2><a href="<?php echo site_url('blog-internal-detail/'.$val->id);?>"><?php echo ucfirst($val->title);?></a></h2>
								<p><?php echo $string;?></p>
								<a class="read-more-btn" href="<?php echo site_url('blog-internal-detail/'.$val->id);?>">Read More...</a>
								<div class="blog-foot">
									<div class="author-name">
										<p>By Admin || <?php echo date('d M Y',strtotime($val->addedondate));?></p>
									</div>
									<!--ul>
										<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
										<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
									</ul-->
								</div>
							</div>
						<?php } ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <aside class="blog-sidebar">
                        <div class="srch-div">
                            <input type="text" placeholder="Search">
                            <button><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                        <div class="recent-posts">
                            <h2>Recent Posts</h2>
							<?php 
								foreach($recentBlogDetail as $vals) { 
									$string = substr($vals->description, 0, 300);
							
							?>
								
								<div class="post-block">
									<figure><img src="<?php echo site_url('assets/upload/images/'.$vals->image);?>"></figure>
									<figcaption>
										<h4><a href="<?php echo site_url('blog-internal-detail/'.$vals->id);?>"><?php echo ucfirst($vals->title);?></a></h4>
										<p><?php echo $string;?></p>
									</figcaption>
								</div>
								
							<?php } ?>
                           
                            
                            
                            
                        </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
	
	
<?php $this->load->view('modal/thanku-popup.php');?>
<?php $this->load->view('include/frontend-footer.php');?>
<?php $this->load->view('modal/login-modal.php');?>
<?php $this->load->view('modal/otp-modal.php');?>
<?php $this->load->view('include/footer.php');?>