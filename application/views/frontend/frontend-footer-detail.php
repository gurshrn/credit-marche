<?php $this->load->view('include/header.php'); ?>
<?php $this->load->view('include/frontend-header.php'); ?>
<?php 
	if($this->session->userdata('userRefId'))
	{
		$userrefId = $this->session->userdata('userRefId');
		$networkDetailUser = 	getUserNetworkDetail($userrefId);
		
	}
	else
	{
		$userrefId = '';
	} 
?>

    <!-- Header Ends Here -->
    <section class="internal-banner" style="background:url('<?php echo site_url(); ?>assets/images/banner-image.jpg');">
        <div class="container">
            <h1><?php echo $footer->title;?></h1>
        </div>
    </section>
	
		<section class="innerPages">
			<div class="container">
				<?php echo $footer->description;?>
			</div>
		</section>
	
<?php $this->load->view('modal/thanku-popup.php');?>
<?php $this->load->view('include/frontend-footer.php');?>
<?php $this->load->view('modal/login-modal.php');?>
<?php $this->load->view('modal/otp-modal.php');?>
<?php $this->load->view('include/footer.php');?>