<?php $this->load->view('include/header.php'); ?>
<?php $this->load->view('include/frontend-header.php'); ?>
<?php 
	if($this->session->userdata('userRefId'))
	{
		$userrefId = $this->session->userdata('userRefId');
		$networkDetailUser = 	getUserNetworkDetail($userrefId);
		
	}
	else
	{
		$userrefId = '';
	}
	
?>
    <section class="internal-banner" style="background:url(<?php echo site_url();?>assets/images/banner-image.jpg);">
        <div class="container">
            <h1>faq</h1>
        </div>
    </section>
    <section class="faq">
        <div class="container">
            <div id="accordion">
				<?php $i=1;foreach($faqDetail as $val){ ;?>
					<div class="card">
						<div class="card-header" id="headingOne">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse<?php echo $i;?>" aria-expanded="false" aria-controls="collapseOne"><?php echo ucfirst($val->title);?></button>
							</h5>
						</div>
						<div id="collapse<?php echo $i;?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
							<div class="card-body"><?php echo $val->description;?></div>
						</div>
					</div>
				
				<?php $i++;} ?>
            </div>
        </div>
    </section>
<?php $this->load->view('modal/thanku-popup.php');?>
<?php $this->load->view('include/frontend-footer.php');?>
<?php $this->load->view('modal/login-modal.php');?>
<?php $this->load->view('modal/otp-modal.php');?>
<?php $this->load->view('include/footer.php');?>
