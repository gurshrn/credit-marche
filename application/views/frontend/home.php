<?php $this->load->view('include/header.php'); ?>
<?php $this->load->view('include/frontend-header.php'); ?>
<?php 
	if($this->session->userdata('userRefId'))
	{
		$userrefId = $this->session->userdata('userRefId');
		$networkDetailUser = 	getUserNetworkDetail($userrefId);
		
	}
	else
	{
		$userrefId = '';
	} 
?>
	<!-- Header Ends Here -->
    <section class="home-slider" style="background:url('<?php echo site_url(); ?>assets/images/banner-image.jpg');">
        <div class="container">
            <h1>Phone Credit Trading Place</h1>
            <div id="carouselExampleFade" class="carousel carousel-fade" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleFade" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleFade" data-slide-to="1"></li>
                    <li data-target="#carouselExampleFade" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active" style="background: url('<?php echo site_url(); ?>assets/images/desktop-slide-img.png');">
                        <div class="video-container">
                            <iframe src="https://www.youtube.com/embed/F3QpgXBtDeo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="carousel-item" style="background: url('<?php echo site_url(); ?>assets/images/desktop-slide-img.png');">
                        <div class="video-container">
                            <iframe src="https://www.youtube.com/embed/MiybniIIvx0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="carousel-item" style="background: url('<?php echo site_url(); ?>assets/images/desktop-slide-img.png');">
                        <div class="video-container">
                            <iframe src="https://www.youtube.com/embed/F3QpgXBtDeo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="market-rates-section pdng" style="background:url('<?php echo site_url(); ?>assets/images/market-rate-bg.jpg');">
         <?php $this->load->view('frontend/home-promotion-sidebar.php');?>
    </section>
	<section class="ntwrk-form-section">
        <div class="container">
            <form>
                <div class="form-group">
                    <label>Offer Type</label>
                    <div class="form-control">
                        <select class="offers-type">
                            <option value="">select...</option>
                             <option value="1">Phone credit</option>
                             <option value="2">Internet volume</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
					<label>Network Operator</label>
					<div class="form-control">
						<select class="networks-operator">
							  <option value="">select</option>
							  <?php if(isset($creditNetwork) && !empty($creditNetwork)) { 
									foreach($creditNetwork as $val)
									{
							 
							 ?>
									 <option value="<?php echo $val->id;?>"><?php echo $val->network;?></option>
							<?php } } ?>
						</select>
					</div>
				</div>
                <!--div class="form-group">
                    <label>Network Validity</label>
                    <div class="form-control">
                        <select>
                            <option disabled>select</option>
                             <option>6 months</option>
                             <option>1 year</option>
                        </select>
                    </div>
                </div-->
              
                <div class="form-submit">
                    <input type="button" data-target="home-offers-search" class="frontend-search-offers" value="Search">
                </div>
            </form>
        </div>
    </section>
    <section class="avlbl-offers">
        <div class="container">
            <h2 class="title">			
			<span>Offers</span> available</h2>
				<div class="allResult">			
			<?php 												
				if(isset($getAllOffers) && !empty($getAllOffers)){					
				foreach($getAllOffers as $val){	
					$countUserRating = count(countUserRating($val->userRefId));
					if($countUserRating != '')
					{
						$sumUserRating = sumUserRating($val->userRefId);
						$totalRating = $sumUserRating->total/$countUserRating;
					}
					else
					{
						$totalRating = 3;
					}
			?>
            <div class="offer-block">
                <div class="top-head">
                    <h3>
					
					<?php 
						$networkDetailUsers = 	getUserNetworkDetail($val->userRefId);
						$getNetworkName = getNetworkName($networkDetailUsers->network_operator);
						echo $getNetworkName->network;
					
					?>
					</h3>
                    <div class="btns-outer">
						<span class="fa fa-star <?php if($totalRating == 1 || $totalRating >1 ) { echo 'checked';}?>" ></span>
						<span class="fa fa-star <?php if($totalRating == 2 || $totalRating >2) { echo 'checked';}?>"></span>
						<span class="fa fa-star <?php if($totalRating == 3 || $totalRating >3) { echo 'checked';}?>"></span>
						<span class="fa fa-star <?php if($totalRating == 4 || $totalRating >4) { echo 'checked';}?>"></span>
						<span class="fa fa-star <?php if($totalRating == 5) { echo 'checked';}?>"></span>
						<?php $soldOffer = count(userSoldOffer($val->userRefId));
								echo '('.$soldOffer.')';
						?>
					<?php 
						
						$expiry_time = date('Y-m-d H:i:s', strtotime($val->expiry_date));							
						$expiry_date = date('Y-m-d H:i:s',strtotime('-6 hour',strtotime($expiry_time)));
						
						$red_point = date('Y-m-d H:i:s',strtotime('-15 hour',strtotime($expiry_date)));							
						$yellow_point = date('Y-m-d H:i:s',strtotime('-24 hour',strtotime($expiry_date)));							
						$currentdatetime = date('Y-m-d H:i:s');	
						if($val->expiry_date != ''){
						if($currentdatetime > $red_point){ 
					?>								
							<span class="status-point red-point"></span>							
						<?php 
							} 
							else if($currentdatetime >= $yellow_point && $currentdatetime < $red_point)
							{
						?>								
							<span class="status-point"></span>							
						<?php 
							} 
							else 
							{ 
						?>								
							<span class="status-point green-point"></span>							
						<?php 
							}
						}
						else
						{
							echo '<span class="status-point green-point"></span>';
						}
						if($userrefId != '' && $userrefId != $val->userRefId && $networkDetailUser->network_operator == $val->credit_network)
						{
							if($this->session->userdata('userRefId') && empty($purchaseOffer)) 
							{ 
								
						?>						
									<a href="javascript:void(0)" class="btn buy-btn offer-history" data-id="<?php echo $val->id;?>">buy now</a>
						<?php 	}
								
							else if($this->session->userdata('userRefId') && !empty($purchaseOffer))
							{ ?>
								<a href="javascript:void(0)" class="btn buy-btn confirmCredit">buy now</a>
								
							<?php }
						}
						if($userrefId == '')
						{ ?>
							<a href="javascript:void(0)" class="btn buy-btn" <?php if($this->session->userdata('userRefId') && $userdetail->status == 1) { echo ''; } else{ echo 'data-attr="login" id="frontend-register"'; } ?>>buy now</a>
						<?php }
							
						?>
                    </div>
                </div>
                <table>					
				<thead>						
				<tr>																															
					<?php 
						if($val->offer_type == 1)
						{ 
							echo '<th>Value of Credits (F)</th>';
						}
						else
						{
							echo '<th>Internet Volume (mb)</th>';
						}
					?>	
					<?php 
						if($val->offer_type == 1)
						{ 
							echo '<th>Expiration Date of Bonus Credits</th>';
						}
						else
						{
							echo '<th>Expiration Date of Pass Internet</th>';
						}
					?>	
					<?php 
						if($val->offer_type == 1)
						{ 
							echo '<th>Credits valid on network</th>';
						}
					?>						
					
					<th>Offer Type</th>	
					<?php 
						if($val->offer_type == 2)
						{ 
							echo '<th>Ratio (F/MB)</th>';
						}
						if($val->offer_type == 1)
						{ 
							echo '<th>Promotion Type</th>';
						}
					?>					
					<th>Selling Price(F)</th>		
					<th>Date of Publication</th>	
					<th>Date of Expiration</th>		
					</tr>					
					</thead>			
					<tbody>				
					<tr>					
						<td>						
						<?php 								
						if($val->offer_type == 1) { echo $val->credits;} 
						if($val->offer_type == 2) { echo $val->internet_volume;} 
						?>																
						</td>						
					
					<td>					
					<?php	
						if($val->expiry_date != '')
						{
							$expirydate= strtotime($val->expiry_date);		
							echo date('d M Y', $expirydate);
						}
						else
						{
							echo 'Unlimited';
						}
										
					?>											
					</td>	
					<?php 								
						if($val->offer_type == 1)	{ ?>				
							<td>
								<?php 
									if($val->credit_network_status == 0)
									{
										echo 'Any Network';
									}
									else
									{
										$networkDetail = getUserNetworkDetail($val->userRefId);
										$getNetworkName = getNetworkName($networkDetail->network_operator);
										echo ucfirst($getNetworkName->network);
									}
								
								?>
							
							</td>
					<?php } ?>
					<td>								
					<?php 								
					if($val->offer_type == 1) { echo 'PHONE CREDIT';} 	
					if($val->offer_type == 2) { echo 'INTERNET VOLUME';} 
					?>														
					</td>												
					<?php if($val->offer_type == 2) { ?>
					<td>													
						<?php 
							$num = $val->selling_price/$val->internet_volume;
							$whole = (int) $num;  // 5
							
							$frac  = $num - (int) $num;  // .7
							if($frac == 0)
							{
								echo $whole;
							}
							else
							{
								 echo number_format($num,2);
							}
						?>														
					</td>	
					<?php } ?>
					<?php if($val->offer_type == 1) { ?>
					<td>													
						<?php 
							
							$num1 = round($val->promotion_type);
							echo $num1.'%';
						?>	
									
																			
					</td>	
					<?php } ?>
					<td><?php echo $val->selling_price;?></td>	
					<td>										
					<?php										
					$addeddate= strtotime($val->addedondate);		
					echo date('d M Y', $addeddate);				
					?>											
					</td>							
					<?php if($val->expiry_date != ''){ if($currentdatetime >= $red_point){ ?>	
					<td class="red-txt">							
					<?php } else if($currentdatetime >= $yellow_point && $currentdatetime < $red_point){?>	
					<td class="orange-txt">							
					<?php } else{ ?>								
					<td class="green-txt">					
					<?php } 
						if($val->expiry_date != '')
						{
							$now = date('Y-m-d');							
							$your_date = date('Y-m-d',strtotime($val->expiry_date));
							if($currentdatetime > $expiry_date)
							{
								echo 'Expired';
							}
							else
							{
								$datetime1 = new DateTime($now);		
								$datetime2 = new DateTime($your_date);	
								$difference = $datetime1->diff($datetime2);	
								echo $difference->d.' days';
							}
						}
						
					}
					else
					{
						echo '<td class="green-txt">Unlimited</td>';
					}
							
				?>									
				</td>						
				</tr>					
				</tbody>				
				</table>
				
				
            </div>			
			
			<?php } } else{ ?>
				<div class="offer-block">
					<div class="top-head">
						No record found...
					</div>
				</div>
			
			<?php } ?>
			</div>
            
            
        </div>
    </section>

	<div id="offerHistory" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
		
	</div>
<?php $this->load->view('modal/thanku-popup.php');?>
<?php $this->load->view('include/frontend-footer.php');?>
<?php $this->load->view('modal/login-modal.php');?>
<?php $this->load->view('modal/otp-modal.php');?>
<?php $this->load->view('include/footer.php');?>
<script>
$('#star1').starrr();

</script>

