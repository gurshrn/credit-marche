<?php $this->load->view('include/header.php'); ?>
<?php $this->load->view('include/frontend-header.php'); 

	$userRefId = $this->session->userdata('userRefId');
	$networkDetail = getUserNetworkDetail($userRefId);
	$getNetworkName = getNetworkName($networkDetail->network_operator);

?>

<section class="internal-banner" style="background:url('<?php echo site_url(); ?>assets/images/banner-image.jpg');">
        <div class="container">
            <h1>Phone Credit Trading Place</h1>
        </div>
    </section>
    <section class="market-rates-section" style="background:url('<?php echo site_url(); ?>assets/images/market-rate-bg.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="rates-block">
                        <div class="block">
                            <div class="networks-type">
                                <span class="type1">o</span>
                                <span class="type2">k</span>
                            </div>
                            <div class="credit-market-rates">
                                <h3>Credit Market Rate</h3>
                                <p>216% <span class="dec">(-12 pts)</span></p>
                                <p>278% <span class="dec">(-16 pts)</span></p>
                            </div>
                            <div class="internetV-market-rates">
                                <h3>Internet V. Market Rate (F/MB)</h3>
                                <p>0.25 <span class="dec">(-14%)</span></p>
                                <p>0.15 <span class="inc">(+18%)</span></p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="rates-block">
                        <div class="block">
                            <div class="networks-type">
                                <span class="type3">T</span>
                                <span class="type4">E</span>
                            </div>
                            <div class="credit-market-rates">
                                <h3>Credit Market Rate</h3>
                                <p>655% <span class="dec">(+19 pts)</span></p>
                                <p>857% <span class="dec">(-11 pts)</span></p>
                            </div>
                            <div class="internetV-market-rates">
                                <h3>Internet V. Market Rate (F/MB)</h3>
                                <p>0.13 <span class="dec">(-3%)</span></p>
                                <p>0.09 <span class="inc">(+10%)</span></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="publish-offer">
        <div class="container">
            <h2 class="title"><span>Update Publish</span> an offer</h2>
            <div class="publish-window">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
					<?php if($offerdetail->offer_type == 1){ ?>
						<li class="nav-item">
							<a class="nav-link active" id="pills-PublishOffer-tab" data-toggle="pill" href="#pills-PublishOffer" role="tab" aria-controls="pills-PublishOffer" aria-selected="true">Update Publish an Offer</a>
						</li>
					<?php } if($offerdetail->offer_type == 2){?>
						<li class="nav-item">
							<a class="nav-link" id="pills-internetOffer-tab" data-toggle="pill" href="#pills-internetOffer" role="tab" aria-controls="pills-internetOffer" aria-selected="false">Update Publish an Internet Offer</a>
						</li>
					<?php } ?>
                </ul>
                <div class="tab-content" id="pills-tabContent">
				<?php if($offerdetail->offer_type == 1){ ?>
                    <div class="tab-pane fade show active" id="pills-PublishOffer" role="tabpanel" aria-labelledby="pills-PublishOffer-tab">
                        <form id="publish-credit-offer" method="POST" action="<?php echo site_url('/publish-credit-offer');?>" autocomplete="off">
                            <input type="hidden" name="id" value="<?php echo $offerdetail->id; ?>">
							<input type="hidden" name="types" value="frontend">
							<input type="hidden" name="credit_network" value="<?php echo $networkDetail->network_operator;?>">
							<div class="form-group">
                                <div class="form-control">
                                    <label>Value of Credits (F):</label>
                                    <input type="text" placeholder="Enter Value of Credit" name="credits" class="validNumber calculateVal creditVal" value="<?php echo $offerdetail->credits; ?>">
									<label class="error creditPriceError"></label>
                                </div>
                                <div class="form-control">
                                    <label>Bonus Expiration Date:</label>
                                    <input id="datepicker" type="text" placeholder="Enter Date" name="expiry_date" value="<?php echo $offerdetail->expiry_date; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-control">
                                    <label>Selling Price (F):</label>
                                    <input type="text" placeholder="Enter Value" name="selling_price" class="validNumber calculateVal sellingPrice" value="<?php echo $offerdetail->selling_price; ?>">
									<label class="error sellingPriceError"></label>
                                </div>
                                <div class="form-control">
                                    <label>Promotion Type:</label>
                                    <input type="text" class="promotionType" placeholder="Calculated" name="promotion_type" value="<?php echo $offerdetail->promotion_type; ?>" readonly>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-control slct">
                                    <label>Bonus Crédits valid on Network:</label>
                                    <select name="credit_network_status" id="creditNetwork">
										<option value="">Select...</option>
                                        <option value="0" <?php if($offerdetail->credit_network_status == 0){ echo "selected='selected'";}?>>Any Network</option>
                                        <option value="<?php echo $networkDetail->network_operator;?>" <?php if($offerdetail->credit_network_status == $networkDetail->network_operator){ echo "selected='selected'";}?>><?php echo $getNetworkName->network;?></option>
										
									</select>
									 
                                </div>
                            </div>
                            <input class="validate-btn validate-offer" type="button" value="validate offer">
                            <input class="publish-btn publish-credit-offer" type="button" value="publish offer" disabled>
                        </form>
                    </div>
				<?php } if($offerdetail->offer_type == 2){ ?>
                    <div class="tab-pane fade show active" id="pills-internetOffer" role="tabpanel" aria-labelledby="pills-internetOffer-tab">

                        <form id="internet-offer" method="POST" action="<?php echo site_url('/publish-internet-offer');?>" autocomplete="off">
							<input type="hidden" name="id" value="<?php echo $offerdetail->id; ?>">
							<input type="hidden" name="types" value="frontend">
							<input type="hidden" name="internet_network" value="<?php echo $networkDetail->network_operator;?>">
							<input type="hidden" name="internet_credit_status" value="<?php echo $networkDetail->network_operator;?>">
                            <div class="form-group">
                                <div class="form-control">
                                    <label>Internet volume</label>
                                    <input type="text" name="internet_volume" placeholder="Enter the Internet Volume" value="<?php echo $offerdetail->internet_volume; ?>">
									<label class="error volumePriceError"></label>
                                </div>
                                <div class="form-control">
                                    <label>Bonus Expiration Date:</label>
                                    <input id="datepicker1" type="text" name="network_expiry_date" placeholder="Enter Date" value="<?php echo $offerdetail->expiry_date; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-control">
                                    <label>Selling Price (F):</label>
                                    <input type="text" name="network_selling_price" placeholder="Enter Value" value="<?php echo $offerdetail->selling_price; ?>">
									<label class="error networkSellingPriceError"></label>
                                </div>
                            </div>
							
                            <input class="validate-btn internet-validate-offer" type="button" value="validate offer">
                            <input class="publish-btn internet-publish-offer" type="button" value="publish offer">
                        </form>

                    </div>
				<?php } ?>
                </div>
            </div>
            <div class="person-image wow zoomIn">
                <img src="<?php echo site_url(); ?>assets/images/dummy-person-img.png" alt="person-image">
            </div>
        </div>
    </section>

<?php $this->load->view('include/frontend-footer.php'); ?>
<?php $this->load->view('modal/login-modal.php');?>
<?php $this->load->view('include/footer.php'); ?>