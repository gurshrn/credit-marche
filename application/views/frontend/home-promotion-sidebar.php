<div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="rates-block">
                        <div class="block">
                            <div class="networks-type">
                                <span class="type1">o</span>
                                <span class="type2">k</span>
                            </div>
                            <div class="credit-market-rates">
                                <h3>Credit Market Rate</h3>
								<?php
									$currentdate = date('Y-m-d');
									$yesterdaydate = date('Y-m-d', strtotime(' -1 day'));
									
									$result = calculatePromotion(1,1,$currentdate);
									$subtractVal = $result[0]->totalCredits-$result[0]->totalSellingPrice;
									if($subtractVal != 0)
									{
										$promotionType = $subtractVal/$result[0]->totalSellingPrice;
										$total = $promotionType;
										$totalPromtion = $total*100;
									}
									else
									{
										$totalPromtion = 0;
									}
									
									$results = calculatePromotion(1,1,$yesterdaydate);
									$subtractVals = $results[0]->totalCredits-$results[0]->totalSellingPrice;
									if($subtractVals != 0)
									{
										$promotionTypes = $subtractVals/$results[0]->totalSellingPrice;
										$totals = $promotionTypes;
										$totalPromtions = $totals*100;
									}
									else
									{
										$totalPromtions = 0;
									}
									//$total = 
									
								?>
                                <p><?php echo round($totalPromtion).'% ';?><span class="<?php if($totalPromtions > 0) { echo 'ase';} else{ echo 'dec';}?>">(<?php echo round($totalPromtion-$totalPromtions).') pts';?></span></p>
								<?php
								
									$result1 = calculatePromotion(2,1,$currentdate);
									$subtractVal1 = $result1[0]->totalCredits-$result1[0]->totalSellingPrice;
									if($subtractVal1 != 0)
									{
										$promotionType1 = $subtractVal1/$result1[0]->totalSellingPrice;
										$total1 = $promotionType1;
										$totalPromtion1 = $total1*100;
									}
									else
									{
										$totalPromtion1 = 0;
									}
									
									$results1 = calculatePromotion(2,1,$currentdate);
									$subtractVals1 = $results1[0]->totalCredits-$results1[0]->totalSellingPrice;
									if($subtractVals1 != 0)
									{
										$promotionTypes1 = $subtractVals1/$results1[0]->totalSellingPrice;
										$totals1 = $promotionTypes1;
										$totalPromtions1 = $totals1*100;
									}
									else
									{
										$totalPromtions1 = 0;
									}
								?>
                                <p><?php echo round($totalPromtions1).'% ';?><span class="dec">(<?php echo round($totalPromtion1-$totalPromtions1).') pts';?></span></p>
                            </div>
                            <div class="internetV-market-rates">
                                <h3>Internet V. Market Rate (F/MB)</h3>
								<?php
								
									$result4 = calculatePromotion(1,2,$currentdate);
									$subtractVal4 = $result4[0]->totalSellingPrice;
									if($subtractVal4 != 0)
									{
										$totalPromtion4 = ($subtractVal4/$result4[0]->totalInternetVolume)/100;
									}
									else
									{
										$totalPromtion4 = 0;
									}
									
									$results4 = calculatePromotion(1,2,$yesterdaydate);
									$subtractVals4 = $results4[0]->totalSellingPrice;
									if($subtractVals4 != 0)
									{
										$totalPromtions4 = ($subtractVals4/$results4[0]->totalInternetVolume)/100;
									}
									else
									{
										$totalPromtions4 = 0;
									}
									
								?>
                                <p><?php echo number_format($totalPromtion4,2).' ';?><span class="dec">(<?php echo number_format($totalPromtion4-$totalPromtions4,2).') pts'; ?></span></p>
								<?php
								
									$result5 = calculatePromotion(2,2,$currentdate);
									$subtractVal5 = $result5[0]->totalSellingPrice;
									if($subtractVal5 != 0)
									{
										$totalPromtion5 = ($subtractVal5/$result5[0]->totalInternetVolume)/100;
									}
									else
									{
										$totalPromtion5 = 0;
									}
									
									$results5 = calculatePromotion(2,2,$yesterdaydate);
									$subtractVals5 = $results5[0]->totalSellingPrice;
									if($subtractVals5 != 0)
									{
										$totalPromtions5 = ($subtractVals5/$results5[0]->totalInternetVolume)/100;
									}
									else
									{
										$totalPromtions5 = 0;
									}
								?>
								<p><?php echo number_format($totalPromtion5,2).' ';?><span class="dec">(<?php echo number_format($totalPromtion5-$totalPromtions5,2).') pts'; ?></span></p>
                            </div>
                        </div>

                    </div>
                </div>
				
                <div class="col-md-6">
                    <div class="rates-block">
                        <div class="block">
                            <div class="networks-type">
                                <span class="type3">T</span>
                                <span class="type4">E</span>
                            </div>
                            <div class="credit-market-rates">
                                <h3>Credit Market Rate</h3>
                                <?php
								
									$result2 = calculatePromotion(17,1,$currentdate);
									$subtractVal2 = $result2[0]->totalCredits-$result2[0]->totalSellingPrice;
									if($subtractVal2 != 0)
									{
										$promotionType2 = $subtractVal2/$result2[0]->totalSellingPrice;
										$total2 = $promotionType2;
										$totalPromtion2 = $total2*100;
									}
									else
									{
										$totalPromtion2 = 0;
									}
									
									$results2 = calculatePromotion(17,1,$yesterdaydate);
									$subtractVals2 = $results2[0]->totalCredits-$results2[0]->totalSellingPrice;
									if($subtractVals2 != 0)
									{
										$promotionTypes2 = $subtractVals2/$results2[0]->totalSellingPrice;
										$totals2 = $promotionTypes2;
										$totalPromtions2 = $totals2*100;
									}
									else
									{
										$totalPromtions2 = 0;
									}
									
								?>
                                <p><?php echo round($totalPromtion2).'% ';?><span class="dec">(<?php echo round($totalPromtion2-$totalPromtions2).') pts'; ?></span></p>
								<?php
								
									$result3 = calculatePromotion(18,1,$currentdate);
									$subtractVal3 = $result3[0]->totalCredits-$result3[0]->totalSellingPrice;
									if($subtractVal3 != 0)
									{
										$promotionType3 = $subtractVal3/$result3[0]->totalSellingPrice;
										$total3 = $promotionType3;
										$totalPromtion3 = $total3*100;
									}
									else
									{
										$totalPromtion3 = 0;
									}
									
									$results3 = calculatePromotion(18,1,$currentdate);
									$subtractVals3 = $results3[0]->totalCredits-$results3[0]->totalSellingPrice;
									if($subtractVals3 != 0)
									{
										$promotionTypes3 = $subtractVals3/$results3[0]->totalSellingPrice;
										$totals3 = $promotionTypes3;
										$totalPromtions3 = $totals3*100;
									}
									else
									{
										$totalPromtions3 = 0;
									}
								?>
                                <p><?php echo round($totalPromtion3).'% ';?><span class="dec">(<?php echo round($totalPromtion3-$totalPromtions3).') pts'; ?></span></p>
                            </div>
                            <div class="internetV-market-rates">
                                <h3>Internet V. Market Rate (F/MB)</h3>
                                <?php
								
									$result6 = calculatePromotion(17,2,$currentdate);
									$subtractVal6 = $result6[0]->totalSellingPrice;
									if($subtractVal6 != 0)
									{
										$totalPromtion6 = ($subtractVal6/$result6[0]->totalInternetVolume)/100;
									}
									else
									{
										$totalPromtion6 = 0;
									}
									
									$results6 = calculatePromotion(17,2,$currentdate);
									$subtractVals6 = $results6[0]->totalSellingPrice;
									if($subtractVals6 != 0)
									{
										$totalPromtions6 = ($subtractVals6/$results6[0]->totalInternetVolume)/100;
									}
									else
									{
										$totalPromtions6 = 0;
									}
									
								?>
                                <p><?php echo number_format($totalPromtion6,2).' ';?><span class="dec">(<?php echo number_format($totalPromtion6-$totalPromtions6,2).') pts'; ?></span></p>
								<?php
								
									$result7 = calculatePromotion(18,2,$currentdate);
									$subtractVal7 = $result7[0]->totalSellingPrice;
									if($subtractVal7 != 0)
									{
										$totalPromtion7 = ($subtractVal7/$result7[0]->totalInternetVolume)/100;
									}
									else
									{
										$totalPromtion7 = 0;
									}
									
									$results7 = calculatePromotion(18,2,$currentdate);
									$subtractVals7 = $results7[0]->totalSellingPrice;
									if($subtractVals7 != 0)
									{
										$totalPromtions7 = ($subtractVals7/$results7[0]->totalInternetVolume)/100;
									}
									else
									{
										$totalPromtions7 = 0;
									}
								?>
								<p><?php echo number_format($totalPromtion7,2).' ';?><span class="dec">(<?php echo number_format($totalPromtion7-$totalPromtions7,2).') pts'; ?></span></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>