<?php 								
									if(isset($offerSold) && !empty($offerSold)){
										foreach($offerSold as $val1){
								?>
											<div class="offer-block">
												<div class="top-head">
													<h3>
					
													<?php 
														$networkDetailUsers2 = 	getUserNetworkDetail($val1->userRefId);
														$getNetworkName2 = getNetworkName($networkDetailUsers2->network_operator);
														echo $getNetworkName2->network;
													
													?>
													</h3>
												</div>
												<table>
													<thead>
														<tr>
															<?php 
																if($val1->offer_type == 1)
																{
																	echo '<th>VALUE OF CREDITS (F)</th>';
																}
																else
																{
																	echo '<th>Internet Volume (mb)</th>';
																}
															?>
															<th>Date sold</th>
															<th>Offer Type</th>
															<?php 
																if($val1->offer_type == 2)
																{
																	echo '<th>Ratio (F/MB)</th>';
																}
															?>
															<?php 
																if($val1->offer_type == 1)
																{
																	echo '<th>Promotion Type</th>';
																}
															?>
															<th>Selling Price(F)</th>
															<th>Date of Publication</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<?php 
																if($val1->offer_type == 1)
																{
																	echo '<td>'.$val1->credits.'</td>';
																}
																else
																{
																	echo '<td>'.$val1->internet_volume.'</td>';
																}
															?>
															<td>
																<?php															
																	$offersolddate= strtotime($val1->offersolddate);															
																	echo date('d M Y', $offersolddate);														
																?>	
															</td>
															<td>
																<?php
																	if($val1->offer_type == 1)
																	{
																		echo 'Phone credit';
																	}
																	else
																	{
																		echo 'Internet Volume';
																	}
																
																?>
															
															</td>
															<?php if($val1->offer_type == 2){ ?>
															<td>
																<?php 
																	$num1 = $val1->selling_price/$val1->internet_volume;
																	$whole1 = (int) $num1;  // 5
																	$frac1  = $num1 - (int) $num1;  // .7	
																	if($frac1 == 0)
																	{
																		echo $whole1;
																	}
																	else
																	{
																		echo number_format($num1,2);
																	}
																?>	
															</td>
															<?php }?>
															<?php 
																if($val1->offer_type == 1)
																{
																	$num1 = round($val1->promotion_type);
																	echo '<td>'.$num1.'%</td>';
																}
															?>
															<td><?php echo $val1->selling_price;?></td>
															<td>
																<?php															
																	$addedondates= strtotime($val1->addedondate);															
																	echo date('d M Y', $addedondates);														
																?>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
								<?php } } else { ?>
										<div class="offer-block">																		
											<div class="top-head">
												No matching record found...
									
											</div>
										</div>
								<?php } ?>