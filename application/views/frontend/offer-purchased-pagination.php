<?php 
									if(isset($offerPurchased) && !empty($offerPurchased))
									{
										foreach($offerPurchased as $offerPur)
										{
											$selectedTime = date('Y-m-d H:i:s', strtotime($offerPur->createdDateTime));
											$endTime = date('Y-m-d H:i:s',strtotime('+5 min',strtotime($selectedTime)));
											$currentdatetime = date('Y-m-d H:i:s');
								?>
                                <div class="offer-block">
                                    <div class="top-head">
                                        <h3>
					
											<?php 
												$networkDetailUsers3 = 	getUserNetworkDetail($offerPur->userRefId);
												$getNetworkName3 = getNetworkName($networkDetailUsers3->network_operator);
												echo $getNetworkName3->network;
											
											?>
											</h3>
										
											<div class="btns-outer">
												<?php 
													$userdetails = getUserDetail($offerPur->buyerRefId); 
													$tel_number = $userdetails->tel_number;
													$paymentDetail = getPaymentDetail($offerPur->transactionId,$offerPur->buyerRefId);
											
													$offerId = $offerPur->id;
													$buyerRefId = $offerPur->buyerRefId;
													$sellrefId = $offerPur->userRefId;
													$notifyMsg = $tel_number.' '.'offer selling price'.' '.$offerPur->selling_price.' '.'purchasing Price'.' '.$paymentDetail->debit;
												?>
												
												<?php if($offerPur->offer_confirmation == 0) { if($currentdatetime >= $endTime){?>
												
													<a class="btn buy-btn red-btn creditsClaim" data-offer="<?php echo $offerId;?>" data-from="<?php echo $buyerRefId?>" data-to="<?php echo $sellrefId?>" data-msg="<?php echo $notifyMsg;?>">claim credits</a>
													
												<?php } ?>
												
													<a class="btn buy-btn credit-recieve" data-id=<?php echo $offerPur->id;?> data-target="<?php echo $offerPur->userRefId;?>">Credit Received</a>
												
												<?php } ?>
												
											</div>
										
										
                                    </div>
                                    <table>
                                        <thead>
                                            <tr>
												<?php 
													if($offerPur->offer_type == 1)
													{
														echo '<th>Value of Credits (F)</th>';
													}
													else
													{
														echo '<th>Internet Volume (mb)</th>';
													}
												?>
                                                <th>purchase Date</th>
                                                <th>Offer Type</th>
												<?php 
													if($offerPur->offer_type == 1)
													{
														echo '<th>Credits valid on network</th><th>Promotion Type</th>';
													}
													else
													{
														echo '<th>Ratio (F/MB)</th>';
													}
												?>
                                                <th>purchase Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <?php 
													if($offerPur->offer_type == 1)
													{
														echo '<td>'.$offerPur->credits.'</td>';
													}
													else
													{
														echo '<td>'.$offerPur->internet_volume.'</td>';
													}
												?>
												<td>
													<?php 
														$date= strtotime($offerPur->purchasedate);															
														echo date('d M Y', $date);
													?>
												</td>
												<?php 
													if($offerPur->offer_type == 1)
													{
														echo '<td>PHONE CREDITS</td>';
													}
													else
													{
														echo '<td>INTERNET VOLUME</td>';
													}
												?>
												<?php 
													if($offerPur->offer_type == 1)
													{
														$num2 = round($offerPur->promotion_type,2);
														echo '<td>'.$offerPur->network.'</td><td>'.$num2.'%</td>';
													}
													else
													{
														$num2 = $offerPur->selling_price/$offerPur->internet_volume;
														$whole2 = (int) $num2;  // 5
														$frac2  = $num2 - (int) $num2;  // .7	
														if($frac2 == 0)
														{
															echo '<td>'.$whole2.'</td>';
														}
														else
														{
															echo '<td>'.number_format($num2,2).'</td>';
														}
													}
												?>
												
												<td><?php echo $offerPur->selling_price;?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
									<?php } } else { ?>
									
									<div class="offer-block">																		
									<div class="top-head">
										No matching record found...
									
									</div>
									</div>
									<?php } ?>