<?php $this->load->view('include/header.php'); ?>
<?php $this->load->view('include/frontend-header.php'); ?>
<?php 
	if($this->session->userdata('userRefId'))
	{
		$userrefId = $this->session->userdata('userRefId');
		$networkDetailUser = 	getUserNetworkDetail($userrefId);
		
	}
	else
	{
		$userrefId = '';
	}
	
	$recentBlogDetail = getRecentBlogDetail();


?>
    <!-- Header Ends Here -->
    <section class="internal-banner" style="background:url(<?php echo site_url();?>assets/images/banner-image.jpg);">
        <div class="container">
            <h1>Blog</h1>
        </div>
    </section>
    <section class="blog-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="main-blogs">
                        <div class="blog-box full-blog">
                            <figure><img src="<?php echo site_url('assets/upload/images/'.$blogDetail->image);?>" alt="blog image"></figure>
                            <h2><?php echo ucfirst($blogDetail->title);?></h2>
                            <p><?php echo $blogDetail->description;?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <aside class="blog-sidebar">
                        <div class="srch-div">
                            <input type="text" placeholder="Search">
                            <button><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                        <div class="recent-posts">
                            <h2>Recent Posts</h2>
							<?php 
								foreach($recentBlogDetail as $vals) { 
									$string = substr($vals->description, 0, 300);
							
							?>
								
								<div class="post-block">
									<figure><img src="<?php echo site_url('assets/upload/images/'.$vals->image);?>"></figure>
									<figcaption>
										<h4><a href="<?php echo site_url('blog-internal-detail/'.$vals->id);?>"><?php echo ucfirst($vals->title);?></a></h4>
										<p><?php echo $string;?></p>
									</figcaption>
								</div>
								
							<?php } ?>
                           
                            
                            
                            
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('modal/thanku-popup.php');?>
<?php $this->load->view('include/frontend-footer.php');?>
<?php $this->load->view('modal/login-modal.php');?>
<?php $this->load->view('modal/otp-modal.php');?>
<?php $this->load->view('include/footer.php');?>
