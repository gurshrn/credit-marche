<?php $this->load->view('include/header.php'); ?>
<?php $this->load->view('include/frontend-header.php'); 

$id = $this->uri->segment(2);
$price = $this->uri->segment(3);


?>
<?php $userrefId = $this->session->userdata('userRefId'); ?>
<style>
.checked {
    color: orange;
}

</style>
 
    <section class="internal-banner" style="background:url('<?php echo site_url(); ?>assets/images/banner-image.jpg');">
        <div class="container">
            <h1>Phone Credit Trading Place</h1>
        </div>
    </section>
    <section class="market-rates-section" style="background:url('<?php echo site_url(); ?>assets/images/market-rate-bg.jpg');">
        <?php $this->load->view('frontend/home-promotion-sidebar.php');?>
    </section>
    <section class="dashboard">
        <div class="container">
            <h2 class="title"><span>my</span> Dashboard</h2>
            <div class="dashboard-tabbing">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link <?php if($id == ''){ echo 'active';}?>" id="pills-Published-tab" data-toggle="pill" href="#pills-Published" role="tab" aria-controls="pills-Published" aria-selected="true">Offers Published</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-Sold-tab" data-toggle="pill" href="#pills-Sold" role="tab" aria-controls="pills-Sold" aria-selected="false">Offers Sold</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-Offers-tab" data-toggle="pill" href="#pills-Offers" role="tab" aria-controls="pills-Offers" aria-selected="false">Offers Purchased</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($id != ''){ echo 'active';}?>" id="pills-virtual-tab" data-toggle="pill" href="#pills-virtual" role="tab" aria-controls="pills-virtual" aria-selected="false">virtual money</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-chat-tab" data-toggle="pill" href="#pills-chat" role="tab" aria-controls="pills-chat" aria-selected="false">Chat with us</a>
                    </li>
					 <li class="nav-item">
                        <a class="nav-link" id="pills-chat-tab" data-toggle="pill" href="#pills-notification" role="tab" aria-controls="pills-notification" aria-selected="false">Notifications</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade <?php if($id == ''){ echo 'show active';}?>" id="pills-Published" role="tabpanel" aria-labelledby="pills-Published-tab">
                        <div class="offer-srch-sec">
                            <form>
                                <div class="form-group">
                                    <label>Date of Publish</label>
                                    <div class="form-control dtpicker">
                                        <input id="datepicker3" class="offer-date" placeholder="Select">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Offer Type</label>
                                    <div class="form-control">
                                        <select class="offer-type">
                                             <option value="">select</option>
                                             <option value="1">Phone Credit</option>
                                             <option value="2">Internet Volume</option>
                                        </select>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label>Offer Status</label>
                                    <div class="form-control">
                                        <select class="offer-published-status">
                                             <option value="">select</option>
                                             <option value="1">Enabled</option>
                                             <option value="2">Disabled</option>
                                             <option value="3">Archieved</option>
                                             <option value="4">Excecuted</option>
                                        </select>
                                    </div>
                                </div>
                               

                                <div class="form-submit">
                                    <input type="button" class="frontend-search-offers" data-target="offer-published" value="Search">
                                </div>
                            </form>
                        </div>
                        <section class="boxes-container">
                            <div class="allOfferPublished">							
							<?php 								
								if(isset($userOffers) && !empty($userOffers)){
									foreach($userOffers as $val){
							?>
									<div class="offer-block">																		
									<div class="top-head">
											<h3>
					
											<?php 
												$networkDetailUsers = 	getUserNetworkDetail($val->userRefId);
												$getNetworkName = getNetworkName($networkDetailUsers->network_operator);
												echo $getNetworkName->network;
											
											?>
											</h3>
											<div class="btns-outer">												
											<?php 
													$expiry_time = date('Y-m-d H:i:s', strtotime($val->expiry_date));
													$expiry_date = date('Y-m-d H:i:s',strtotime('-6 hour',strtotime($expiry_time)));
													$currentdatetime = date('Y-m-d H:i:s');
													
													if($val->status == 3){ ?>
														
														<a class="btn buy-btn red-btn">Archived</a>
													
													<?php } if($val->status == 1) { ?>
														
														<a class="btn buy-btn enable-btn offerStatus disable<?php echo $val->id;?>" data-attr="2" data-id="<?php echo $val->id;?>">enabled</a>												
													<?php } if($val->status == 2){ ?>													
														<a class="btn buy-btn disable-btn offerStatus enable<?php echo $val->id;?>" data-attr="1" data-id="<?php echo $val->id;?>">disabled</a>												
													<?php } if($val->status == 4){?>
														
														<a class="btn buy-btn red-btn">Excecuted</a>
													
													<?php } if($val->status != 4 && $val->status != 5){ ?>
												
												<a href="<?php echo site_url('/publish-offer/'.$val->id);?>" class="btn buy-btn">edit offer</a>
													<?php } ?>
											</div>
										</div>
										
										<table>
											<thead>
												<tr>																									
												<?php if($val->offer_type == 1){ ?>															
													<th>Value of Credits (F)</th>													
													<?php } else{ ?>															
													<th>Internet Volume (mb)</th>													
													<?php } ?>
													<?php if($val->offer_type == 1){ ?>															
													<th>Expiration Date of Bonus Credits</th>													
													<?php } else{ ?>															
													<th>Expiration Date of Pass Internet</th>													
													<?php } ?>
																																												<?php if($val->offer_type == 1){ ?>
																																													<th>Credits valid on network</th>
																																												<?php } ?>																										
													<th>Offer Type</th>																										
													<?php if($val->offer_type == 2){ ?>															
													<th>Ratio (F/MB)</th>													
													<?php }?>	
													<?php if($val->offer_type == 1){ ?>	
														<th>Promotion Type</th>	
													<?php } ?>
													<th>Selling Price(F)</th>
													<th>Date of Publication</th>
													<th>Date of Expiration</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>														
													<?php 															
														if($val->offer_type == 1) { echo $val->credits;} 															
														if($val->offer_type == 2) { echo $val->internet_volume;} 														
													?>													
													</td>
													<td>														
													<?php
														
														if($val->expiry_date != '')
														{
															$expirydate= strtotime($val->expiry_date);															
															echo date('d M Y', $expirydate);
														}
														else
														{
															echo 'Unlimited';
														}
																												
													?>													
													</td>
													<?php if($val->offer_type == 1) { ?>
														<td>													
														<?php 
															if($val->credit_network_status == 0)
															{
																echo 'Any Network';
															}
															else
															{
																$networkDetail = getUserNetworkDetail($val->userRefId);
																$getNetworkName = getNetworkName($networkDetail->network_operator);
																echo ucfirst($getNetworkName->network);
															}
														
														?>	
														</td>	
													<?php } ?>
													<td>														
													<?php 															
														if($val->offer_type == 1) { echo 'PHONE CREDIT';} 															
														if($val->offer_type == 2) { echo 'INTERNET VOLUME';} 														
													?>													
													</td>
													<?php 															
														if($val->offer_type == 2) {													
													?>													
														<td>														
															<?php 
																 
																$num = $val->selling_price/$val->internet_volume;
																$whole = (int) $num;  // 5
																$frac  = $num - (int) $num;  // .7	
																if($frac == 0)
																{
																	echo $whole;
																}
																else
																{
																	echo number_format($num,2);
																}
															?>													
														</td>
													<?php } ?>
													<?php 															
														if($val->offer_type == 1) {													
													?>	
														<td>
															<?php 
								
																$num1 = round($val->promotion_type);
																echo $num1.'%';
															?>	
														</td>
													<?php } ?>
													<td><?php echo $val->selling_price;?></td>
													<td>														
													<?php															
														$addeddate= strtotime($val->addedondate);															
														echo date('d M Y', $addeddate);														
													?>													
													</td>
													<td>														
														<?php 
														
															if($val->expiry_date != '')
															{
																$now = date('Y-m-d');														
																$your_date = date('Y-m-d',strtotime($val->expiry_date));
																if($currentdatetime >= $expiry_date)
																{
																	echo 'Expired';
																}
																else
																{
																	$datetime1 = new DateTime($now);

																	$datetime2 = new DateTime($your_date);

																	$difference = $datetime1->diff($datetime2);

																	echo $difference->d.' days';
																}
															}
															else
															{
																echo 'Unlimited';
															}
																												
															
															
														?>													
													</td>
												</tr>
											</tbody>
										</table>
									</div>								
									<?php } } else { ?>
										<div class="offer-block">																		
											<div class="top-head">
												No matching record found...
									
											</div>
										</div>
									<?php } ?>
								
                                
                                
                                <!--div class="pagination">
                                    <img src="<?php //echo site_url();?>assets/images/pagination.jpg">
                                </div-->
                            </div>
                        </section>
                    </div>
                    <div class="tab-pane fade" id="pills-Sold" role="tabpanel" aria-labelledby="pills-Sold-tab">
                        <div class="offer-srch-sec">
                            <form>
                                <div class="form-group">
                                    <label>Date of Publish</label>
                                    <div class="form-control dtpicker">
                                        <input id="datepickers" placeholder="Select" class="sold-offer-date">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Offer Type</label>
                                    <div class="form-control">
                                        <select class="sold-offer-type">
                                             <option value="">select</option>
                                             <option value="1">Phone Credit</option>
                                             <option value="2">Internet Volume</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Network Operator</label>
                                    <div class="form-control">
                                        <select class="sold-network-operator">
                                              <option value="">select</option>
                                              <?php if(isset($creditNetwork) && !empty($creditNetwork)) { 
													foreach($creditNetwork as $val)
													{
											 
											 ?>
													 <option value="<?php echo $val->id;?>"><?php echo $val->network;?></option>
											<?php } } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-submit">
                                    <input type="button" class="frontend-search-offers" data-target="offer-sold" value="Search">
                                </div>
                            </form>
                        </div>
                        <section class="boxes-container">
                            <div class="allSoldOffer">
								<?php 								
									if(isset($offerSold) && !empty($offerSold)){
										foreach($offerSold as $val1){
								?>
											<div class="offer-block">
												<div class="top-head">
													<h3>
					
													<?php 
														$networkDetailUsers2 = 	getUserNetworkDetail($val1->userRefId);
														$getNetworkName2 = getNetworkName($networkDetailUsers2->network_operator);
														echo $getNetworkName2->network;
													
													?>
													</h3>
												</div>
												<table>
													<thead>
														<tr>
															<?php 
																if($val1->offer_type == 1)
																{
																	echo '<th>VALUE OF CREDITS (F)</th>';
																}
																else
																{
																	echo '<th>Internet Volume (mb)</th>';
																}
															?>
															<th>Date sold</th>
															<th>Offer Type</th>
															<?php 
																if($val1->offer_type == 2)
																{
																	echo '<th>Ratio (F/MB)</th>';
																}
															?>
															<?php 
																if($val1->offer_type == 1)
																{
																	echo '<th>Promotion Type</th>';
																}
															?>
															<th>Selling Price(F)</th>
															<th>Date of Publication</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<?php 
																if($val1->offer_type == 1)
																{
																	echo '<td>'.$val1->credits.'</td>';
																}
																else
																{
																	echo '<td>'.$val1->internet_volume.'</td>';
																}
															?>
															<td>
																<?php															
																	$offersolddate= strtotime($val1->offersolddate);															
																	echo date('d M Y', $offersolddate);														
																?>	
															</td>
															<td>
																<?php
																	if($val1->offer_type == 1)
																	{
																		echo 'Phone credit';
																	}
																	else
																	{
																		echo 'Internet Volume';
																	}
																
																?>
															
															</td>
															<?php if($val1->offer_type == 2){ ?>
															<td>
																<?php 
																	$num1 = $val1->selling_price/$val1->internet_volume;
																	$whole1 = (int) $num1;  // 5
																	$frac1  = $num1 - (int) $num1;  // .7	
																	if($frac1 == 0)
																	{
																		echo $whole1;
																	}
																	else
																	{
																		echo number_format($num1,2);
																	}
																?>	
															</td>
															<?php }?>
															<?php 
																if($val1->offer_type == 1)
																{
																	$num1 = round($val1->promotion_type);
																	echo '<td>'.$num1.'%</td>';
																}
															?>
															<td><?php echo $val1->selling_price;?></td>
															<td>
																<?php															
																	$addedondates= strtotime($val1->addedondate);															
																	echo date('d M Y', $addedondates);														
																?>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
								<?php } } else { ?>
										<div class="offer-block">																		
											<div class="top-head">
												No matching record found...
									
											</div>
										</div>
								<?php } ?>
                               
                               
                            </div>
                        </section>
                    </div>
                    <div class="tab-pane fade" id="pills-Offers" role="tabpanel" aria-labelledby="pills-Offers-tab">				
                        <div class="offer-srch-sec">
                            <form>
                                <div class="form-group">
                                    <label>Date of Publish</label>
                                    <div class="form-control dtpicker">
                                        <input id="datepicker4" class="offer-dates" placeholder="Select">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Offer Type</label>
                                    <div class="form-control">
                                        <select class="offer-types">
                                             <option value="">select</option>
                                             <option value="1">Phone Credit</option>
                                             <option value="2">Internet Volume</option>
                                        </select>
                                    </div>
                                </div>
                                <!--div class="form-group">
                                    <label>Network Operator</label>
                                    <div class="form-control">
                                        <select class="network-operators">
                                              <option value="">select</option>
                                              <?php //if(isset($creditNetwork) && !empty($creditNetwork)) { 
													//foreach($creditNetwork as $val)
													//{
											 
											 ?>
													 <option value="<?php //echo $val->id;?>"><?php// echo $val->network;?></option>
											<?php //} } ?>
                                        </select>
                                    </div>
                                </div-->

                                <div class="form-submit">
                                    <input type="button" class="frontend-search-offers" data-target="offer-purchased" value="Search">
                                </div>
                            </form>
                        </div>
                        <section class="boxes-container">
						
							<div class="allPurchasedOffer">
								<?php 
									if(isset($offerPurchased) && !empty($offerPurchased))
									{
										foreach($offerPurchased as $offerPur)
										{
											$selectedTime = date('Y-m-d H:i:s', strtotime($offerPur->createdDateTime));
											$endTime = date('Y-m-d H:i:s',strtotime('+5 min',strtotime($selectedTime)));
											$currentdatetime = date('Y-m-d H:i:s');
								?>
                                <div class="offer-block">
                                    <div class="top-head">
                                        <h3>
					
											<?php 
												$networkDetailUsers3 = 	getUserNetworkDetail($offerPur->userRefId);
												$getNetworkName3 = getNetworkName($networkDetailUsers3->network_operator);
												echo $getNetworkName3->network;
											
											?>
											</h3>
										
											<div class="btns-outer">
												<?php 
													$userdetails = getUserDetail($offerPur->buyerRefId); 
													$tel_number = $userdetails->tel_number;
													$paymentDetail = getPaymentDetail($offerPur->transactionId,$offerPur->buyerRefId);
											
													$offerId = $offerPur->id;
													$buyerRefId = $offerPur->buyerRefId;
													$sellrefId = $offerPur->userRefId;
													$notifyMsg = $tel_number.' '.'offer selling price'.' '.$offerPur->selling_price.' '.'purchasing Price'.' '.$paymentDetail->debit;
												?>
												
												<?php if($offerPur->offer_confirmation == 0) { if($currentdatetime >= $endTime){?>
												
													<a class="btn buy-btn red-btn creditsClaim" data-offer="<?php echo $offerId;?>" data-from="<?php echo $buyerRefId?>" data-to="<?php echo $sellrefId?>" data-msg="<?php echo $notifyMsg;?>">claim credits</a>
													
												<?php } ?>
												
													<a class="btn buy-btn credit-recieve" data-id=<?php echo $offerPur->id;?> data-target="<?php echo $offerPur->userRefId;?>">Credit Received</a>
												
												<?php } ?>
												
											</div>
										
										
                                    </div>
                                    <table>
                                        <thead>
                                            <tr>
												<?php 
													if($offerPur->offer_type == 1)
													{
														echo '<th>Value of Credits (F)</th>';
													}
													else
													{
														echo '<th>Internet Volume (mb)</th>';
													}
												?>
                                                <th>purchase Date</th>
                                                <th>Offer Type</th>
												<?php 
													if($offerPur->offer_type == 1)
													{
														echo '<th>Credits valid on network</th><th>Promotion Type</th>';
													}
													else
													{
														echo '<th>Ratio (F/MB)</th>';
													}
												?>
                                                <th>purchase Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <?php 
													if($offerPur->offer_type == 1)
													{
														echo '<td>'.$offerPur->credits.'</td>';
													}
													else
													{
														echo '<td>'.$offerPur->internet_volume.'</td>';
													}
												?>
												<td>
													<?php 
														$date= strtotime($offerPur->purchasedate);															
														echo date('d M Y', $date);
													?>
												</td>
												<?php 
													if($offerPur->offer_type == 1)
													{
														echo '<td>PHONE CREDITS</td>';
													}
													else
													{
														echo '<td>INTERNET VOLUME</td>';
													}
												?>
												<?php 
													if($offerPur->offer_type == 1)
													{
														$num2 = round($offerPur->promotion_type,2);
														echo '<td>'.$offerPur->network.'</td><td>'.$num2.'%</td>';
													}
													else
													{
														$num2 = $offerPur->selling_price/$offerPur->internet_volume;
														$whole2 = (int) $num2;  // 5
														$frac2  = $num2 - (int) $num2;  // .7	
														if($frac2 == 0)
														{
															echo '<td>'.$whole2.'</td>';
														}
														else
														{
															echo '<td>'.number_format($num2,2).'</td>';
														}
													}
												?>
												
												<td><?php echo $offerPur->selling_price;?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
									<?php } } else { ?>
									
									<div class="offer-block">																		
									<div class="top-head">
										No matching record found...
									
									</div>
									</div>
									<?php } ?>
                                
                            </div>
                        </section>
                    </div>
                    <div class="tab-pane fade <?php if($id != ''){ echo 'active show';}?>" id="pills-virtual" role="tabpanel" aria-labelledby="pills-virtual-tab">
                        <div class="history-block">
                            <div class="offer-block" <?php if($id != ''){ echo 'style="display:none"';}?>>
                                <div class="top-head">
                                    <h3>History</h3>
                                    <div class="btns-outer">
                                        <a class="btn buy-btn withdraw-money">Withdraw Money</a>
                                    </div>
                                </div>
                                <table>
                                    <thead>
                                        <tr>
                                        
                                            <th>Transaction Type</th>
                                            <th>Debit</th>
                                            <th>Credit</th>
                                            <th>Date</th>
                                            <th>time</th>
                                            <th>ID Transaction</th>
                                            <th>Running Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php if(isset($creditTransaction) && !empty($creditTransaction)){
												foreach($creditTransaction as $val1){
										?>
													<tr>
													   
														<td><?php echo $val1->transaction_type;?></td>
														<td><?php echo $val1->debit;?></td>
														<td><?php echo $val1->credit;?></td>
														<td>
															<?php 
																$date= strtotime($val1->date);															
																echo date('d M Y', $date);
															?>
														</td>
														<td><?php echo $val1->time;?></td>
														<td><?php echo $val1->transaction_id;?></td>
														<td><?php echo $val1->running_balance;?></td>
													</tr>
										<?php } } else{ ?>
											<tr>
												<td colspan="7"></td>
											</tr>
										
										<?php } ?>
                                        

                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class="offer-block fund-account" <?php if($id != ''){ echo 'style="display:block"';}?>>
								<div class="alert alert-danger">
									Your wallet not have sufficient balance.
								</div>
								<input type="hidden" class="offerId" value="<?php echo $id;?>">
								<input type="hidden" class="offerSellingPrice" value="<?php echo $price;?>">
                                <p>Please Fund Your Account with <?php echo $price;?>F</p>
                                <a href="javacsript:void(0)" class="btn fund-btn fund-modal">Fund</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-chat" role="tabpanel" aria-labelledby="pills-chat-tab">
					
						<body class="btm_sp">
    <!-- Header area -->
    <section class="home_main_wrap">
        <div class="home_wrap_mid_cntnt">
            <div class="message-chat-block">
                <div class="users-detail-block">
                   

                </div>
                <div class="clr"></div>
                <div class="overall-chat-block individual-chat">
					<?php if(isset($chatHistory) && !empty($chatHistory)) {
							foreach($chatHistory as $val){?>
                    <div class="<?php if($userrefId==$val->msg_from){ echo 'your-msg-block'; }else{ echo 'user-msg-block'; } ?>">
						<?php 
							if($userrefId!=$val->msg_from){ 
								if($val->msg_from!='Admin')
								{
									$user = getUserDetail($val->msg_from);
									$username = ucwords($user->first_name);
								}
								else
								{
									$username = 'Admin';
								} 
						?>
							<span class="username"><?php echo $username;?></span>
						
						<?php } ?>
                        
                        <div class="user-msg">
                            <p>
                                <span><?php echo $val->message; ?><time class="msgtime"><?php echo $val->msg_date.' '.$val->msg_time;?></time></span>
                            </p>
                        </div>
						<?php if($userrefId==$val->msg_from){ 
								if($val->msg_from!='Admin')
								{
									$user = getUserDetail($val->msg_from);
									$username = ucwords($user->first_name);
								}
								else
								{
									$username = 'Admin';
								} 
						
						?>
							<span class="username"><?php echo $username;?></span>
						<?php } ?>
					</div>
					<?php } } ?>
                    
                </div>
            </div>
            <div class="type-section">
				<form method="post" id="chat_form">
					<div class="enter-txt-msg">
						<input type="text" name="txtmsg" id="txtmsg" class="contactBtnQuote" placeholder="Add your text. . .">
						<input type="hidden" name="msgfrom" id="msgfrom" value="<?php echo $userrefId; ?>">
						<input type="hidden" name="msgto" id="msgto" value="<?php echo 'Admin'; ?>">
					</div>
				</form>
                
            </div>
        </div>

    </section>
					
					</div>
					 <div class="tab-pane fade" id="pills-notification" role="tabpanel" aria-labelledby="pills-notification">
					
						<section class="boxes-container">
                            <div class="allSoldOffer">
								
											<div class="offer-block">
												<div class="top-head">
													<h3>Notifications</h3>
												</div>
												<table>
													<thead>
														<tr>
															<th>Notification From</th>
															<th>Notification Message</th>
														</tr>
													</thead>
													<tbody>
													<?php 								
														if(isset($notification) && !empty($notification)){
															foreach($notification as $vals1){
													?>
														<tr>
															<td>Buyer</td>
															<td><?php echo ucfirst($vals1->notification_msg);?></td>
															
														</tr>
													
														<?php } } else { ?>
															<tr colspan="2"><td>No matching record found...</td></tr>
										
																
														<?php } ?>
													</tbody>
													</table>
														
													
											</div>
								
                               
                               
                            </div>
                        </section>
					
					</div>
                </div>
            </div>
        </div>
    </section>
	<div class="modal fade login-reg" id="payFund" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></a>
                    <h2>Fund Your Account</h2>
                    <p>You need to fund your account to buy Credits or Internet Bandwidth</p>
                </div>
                <div class="modal-body">
                    <div class="amount-div">
                        <form id="paymentExpress" action="<?php echo site_url('payment-gateway');?>" method="post" autocomplete="off">
							<input type="hidden" name="offerid" class="offerids">
                            <div class="amount-enter">
                                <label>enter amount</label>
                                <input type="text" name="sellingPrice" class="sellingAmount validNumber">
                                <p>*You can Fund your Account more than Credit’s Sale Price</p>
                            </div>
                            <div class="select-amount">
                                <h4>Select Amount</h4>
                                <div class="amnts">
                                    <p>
                                        <input type="radio" id="test1" class="checkedAmount" name="radio-group" value="1000">
                                        <label for="test1">1000</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test2" class="checkedAmount" name="radio-group" value="2000">
                                        <label for="test2">2000</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test3" class="checkedAmount" name="radio-group" value="3000">
                                        <label for="test3">3000</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test4" class="checkedAmount" name="radio-group" value="4000">
                                        <label for="test4">4000</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test5" class="checkedAmount" name="radio-group" value="5000">
                                        <label for="test5">5000</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test6" class="checkedAmount" name="radio-group" value="6000">
                                        <label for="test6">6000</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test7" class="checkedAmount" name="radio-group" value="7000">
                                        <label for="test7">7000</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test8" class="checkedAmount" name="radio-group" value="8000">
                                        <label for="test8">8000</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test9" class="checkedAmount" name="radio-group" value="9000">
                                        <label for="test9">9000</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test10" class="checkedAmount" name="radio-group" value="10000">
                                        <label for="test10">10,000</label>
                                    </p>
                                </div>
                                <a class="paynow-btn" href="javascript:void(0)">paynow</a>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
	
<!-- Claim recieving modal -->

	<div id="credit-recieved" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					  ×
					</button>
					<h4 class="modal-title" id="classModalLabel">
					  Credit Recieving
					</h4>
				</div>
				<div class="modal-body offerHistory">
					<input type="hidden" class="offers-id">
					<input type="hidden" class="user-ref">
					<input type="hidden" class="rating">
					<h5>Click to rate:</h5>
					<div class='starrr' id='star1'></div>
					<div>&nbsp;
					  <span class='your-choice-was' style='display: none;'>
						Your rating was <span class='choice'></span>.
					  </span>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary offerrating">Submit</button>
				</div>
			</div>
		</div>
	</div>
	
<!-- Claim credits modal -->

	<div id="claim-credits" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					  ×
					</button>
					<h4 class="modal-title" id="classModalLabel">
					 Claim Credits
					</h4>
				</div>
				
				<div class="modal-body">
					<input type="hidden" name="notifyFrom" class="notifyFrom">
					<input type="hidden" name="notifyTo" class="notifyTo">
					<input type="hidden" name="notifyMsg" class="notifyMsg">
					<input type="hidden" name="offerId" class="offerId">
					Are you sure you want to credit claim ?
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-primary claimingCredits">Yes</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					
				</div>
			</div>
		</div>
	</div>
	
<!-- Withdraw money modal -->	

	<div class="modal fade" id="withdrawMoney" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Withdraw Money</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <?php $walletdetail = getWalletDetail($userrefId);?>
		  <div class="modal-body">
			<form id="withdrawMoneyRequest" method="POST" action="<?php site_url('/withdraw-money');?>" autocomplete="off">
				<input type="hidden" class="wallet-amount validNumber" value="<?php echo  $walletdetail[0]->wallet_amount;?>">
				<div class="form-group">
					<label for="recipient-name" class="col-form-label">Amount</label>
					<input type="text" class="form-control" name="withdraw_amount" id="withdraw-amount">
				</div>
			
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary withdray-amounts">Submit</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>

<?php $this->load->view('modal/modal.php');?>
<?php $this->load->view('include/frontend-footer.php');?>
<?php $this->load->view('include/footer.php');?>
<script>
$('#star1').starrr({
		change: function(e, value){
			$('.rating').val(value);
		}
    });

</script>
<script>
  $(function () {
    $('#abc').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
