<?php 								
								if(isset($userOffers) && !empty($userOffers)){
									foreach($userOffers as $val){
							?>
									<div class="offer-block">																		
									<div class="top-head">
											<h3>
					
											<?php 
												$networkDetailUsers = 	getUserNetworkDetail($val->userRefId);
												$getNetworkName = getNetworkName($networkDetailUsers->network_operator);
												echo $getNetworkName->network;
											
											?>
											</h3>
											<div class="btns-outer">												
											<?php 
													$expiry_time = date('Y-m-d H:i:s', strtotime($val->expiry_date));
													$expiry_date = date('Y-m-d H:i:s',strtotime('-6 hour',strtotime($expiry_time)));
													$currentdatetime = date('Y-m-d H:i:s');
													
													if($val->status == 3){ ?>
														
														<a class="btn buy-btn red-btn">Archived</a>
													
													<?php } if($val->status == 1) { ?>
														
														<a class="btn buy-btn enable-btn offerStatus disable<?php echo $val->id;?>" data-attr="2" data-id="<?php echo $val->id;?>">enabled</a>												
													<?php } if($val->status == 2){ ?>													
														<a class="btn buy-btn disable-btn offerStatus enable<?php echo $val->id;?>" data-attr="1" data-id="<?php echo $val->id;?>">disabled</a>												
													<?php } if($val->status == 4){?>
														
														<a class="btn buy-btn red-btn">Excecuted</a>
													
													<?php } if($val->status != 4 && $val->status != 5){ ?>
												
												<a href="<?php echo site_url('/publish-offer/'.$val->id);?>" class="btn buy-btn">edit offer</a>
													<?php } ?>
											</div>
										</div>
										
										<table>
											<thead>
												<tr>																									
												<?php if($val->offer_type == 1){ ?>															
													<th>Value of Credits (F)</th>													
													<?php } else{ ?>															
													<th>Internet Volume (mb)</th>													
													<?php } ?>
													<?php if($val->offer_type == 1){ ?>															
													<th>Expiration Date of Bonus Credits</th>													
													<?php } else{ ?>															
													<th>Expiration Date of Pass Internet</th>													
													<?php } ?>
																																												<?php if($val->offer_type == 1){ ?>
																																													<th>Credits valid on network</th>
																																												<?php } ?>																										
													<th>Offer Type</th>																										
													<?php if($val->offer_type == 2){ ?>															
													<th>Ratio (F/MB)</th>													
													<?php }?>	
													<?php if($val->offer_type == 1){ ?>	
														<th>Promotion Type</th>	
													<?php } ?>
													<th>Selling Price(F)</th>
													<th>Date of Publication</th>
													<th>Date of Expiration</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>														
													<?php 															
														if($val->offer_type == 1) { echo $val->credits;} 															
														if($val->offer_type == 2) { echo $val->internet_volume;} 														
													?>													
													</td>
													<td>														
													<?php
														
														if($val->expiry_date != '')
														{
															$expirydate= strtotime($val->expiry_date);															
															echo date('d M Y', $expirydate);
														}
														else
														{
															echo 'Unlimited';
														}
																												
													?>													
													</td>
													<?php if($val->offer_type == 1) { ?>
														<td>													
														<?php 
															if($val->credit_network_status == 0)
															{
																echo 'Any Network';
															}
															else
															{
																$networkDetail = getUserNetworkDetail($val->userRefId);
																$getNetworkName = getNetworkName($networkDetail->network_operator);
																echo ucfirst($getNetworkName->network);
															}
														
														?>	
														</td>	
													<?php } ?>
													<td>														
													<?php 															
														if($val->offer_type == 1) { echo 'PHONE CREDIT';} 															
														if($val->offer_type == 2) { echo 'INTERNET VOLUME';} 														
													?>													
													</td>
													<?php 															
														if($val->offer_type == 2) {													
													?>													
														<td>														
															<?php 
																 
																$num = $val->selling_price/$val->internet_volume;
																$whole = (int) $num;  // 5
																$frac  = $num - (int) $num;  // .7	
																if($frac == 0)
																{
																	echo $whole;
																}
																else
																{
																	echo number_format($num,2);
																}
															?>													
														</td>
													<?php } ?>
													<?php 															
														if($val->offer_type == 1) {													
													?>	
														<td>
															<?php 
								
																$num1 = round($val->promotion_type);
																echo $num1.'%';
															?>	
														</td>
													<?php } ?>
													<td><?php echo $val->selling_price;?></td>
													<td>														
													<?php															
														$addeddate= strtotime($val->addedondate);															
														echo date('d M Y', $addeddate);														
													?>													
													</td>
													<td>														
														<?php 
														
															if($val->expiry_date != '')
															{
																$now = date('Y-m-d');														
																$your_date = date('Y-m-d',strtotime($val->expiry_date));
																if($currentdatetime >= $expiry_date)
																{
																	echo 'Expired';
																}
																else
																{
																	$datetime1 = new DateTime($now);

																	$datetime2 = new DateTime($your_date);

																	$difference = $datetime1->diff($datetime2);

																	echo $difference->d.' days';
																}
															}
															else
															{
																echo 'Unlimited';
															}
																												
															
															
														?>													
													</td>
												</tr>
											</tbody>
										</table>
									</div>								
									<?php } } else { ?>
										<div class="offer-block">																		
											<div class="top-head">
												No matching record found...
									
											</div>
										</div>
									<?php } ?>