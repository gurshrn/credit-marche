<?php

	if (!defined('BASEPATH'))
    exit('No direct script access allowed');

	function getUserDetail($userRefId = null) {
	    $ci = & get_instance();
		$ci->db->select('*,AES_DECRYPT(email,"/*awshp$*/") as email,AES_DECRYPT(tel_number,"/*awshp$*/") as tel_number');
	    $ci->db->from('credit_login_detials');
	    $ci->db->where('userRefId', $userRefId);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function getUserNetworkDetail($userRefId = null) {
	    $ci = & get_instance();
		$ci->db->select('network_operator');
	    $ci->db->from('credit_user_detail');
	    $ci->db->where('userRefId', $userRefId);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function getNetworkName($networkId =null)
	{
		 $ci = & get_instance();
		$ci->db->select('network');
	    $ci->db->from('credit_networks');
	    $ci->db->where('id', $networkId);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function getPaymentDetail($transactionId,$userrefId) {
	    $ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('credit_payment');
	    $ci->db->where('userRefId', $userrefId);
	    $ci->db->where('transaction_id', $transactionId);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function getWalletDetail($userRefId = null) {
	    $ci = & get_instance();
	    $ci->db->select('wallet_amount');
	    $ci->db->from('credit_wallet');
	    $ci->db->where('userRefId', $userRefId);
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}
	function calculatePromotion($networkid,$offerType,$date)
	{
		$ci = & get_instance();
	    $ci->db->select('credit_offer_purchased.*,Sum(credit_publish_offer.internet_volume) as totalInternetVolume,Sum(credit_publish_offer.credits) as totalCredits,Sum(credit_publish_offer.selling_price) as totalSellingPrice');
	    $ci->db->from('credit_offer_purchased');
	    $ci->db->join('credit_publish_offer','credit_publish_offer.id = credit_offer_purchased.offerId','left');
	    $ci->db->where('credit_offer_purchased.addedondate', $date);
	    $ci->db->where('credit_publish_offer.offer_type',$offerType);
	    $ci->db->where('credit_publish_offer.credit_network',$networkid);
	    $query = $ci->db->get();
		$result = $query->result();
	    return $result;
	}
	function generateRef()
	{
		$lengths = 8;
		return $randomStrings = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $lengths);
	}
	function getCommissionDetail()
	{
		$ci = & get_instance();
	    $ci->db->select('*');
	    $ci->db->from('credit_commission');
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function countUserRating($userRefId)
	{
		$ci = & get_instance();
	    $ci->db->select('rating');
	    $ci->db->from('credit_offer_rating');
	    $ci->db->where('userRefId',$userRefId);
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}
	function sumUserRating($userRefId)
	{
		$ci = & get_instance();
	    $ci->db->select('Sum(rating) as total');
	    $ci->db->from('credit_offer_rating');
	    $ci->db->where('userRefId',$userRefId);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function userSoldOffer($userRefId)
	{
		$ci = & get_instance();
	    $ci->db->select('*');
	    $ci->db->from('credit_payment');
	    $ci->db->where('userRefId',$userRefId);
	    $ci->db->where('transaction_type','OfferSold');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}
	function getFooterList()
	{
		$ci = & get_instance();
	    $ci->db->select('*');
	    $ci->db->from('credit_frontend_footer');
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}
	function getRecentBlogDetail()
	{
		$ci = & get_instance();
	    $ci->db->select('*');
	    $ci->db->from('credit_blog');
	    $ci->db->order_by('id','DESC');
	    $ci->db->limit(5);
	    $query = $ci->db->get();
	    $result = $query->result();
	    return $result;
	}
	function updateStandByStatus()
	{
		$ci = & get_instance();
	    $ci->db->select('*');
	    $ci->db->from('credit_login_detials');
	    $ci->db->where('user_active',1);
	    $ci->db->where('status',1);
	    $query = $ci->db->get();
	    $result = $query->result();
		foreach($result as $val)
		{
			 $ci = & get_instance();
			 $ci->db->select('credit_publish_offer.*,credit_offer_purchased.createdDateTime,credit_offer_purchased.addedondate');
			 $ci->db->from('credit_publish_offer');
			 $ci->db->join('credit_offer_purchased','credit_offer_purchased.offerId = credit_publish_offer.id','inner');
			 $ci->db->where('credit_offer_purchased.addedondate',date('Y-m-d'));
			 $ci->db->where('credit_publish_offer.status',4);
			 $ci->db->where('credit_publish_offer.userRefId',$val->userRefId);
			 $ci->db->order_by('credit_offer_purchased.id','ASC');
			 $ci->db->group_by('credit_publish_offer.id');
			 $query = $ci->db->get();
			 $results[] = $query->result();
		}
		
		foreach($results as $val1)
		{
			
			if(!empty($val1) && count($val1) == 3)
			{
				$purshasedate = $val1[0]->createdDateTime;
				$afterOneDay = date('Y-m-d H:i:s',strtotime('+1 day',strtotime($val1[0]->createdDateTime)));
				$userRefId = $val1[0]->userRefId;
				$currentdatetime = date('Y-m-d H:i:s');
				if($currentdatetime >= $afterOneDay)
				{
					print_r('fds');
					$updateStatus = array('status'=>1);
					$this->db->where('userRefId',$userRefId);
					$this->db->update('credit_publish_offer',$updateStatus);
				}
			}
		}
		return $result;
		
	}
	


?>