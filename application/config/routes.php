<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home_controller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*==============Home controller route================*/


$route['view-offer'] = 'Home_controller/viewOffer';
$route['publish-offer'] = 'Home_controller/publishOffer';
$route['publish-offer/(:any)'] = 'Home_controller/publishOffer/$1';
$route['check-email'] = 'Home_controller/checkEmailExist';
$route['publish-credit-offer'] = 'Home_controller/publishCreditOffer';
$route['publish-internet-offer'] = 'Home_controller/publishInternetOffer';
$route['payment-gateway'] = 'Home_controller/paymentGateway';
$route['payment-gateway/payment-success'] = 'Home_controller/paymentSuccess';
$route['offer-history'] = 'Home_controller/getOfferDetailById';
$route['purchase-offer'] = 'Home_controller/purchaseOffer';
$route['frontend-search-offers'] = 'Home_controller/searchOffers';
$route['payment-ipn'] = 'Payment_controller/index';
$route['contact-form'] = 'Home_controller/contactForm';
$route['footer-detail/(:any)'] = 'Home_controller/getFooterDetail/$1';

/*========Frontend Dashboard controller route========*/

$route['dashboard'] = 'Dashboard_controller/index';
$route['dashboard/(:any)/(:any)'] = 'Dashboard_controller/index/$1/$1';
$route['update-offer-status'] = 'Dashboard_controller/updateOfferStatus';
$route['offer-rating'] = 'Dashboard_controller/offerRating';
$route['send-chat-message'] = 'Dashboard_controller/sendChatMsg';
$route['claim-credits'] = 'Dashboard_controller/creditsClaim';
$route['withdraw-money'] = 'Dashboard_controller/withdrawMoney';

/*==============Login and signup route==============*/

$route['user-signup'] = 'User_controller/userSignup';
$route['check-otp'] = 'User_controller/checkOtp';
$route['user-login'] = 'User_controller/userLogin';
$route['logout'] = 'User_controller/logout';
$route['login'] = 'Admin_controller/index';

/*=====================Admin routes===============*/

$route['admin'] = 'Admin_controller/index';
$route['admin/login'] = 'Admin_controller/loginAdmin';
$route['admin/logout'] = 'Admin_controller/logout';
$route['admin/users'] = 'Admin_controller/users';
$route['admin/dashboard'] = 'Admin_controller/dashboard';
$route['admin/update-status'] = 'Admin_controller/updateStatus';
$route['admin/offers'] = 'Admin_controller/getAllOffers';
$route['admin/admin-view-offers'] = 'Admin_controller/getOfferDetailById';
$route['admin/offer-detail-by-status'] = 'Admin_controller/getOfferDetailByStatus';
$route['admin/credits-network'] = 'Admin_controller/getCreditNetwork';
$route['admin/delete-table-row'] = 'Admin_controller/commonDelete';
$route['admin/add-credit-network'] = 'Admin_controller/addCreditNetwork';
$route['admin/add-network-operator'] = 'Admin_controller/addNetworkOperator';
$route['admin/edit-credit-network'] = 'Admin_controller/commonEdit';
$route['admin/network-operator'] = 'Admin_controller/getNetworkOpertaor';
$route['admin/commission-percent'] = 'Admin_controller/getCommisionValues';
$route['admin/add-credit-commission'] = 'Admin_controller/addCommission';
$route['admin/edit-credit-commission'] = 'Admin_controller/commonEdit';
$route['admin/commission-by-user'] = 'Admin_controller/commissionByUser';
$route['admin/transaction-history'] = 'Admin_controller/getTransactionDetail';
$route['admin/chat-messages'] = 'Admin_controller/getMessagesDetail';
$route['admin/search-transaction'] = 'Admin_controller/searchTransaction';
$route['admin/getChatDetail'] = 'Admin_controller/getChatHistory';
$route['admin/footer-section'] = 'Admin_controller/footerList';
$route['admin/blog'] = 'Admin_controller/blogList';
$route['admin/add-blog'] = 'Admin_controller/addBlog';
$route['admin/faq'] = 'Admin_controller/faqList';
$route['admin/add-faq'] = 'Admin_controller/addFaq';
$route['blog-detail'] = 'Admin_controller/blogDetail';
$route['faq-detail'] = 'Admin_controller/faqDetail';
$route['blog-internal-detail/(:any)'] = 'Admin_controller/blogInternalDetail/$1';
$route['admin/add-footer-detail'] = 'Admin_controller/addFooterDetail';
$route['admin/edit-footer/(:any)'] = 'Admin_controller/editFooterDetail/$1';
$route['admin/transaction-history/(:any)'] = 'Admin_controller/getTransactionDetail/$1';
$route['admin/admin-edit-offers/(:any)'] = 'Admin_controller/editOfferDetail/$1';
$route['admin/edit-blog/(:any)'] = 'Admin_controller/editBlog/$1';
$route['admin/edit-faq/(:any)'] = 'Admin_controller/editFaq/$1';



