<?php 
if (!defined('BASEPATH'))

    exit('No direct script access allowed');

class Dashboard extends CI_Model {



    public function __construct() {

        parent::__construct();

    }
	
	public function getOffersByUserRef()
	{
		$userRefId = $this->session->userdata('userRefId');
		$this->db->select('credit_publish_offer.*,credit_networks.network');
        $this->db->from('credit_publish_offer');
        $this->db->join('credit_networks','credit_networks.id = credit_publish_offer.credit_network','left');
        $this->db->where('credit_publish_offer.userRefId',$userRefId);
        $this->db->where('credit_publish_offer.status !=',3);
        $this->db->order_by('credit_publish_offer.id','desc');
        $result = $this->db->get();
        $result = $result->result();
        return $result;	
	}
	public function updateStatus($data)
	{
		$status = array('status' => $data['status']);
		$this->db->where('id',$data['id']);
		$this->db->update('credit_publish_offer',$status);
		$db_error = $this->db->error();		
		if ($db_error['code'] != 0) 		
		{			
			return false;		
		} 		
		else		
		{			
			return true;		
		}		
		
	}
	public function getTransactionDetail()
	{
		$userRefId = $this->session->userdata('userRefId');
		$this->db->select('*');
        $this->db->from('credit_payment');
        $this->db->where('userRefId',$userRefId);
        $this->db->order_by('id','ASC');
		$result = $this->db->get();
        $result = $result->result();
		return $result;
	}
	public function getOfferPurchaseDetail()
	{
		$userRefId = $this->session->userdata('userRefId');
		$this->db->select('credit_publish_offer.*,credit_networks.network,credit_offer_purchased.transactionId,credit_offer_purchased.userRefId as buyerRefId,credit_offer_purchased.addedondate as purchasedate,credit_offer_purchased.createdDateTime,credit_offer_purchased.offer_confirmation');
        $this->db->from('credit_publish_offer');
        $this->db->join('credit_networks','credit_networks.id = credit_publish_offer.credit_network','left');
        $this->db->join('credit_offer_purchased','credit_offer_purchased.offerId = credit_publish_offer.id','left');
        $this->db->where('credit_offer_purchased.userRefId',$userRefId);
        $this->db->order_by('credit_publish_offer.id','desc');
        $result = $this->db->get();
		$result = $result->result();
        return $result;	
	}
	public function getOfferSoldDetail()
	{
		$userRefId = $this->session->userdata('userRefId');
		$this->db->select('credit_publish_offer.*,credit_networks.network,credit_offer_purchased.addedondate as offersolddate');
        $this->db->from('credit_publish_offer');
        $this->db->join('credit_networks','credit_networks.id = credit_publish_offer.credit_network','left');
        $this->db->join('credit_offer_purchased','credit_offer_purchased.offerId = credit_publish_offer.id','left');
        $this->db->where('credit_publish_offer.userRefId',$userRefId);
        $this->db->where('credit_publish_offer.status',4);
		$this->db->order_by('credit_publish_offer.id','desc');
        $result = $this->db->get();
        $result = $result->result();
        return $result;	
	}
	public function saveOfferRating($data)
	{
		$ratingBy = $this->session->userdata('userRefId');
		$data = array(
			'ratingBy'=>$ratingBy,
			'userRefId'=>$data['userRefId'],
			'offerId'=>$data['offerId'],
			'rating'=>$data['rating'],
			'addedondate'=>date('Y-m-d'),
			);
		$this->db->insert('credit_offer_rating',$data);
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{
			$data1 = array('offer_confirmation'=>1);
			$this->db->where('userRefId',$ratingBy);
			$this->db->where('offerId',$data['offerId']);
			$this->db->update('credit_offer_purchased',$data1);
			return true;
		}
		else
		{
			return false;
		}
	}
	public function sendMessage($msgDetail)
	{
		if(isset($msgDetail)){
			$this->db->insert('chat_detail',$msgDetail);
			$insert_id = $this->db->insert_id();
			$this->db->select('*');
			$this->db->from('chat_detail');
			$this->db->where('id',$insert_id);
			$result = $this->db->get();
			$result = $result->row();
			return $result;
		}
	}
	public function getChatHistory()
	{
		$userrefId = $this->session->userdata('userRefId');
		$this->db->select('*');
		$this->db->from('chat_detail');
		$this->db->where('msg_from',$userrefId);
		$this->db->or_where('msg_to',$userrefId);
		$result = $this->db->get();
		$result = $result->result();
		return $result;
	}
	public function saveNotification($data)
	{
		$datas = array(
			'notification_to'=>$data['notifyTo'],
			'notification_from'=>$data['notifyFrom'],
			'notification_msg'=>$data['notifyMsg']
		);
		$this->db->insert('credit_notification',$datas);
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{
			$data1 = array('offer_confirmation'=>1);
			$this->db->where('userRefId',$data['notifyFrom']);
			$this->db->where('offerId',$data['offerId']);
			$this->db->update('credit_offer_purchased',$data1);
			return true;
		}
		else
		{
			return false;
		}
		
	}
	public function getNotificationDetail()
	{
		$userrefId = $this->session->userdata('userRefId');
		$this->db->select('*');
		$this->db->from('credit_notification');
		$this->db->where('notification_to',$userrefId);
		$result = $this->db->get();
		$result = $result->result();
		return $result;
		
	}
	public function withdrawMoneyRequest($param)
	{
		$userrefId = $this->session->userdata('userRefId');
		$uniqueId = generateRef();
		$data = array(
			'withdraw_amount'=>$param['withdrawAmount'],
			'userrefId'=>$userrefId,
			'uniqueId'=>$uniqueId,
			'request_date'=>date('Y-m-d'),
			'status' => 0
		);
		$this->db->insert('credit_withdraw_amount',$data);
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
