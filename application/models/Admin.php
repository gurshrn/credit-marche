<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	/*=================Get all user detail=================*/
	
	public function login($data)
	{
		$salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
        $salt = base64_encode($salt);
        $salt = str_replace('+', '.', $salt);
        $hash = crypt($data['password'], '$2y$10$' . $salt . '$');		
		$this->db->select('*');		
		$this->db->from('credit_login_detials');		
		$this->db->where('tel_number', "AES_ENCRYPT('{$data['tel_number']}', '/*awshp$*/')", FALSE);		
		$this->db->where('user_active', 1);        
		$this->db->where('type', 'Admin');        
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;
	}

    public function getAllUsers()
    {
	 	$this->db->select('credit_login_detials.*,credit_user_detail.*,AES_DECRYPT(credit_login_detials.email,"/*awshp$*/") as email, AES_DECRYPT(credit_login_detials.tel_number,"/*awshp$*/") as tel_number,region_residence.region as residence,network_operator.operator as network_operator,age_range.range as age_range');
        $this->db->from('credit_login_detials');
        $this->db->join('credit_user_detail','credit_user_detail.userRefId = credit_login_detials.userRefId','left');
        $this->db->join('region_residence','region_residence.id = credit_user_detail.residence','left');
        $this->db->join('network_operator','network_operator.id = credit_user_detail.network_operator','left');
        $this->db->join('age_range','age_range.id = credit_user_detail.age_range','left');
        $this->db->where('credit_login_detials.otp = 0');
        $this->db->where('credit_login_detials.type' , 'Customer');
        $result = $this->db->get();
        $result = $result->result();
        return $result;
    }
	
	public function countOfferPurchased()
	{
		$this->db->select('count(id) as totalPurchased');
		$this->db->from('credit_offer_purchased');
		$result = $this->db->get();
        $result = $result->row();
        return $result;
	}
	public function countOfferSold()
	{
		$this->db->select('count(id) as totalSold');
		$this->db->from('credit_publish_offer');
		$this->db->where('status',4);
		$result = $this->db->get();
        $result = $result->row();
        return $result;
	}
	public function countRecentOfferSold()
	{
		$this->db->select('count(credit_offer_purchased.id) as totalRecentSold');
		$this->db->from('credit_publish_offer');
		$this->db->join('credit_offer_purchased','credit_offer_purchased.offerId = credit_publish_offer.id','left');
		$this->db->where('credit_publish_offer.status',4);
		$this->db->where('credit_offer_purchased.addedondate',date('Y-m-d'));
		$result = $this->db->get();
        $result = $result->row();
        return $result;
	}
	public function totalEarnedCommission()
	{
		$this->db->select('Sum(commission) as totalEarned');
		$this->db->from('credit_payment');
		$result = $this->db->get();
        $result = $result->row();
        return $result;
	}
	
	/*=================Update user status=================*/

    public function statusUpdate($data)
    {
        if($data['statusType'] == 'Disable' || $data['statusType'] == 'Enable')
        {
            $status = array('user_active' => $data['status']);
            $this->db->where('userRefId',$data['userrefId']);
            $this->db->update('credit_login_detials',$status);
            if ($this->db->affected_rows() == '1') 
            {
                return TRUE;
            } 
            else 
            {
                
                return False;
            }
        }
        if($data['statusType'] == 'Reject' || $data['statusType'] == 'Approve')
        {
            $status = array('status' => $data['status']);
            $this->db->update('credit_login_detials',$status);
            $this->db->where('userRefId',$data['userrefId']);
            if ($this->db->affected_rows() == '1') 
            {
                return TRUE;
            } 
            else 
            {
                
                return False;
            }
        }

    }

	/*=================get all offers detail=================*/
	
	public function getOffers()	
	{		
		$this->db->select('credit_publish_offer.*,AES_DECRYPT(credit_login_detials.tel_number,"/*awshp$*/") as tel_number');		
		$this->db->from('credit_publish_offer');
		$this->db->join('credit_login_detials','credit_login_detials.userRefId = credit_publish_offer.userRefId','left');
		$this->db->order_by('credit_publish_offer.id','Desc');	
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;	
	}
	
	/*=================get offers detail for edit and view offers=================*/
	
	public function offerDetalById($id)
	{
		$this->db->select('credit_publish_offer.*,credit_user_detail.first_name,credit_user_detail.last_name,AES_DECRYPT(credit_login_detials.tel_number,"/*awshp$*/") as tel_number,credit_networks.network,credit_networks.id as network_id');
        $this->db->from('credit_publish_offer');
        $this->db->join('credit_networks','credit_networks.id = credit_publish_offer.credit_network','left');
        $this->db->join('credit_login_detials','credit_login_detials.userRefId = credit_publish_offer.userRefId','left');
        $this->db->join('credit_user_detail','credit_user_detail.userRefId = credit_publish_offer.userRefId','left');
        $this->db->where('credit_publish_offer.id',$id);
		$result = $this->db->get();		
		$result = $result->row();		
		return $result;
	}
	
	/*===============Offer search================*/
	
	public function getOfferByStatus($data)
	{
		$this->db->select('credit_publish_offer.*,AES_DECRYPT(credit_login_detials.tel_number,"/*awshp$*/") as tel_number');		
		$this->db->from('credit_publish_offer');
		$this->db->join('credit_login_detials','credit_login_detials.userRefId = credit_publish_offer.userRefId','left');
		if($data['statusid'] != '' || $data['typeid'] != '' || $data['keyword'] != '')
		{
			if(isset($data['status']))
			{
				$this->db->group_start();
				if($data['status'] == 'status')
				{
					$this->db->where('credit_publish_offer.status',$data['statusid']);	
				}
				$this->db->group_end();
			}
			
			if(isset($data['type']))
			{
				$this->db->group_start();
				if($data['type'] == 'offer-type')
				{
					$this->db->where('credit_publish_offer.offer_type',$data['typeid']);
				}
				$this->db->group_end();
			}
			
			
			$this->db->group_start();
				$this->db->like('credit_publish_offer.credits' , $data['keyword'],'after');
				$this->db->or_like('credit_publish_offer.selling_price' , $data['keyword'],'after');
				$this->db->or_like('credit_publish_offer.internet_volume' , $data['keyword'],'after');
			$this->db->group_end();
			
			
		}
		$this->db->order_by('credit_publish_offer.id','Desc');	
		$result = $this->db->get();	
		$result = $result->result();		
		return $result;
	}
	
	/*===================get detail of credit network===================*/
	
	public function getCreditsNetwork()
	{
		$this->db->select('*');		
		$this->db->from('credit_networks');
		$this->db->order_by('id','Desc');	
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;	
	}
	
	/*===================get detail of network operator===================*/ 
	
	public function getAllNetworkOperator()
	{
		$this->db->select('*');		
		$this->db->from('network_operator');
		$this->db->order_by('id','Desc');	
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;	
	}
	
	/*===============Common insert and edit function=================*/

	public function commonInsert($data,$tablename)
	{
		if($data['id'] == '')
		{
			$this->db->insert($tablename,$data);
			$db_error = $this->db->error();		
			if ($db_error['code'] == 0) 		
			{			
				$result['success'] = true;			
				$result['success_message'] = 'Added successfully';		
			} 		
			else		
			{			
				$result['success'] = false;			
				$result['error_message'] = 'Not Added successfully';		
			}	
		}
		if($data['id'] != '')
		{
			$this->db->where('id',$data['id']);
			$this->db->update($tablename,$data);
			$db_error = $this->db->error();		
			if ($db_error['code'] == 0) 		
			{			
				$result['success'] = true;			
				$result['success_message'] = 'Updated successfully';		
			} 		
			else		
			{			
				$result['success'] = false;			
				$result['error_message'] = 'Not Updated successfully';		
			}	
		}
		return $result;
	}
	
	/*===============Common edit get detail function==================*/

	public function getDetailById($id,$tablename)
	{
		$this->db->select('*');		
		$this->db->from($tablename);
		$this->db->where('id',$id);	
		$result = $this->db->get();		
		$result = $result->row();		
		return $result;
	}
	
	/*===============get Commission detail================*/
	
	public function getCommission()
	{
		$this->db->select('*');		
		$this->db->from('credit_commission');
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;
	}
	
	/*==============get transaction detail of offers================*/
	
	public function transactionDetial($userRefId = NULL)
	{
		$this->db->select('credit_payment.*,credit_user_detail.first_name,credit_user_detail.last_name');		
		$this->db->from('credit_payment');
		$this->db->join('credit_user_detail','credit_user_detail.userRefId = credit_payment.userRefId','left');
		if($userRefId != '')
		{
			$this->db->where('credit_payment.userRefId',$userRefId);
		}
		$this->db->order_by('credit_payment.id','desc');
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;
	}
	public function getCommissionByUser()
	{
		$this->db->select('credit_user_detail.* , (select sum(credit_payment.debit) from credit_payment where userRefId = credit_user_detail.userRefId AND transaction_type = "Sales Commission") as totalCommission');		
		$this->db->from('credit_user_detail');
		$this->db->join('credit_payment','credit_payment.userRefId = credit_user_detail.userRefId','inner');
		$this->db->group_by('credit_user_detail.userrefId');
		
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;
		
	}
	public function getChatMessagesDetail()
	{
		$this->db->select('*');		
		$this->db->from('chat_detail');
		$this->db->where('msg_to','Admin');
		$this->db->group_by('msg_from');
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;
	}
	public function getChatHistory($data)
	{
		$this->db->select('*');		
		$this->db->from('chat_detail');
		$this->db->where('msg_from',$data['userRefId']);
		$this->db->or_where('msg_to',$data['userRefId']);
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;
	}
	public function getSearchByTransaction($data)
	{
		$this->db->select('credit_payment.*,credit_user_detail.first_name,credit_user_detail.last_name');		
		$this->db->from('credit_payment');
		$this->db->join('credit_user_detail','credit_user_detail.userRefId = credit_payment.userRefId','left');
		if($data['transactiondate'] != '')
		{
			$this->db->group_start();
				$this->db->where('credit_payment.addedondate' , date('Y-m-d',strtotime($data['transactiondate'])));
			$this->db->group_end();
			
		}
		if($data['paymentType'] != '')
		{
			$this->db->group_start();
			$this->db->where('credit_payment.transaction_type' , $data['paymentType']);
			$this->db->group_end();
			
		}
		if($data['keyword'] != '')
		{
			$this->db->group_start();
				$this->db->like('credit_user_detail.first_name' , $data['keyword'],'after');
				$this->db->or_like('credit_payment.running_balance' , $data['keyword'],'after');
				if($data['keyword'] != 0)
				{
					$this->db->or_like('credit_payment.debit' , $data['keyword'],'after');
					$this->db->or_like('credit_payment.credit' , $data['keyword'],'after');
				}
			$this->db->group_end();
			
		}
		$this->db->order_by('credit_payment.id','desc');
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;
	}
	public function getFrontendFooterList()
	{
		$this->db->select('*');		
		$this->db->from('credit_frontend_footer');
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;
	}
	public function editFooter($id)
	{
		$this->db->select('*');		
		$this->db->from('credit_frontend_footer');
		$this->db->where('id',$id);
		$result = $this->db->get();		
		$result = $result->row();		
		return $result;
	}
	public function getBlogListDetail()
	{
		$this->db->select('*');		
		$this->db->from('credit_blog');
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;
	}
	public function getBlogDetailById($id)
	{
		$this->db->select('*');		
		$this->db->from('credit_blog');
		$this->db->where('id',$id);
		$result = $this->db->get();		
		$result = $result->row();		
		return $result;
	}
	public function getFaqList()
	{
		$this->db->select('*');		
		$this->db->from('credit_faq');
		$this->db->order_by('id','DESC');
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;
	}
	public function getFaqDetailById($id)
	{
		$this->db->select('*');		
		$this->db->from('credit_faq');
		$this->db->where('id',$id);
		$result = $this->db->get();		
		$result = $result->row();		
		return $result;
	}
	
	
}