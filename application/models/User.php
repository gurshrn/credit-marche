<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function login($data)
    {
		$salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
        $salt = base64_encode($salt);
        $salt = str_replace('+', '.', $salt);
        $hash = crypt($data['password'], '$2y$10$' . $salt . '$');
		
		$this->db->select('*');		
		$this->db->from('credit_login_detials');		
		$this->db->where('tel_number', "AES_ENCRYPT('{$data['tel_number']}', '/*awshp$*/')", FALSE);		
		//$this->db->where('access_pin', $hash);		
		$this->db->where('user_active', 1);        
		$this->db->where('status', 1);        
		$this->db->where('country_code', $data['country_code']);		
		$this->db->where('otp', 0);		
		$this->db->where('type', 'Customer');		
		$result = $this->db->get();		
		$result = $result->result();		
		return $result;
    }

    public function addUser($data,$postdata)
    {
        $this->db->set('email', "AES_ENCRYPT('{$postdata['email']}','/*awshp$*/')", FALSE);
        $this->db->set('tel_number', "AES_ENCRYPT('{$postdata['tel_number']}','/*awshp$*/')", FALSE);
        $this->db->set('type', 'Customer');
	 	$this->db->insert('credit_login_detials',$data);
        $userId = $this->db->insert_id();
        $db_error = $this->db->error();
        if ($db_error['code'] != 0) 
        {
            $result['success'] = false;
            $result['error_message'] = $db_error['message'];
        } 
        else 
        {
            $userdata = array(
                    'userRefId' => $data['userRefId'],
                    'civility' => $postdata['civility'],
                    'first_name' => $postdata['first_name'],
                    'last_name' => $postdata['last_name'],
                    'residence' => $postdata['residence'],
                    'network_operator' => $postdata['network_operator'],
                    'age_range' => $postdata['age_range'],
                    'created_date' => date('Y-m-d'),
            );
            
            $this->db->insert('credit_user_detail',$userdata);
            
            $result['success'] = true;
            $result['userRefId'] = $data['userRefId'];
            $result['success_message'] = 'Thanks for registering with us. Please check verify your OTP to login.';
        }
        return $result;
    }

    public function checkUserOtp($param)
    {
        if ($param['otp'] == 0) 
        {
            $response['success'] = false;
            $response['error_message'] = 'OTP is not matched.Please Check again.';
        } 
        else 
        {
            $this->db->select('otp,expiry_time,userRefId');
            $this->db->from('credit_login_detials');
            $this->db->where('otp', $param['otp']);
            $this->db->where('user_active', 0);
            $this->db->where('userRefId', $param['userRefId']);
            
            $query = $this->db->get();
            $output = $query->result();



            if (!empty($output) || count($output) > 0) 
            {
                $currentTime = date("Y-m-d H:i:s");
                if ($currentTime <= $output[0]->expiry_time) 
                {
                    $this->db->set('otp', 0);
                    $this->db->set('user_active', 1);
                    $this->db->where('userRefId', $output[0]->userRefId);
                    $this->db->update('credit_login_detials');

                    $response['success'] = true;
                    $response['success_message'] = 'Your OTP verify successfully.';
                } 
                else 
                {
                    $response['success'] = false;
                    $response['error_message'] = 'OTP IS EXPIRED.';
                }
            } 
            else 
            {
                $response['success'] = false;
                $response['error_message'] = 'OTP is not matched.Please Check agains.';
            }
        }
        return $response;
    }

    
    
   
}