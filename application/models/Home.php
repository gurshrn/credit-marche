<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	public function updateExpiryStatus()
	{
		$this->db->select('*');
        $this->db->from('credit_publish_offer');
        $result = $this->db->get();
        $result = $result->result();
		foreach($result as $val)
		{
			$expirydate = $val->expiry_date;
			if($val->expiry_date != '')
			{
				$expiry_time = date('Y-m-d H:i:s', strtotime($expirydate));
				$expiry_date = date('Y-m-d H:i:s',strtotime('-6 hour',strtotime($expiry_time)));
				$currentdatetime = date('Y-m-d H:i:s');	
				if($currentdatetime >= $expiry_date)
				{
					$status = array('status' => 3);
					$this->db->where('id',$val->id);
					$this->db->where('status != 4');
					$this->db->update('credit_publish_offer',$status);
					
				}
			}
			
		}
		return $result;
	}
	public function updateStandByStatus()
	{
		$data['userRefId'] = $this->session->userdata('userRefId');
	}
	public function getRegions()
    {
	 	$this->db->select('*');
        $this->db->from('region_residence');
        $result = $this->db->get();
        $result = $result->result();
        return $result;
    }
    public function getAgeRange()
    {
    	$this->db->select('*');
        $this->db->from('age_range');
        $result = $this->db->get();
        $result = $result->result();
        return $result;
    }
    public function getNetworkOperator()
    {
    	$this->db->select('*');
        $this->db->from('network_operator');
        $result = $this->db->get();
        $result = $result->result();
        return $result;
    }
	public function getCreditNetwork()
	{
		$this->db->select('*');
        $this->db->from('credit_networks');
        $result = $this->db->get();
        $result = $result->result();
        return $result;
	}
    public function checEmail($data)
    {
        $this->db->select('email');
        $this->db->from('credit_login_detials');
        $this->db->where('user_active',1);
        $this->db->where('email',"AES_ENCRYPT('{$data['email']}', '/*awshp$*/')", FALSE);
        $this->db->where('otp',0);
        $result = $this->db->get();
        $result = $result->result();
        return $result;		
	}		
	public function createCreditOffer($data)		
	{
		unset($data['types']);
		$data['userRefId'] = $this->session->userdata('userRefId');
		if($data['id'] == '')
		{
			$data['status'] = 1;				
			$data['offer_type']=1;				
			$data['addedondate']=date('Y-m-d');				
			$this->db->insert('credit_publish_offer',$data);				
			$db_error = $this->db->error();				
			if ($db_error['code'] != 0) 				
			{						
				$result['success'] = false;						
				$result['error_message'] = 'Offer not created successfully';				
			} 				
			else				
			{						
				$result['success'] = true;						
				$result['success_message'] = 'Offer created successfully';				
			}				
		}
		if($data['id'] != '')
		{
			$this->db->where('id',$data['id']);
			$this->db->update('credit_publish_offer',$data);
			$db_error = $this->db->error();				
			if ($db_error['code'] != 0) 				
			{						
				$result['success'] = false;						
				$result['error_message'] = 'Offer not updated successfully';				
			} 				
			else				
			{						
				$result['success'] = true;						
				$result['success_message'] = 'Offer updated successfully';				
			}				
		}
		return $result;		
	}
	public function createInternetOffer($data)	
	{	
	
		
		$data1['internet_volume']=$data['internet_volume'];		
		$data1['expiry_date']=$data['network_expiry_date'];		
		$data1['selling_price']=$data['network_selling_price'];		
		$data1['credit_network']=$data['internet_network'];		
		$data1['credit_network_status']=$data['internet_credit_status'];		
		if($data['id'] == '')
		{
			$data1['status'] = 1;		
			$data1['offer_type']=2;
			$data1['addedondate']=date('Y-m-d');		
			$data1['userRefId'] = $this->session->userdata('userRefId');
			$this->db->insert('credit_publish_offer',$data1);
			$db_error = $this->db->error();		
			if ($db_error['code'] != 0) 		
			{			
				$result['success'] = false;			
				$result['error_message'] = 'Internet Offer not created successfully';		
			} 		
			else		
			{			
				$result['success'] = true;			
				$result['success_message'] = 'Internet Offer created successfully';		
			}	
		}	
		if($data['id'] != '')
		{
			$this->db->where('id',$data['id']);
			$this->db->update('credit_publish_offer',$data1);
			$db_error = $this->db->error();		
			if ($db_error['code'] != 0) 		
			{			
				$result['success'] = false;			
				$result['error_message'] = 'Internet Offer not updated successfully';		
			} 		
			else		
			{			
				$result['success'] = true;			
				$result['success_message'] = 'Internet Offer updated successfully';		
			}
		}
			
		return $result;	
	}
	public function getofferdetail($id)
	{
		$userRefId = $this->session->userdata('userRefId');
		$this->db->select('credit_publish_offer.*,credit_networks.network');
        $this->db->from('credit_publish_offer');
        $this->db->join('credit_networks','credit_networks.id = credit_publish_offer.credit_network','left');
        $this->db->where('credit_publish_offer.id',$id);
		$result = $this->db->get();
        $result = $result->row();
        return $result;	
	}
	public function getAllOffers($limit= NULL,$start = NULL)
	{
		$userRefId = $this->session->userdata('userRefId');
		$this->db->select('credit_publish_offer.*,credit_networks.network');
        $this->db->from('credit_publish_offer');
        $this->db->join('credit_networks','credit_networks.id = credit_publish_offer.credit_network','left');
        $this->db->where('credit_publish_offer.status',1);
        $this->db->where('credit_publish_offer.status !=',5);
		// if($this->session->userdata('userRefId'))
		// {
			// $this->db->where('credit_publish_offer.userRefId !=',$userRefId);
		// }
		$this->db->order_by('credit_publish_offer.id','desc');
		if($limit != '')
		{
			$this->db->limit($limit,$start);
		}
		$result = $this->db->get();
		//echo $this->db->last_query();die;
		$result = $result->result();
        return $result;	
	}
	
	public function getWalletDetail($userRefId)
	{
		$this->db->select('*');
        $this->db->from('credit_wallet');
        $this->db->where('userRefId',$userRefId);
		$result = $this->db->get();
        $result = $result->row();
        return $result;	
	}
	
	public function savePurchasePayment($data)
	{
		$this->db->select('*');
		$this->db->from('credit_payment');
		$this->db->where('userRefId',$data['sellerRefId']);
		$this->db->where('addedondate',date('Y-m-d'));
		$this->db->where('transaction_type','OfferSold');
		$result = $this->db->get();
        $result = $result->result();
		$transactions = count($result);
        $data0 = array(
					array(
						'time'=>date('H:i:s'),
						'transaction_type'=>$data['buyerTransactionType'],
						'userRefId'=>$data['buyerrefId'],
						'debit'=>$data['debit'],
						'date'=>date('Y-m-d'),
						'transaction_id'=>'CM-'.$data['transactionId'],
						'createdDateTime'=>date('Y-m-d H:i:s'),
						'status'=>1,
						'addedondate'=>date('Y-m-d'),
						'running_balance'=>$data['buyerWallet'],
					),
					array(
						'time'=>date('H:i:s'),
						'transaction_type'=>'Sales Commission',
						'userRefId'=>$data['buyerrefId'],
						'debit'=>$data['buyerCm'],
						'date'=>date('Y-m-d'),
						'transaction_id'=>'CM-'.$data['transactionId'],
						'createdDateTime'=>date('Y-m-d H:i:s'),
						'status'=>1,
						'addedondate'=>date('Y-m-d'),
						'running_balance'=>$data['buyerWallet']-$data['buyerCm'],
					),
					
				);
		
		$this->db->insert_batch('credit_payment',$data0);
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{
			$datas = array(
						'time'=>date('H:i:s'),
						'transaction_type'=>$data['sellerTransactionType'],
						'userRefId'=>$data['sellerRefId'],
						'credit'=>$data['credit'],
						'date'=>date('Y-m-d'),
						'transaction_id'=>'CM-'.$data['transactionId'],
						'createdDateTime'=>date('Y-m-d H:i:s'),
						'status'=>1,
						'addedondate'=>date('Y-m-d'),
						'running_balance'=>$data['sellerWallet'],
					);
			$datas1	= array(
						'time'=>date('H:i:s'),
						'transaction_type'=>'Sales Commission',
						'userRefId'=>$data['sellerRefId'],
						'debit'=>$data['sellerCm'],
						'date'=>date('Y-m-d'),
						'transaction_id'=>'CM-'.$data['transactionId'],
						'createdDateTime'=>date('Y-m-d H:i:s'),
						'status'=>1,
						'commission'=>$data['sellerCm'],
						'addedondate'=>date('Y-m-d'),
						'running_balance'=>$data['sellerWallet']-$data['sellerCm'],
					);
			$this->db->insert('credit_payment',$datas);
			$this->db->insert('credit_payment',$datas1);
			$data1 = array(
				'userRefId'=>$data['buyerrefId'],
				'offerId'=>$data['offerId'],
				'addedondate'=>date('Y-m-d'),
				'transactionId'=>'CM-'.$data['transactionId'],
				'createdDateTime'=>date('Y-m-d H:i:s'),
				'status'=>1
				);
			$this->db->insert('credit_offer_purchased',$data1);
			$db_error = $this->db->error();		
			if ($db_error['code'] == 0) 		
			{
				$data2 = array('wallet_amount'=>$data['buyerWallet']-$data['buyerCm']);
				$this->db->where('userRefId',$data['buyerrefId']);
				$this->db->update('credit_wallet',$data2);

				$data3 = array('wallet_amount'=>$data['sellerWallet']-$data['sellerCm']);
				$this->db->where('userRefId',$data['sellerRefId']);
				$this->db->update('credit_wallet',$data3);
				
				$updatestatus = array('status'=>4);
				$this->db->where('id',$data['offerId']);
				$this->db->update('credit_publish_offer',$updatestatus);
				
				if($transactions == 2)
				{
					$updatestatus1 = array('status'=>5);
					$this->db->where('userRefId',$data['sellerRefId']);
					$this->db->where('status !=',4);
					$this->db->update('credit_publish_offer',$updatestatus1);
				}
				return true;
			} 		
			else		
			{			
				return false;		
			}
		}
		else
		{
			return false;
		}
	}
	public function getOffersDetailByKey($data)
	{
		$userRefId = $this->session->userdata('userRefId');
		$this->db->select('credit_publish_offer.*,credit_networks.network,credit_networks.id as network_id');
        $this->db->from('credit_publish_offer');
        $this->db->join('credit_networks','credit_networks.id = credit_publish_offer.credit_network','left');
        if(isset($data['offerdate']) && $data['offerdate'] != '')
		{
			$this->db->where('credit_publish_offer.addedondate' , date('Y-m-d',strtotime($data['offerdate'])));
			
		}
		if(isset($data['offertype']) && $data['offertype'] != '')
		{
			$this->db->where('credit_publish_offer.offer_type' , $data['offertype']);
		}
		if(isset($data['networkOperator']) && $data['networkOperator'] != '')
		{
			$this->db->where('credit_publish_offer.credit_network' , $data['networkOperator']);
		}
		if(isset($data['offerStatus']) && $data['offerStatus'] != '')
		{
			$this->db->where('credit_publish_offer.status',$data['offerStatus']);
		}
		$this->db->where('credit_publish_offer.userRefId',$userRefId);
		$this->db->where('credit_publish_offer.status !=',5);
		$this->db->order_by('id','Desc');	
		$result = $this->db->get();	
		$result = $result->result();		
		return $result;
	}
	public function getPurchaseOffersDetailByKey($data)
	{
		$userRefId = $this->session->userdata('userRefId');
		$this->db->select('credit_publish_offer.*,credit_networks.network,credit_offer_purchased.transactionId,credit_offer_purchased.userRefId as buyerRefId,credit_offer_purchased.addedondate as purchasedate,credit_offer_purchased.createdDateTime,credit_offer_purchased.offer_confirmation');
        $this->db->from('credit_publish_offer');
        $this->db->join('credit_networks','credit_networks.id = credit_publish_offer.credit_network','left');
        $this->db->join('credit_offer_purchased','credit_offer_purchased.offerId = credit_publish_offer.id','left');
		if(isset($data['offerdate']) &&$data['offerdate'] != '')
		{
			$this->db->where('credit_publish_offer.addedondate' , date('Y-m-d',strtotime($data['offerdate'])));
			
		}
		if(isset($data['offertype']) && $data['offertype'] != '')
		{
			$this->db->where('credit_publish_offer.offer_type' , $data['offertype']);
		}
		if(isset($data['networkOperator']) &&$data['networkOperator'] != '')
		{
			$this->db->where('credit_publish_offer.credit_network' , $data['networkOperator']);
		}
		$this->db->where('credit_offer_purchased.userRefId',$userRefId);
		$this->db->order_by('credit_publish_offer.id','Desc');
		$result = $this->db->get();
		$result = $result->result();
		return $result;
	}
	public function getSoldOffersDetailByKey($data)
	{
		$userRefId = $this->session->userdata('userRefId');
		$this->db->select('credit_publish_offer.*,credit_networks.network,credit_offer_purchased.addedondate as offersolddate');
        $this->db->from('credit_publish_offer');
        $this->db->join('credit_networks','credit_networks.id = credit_publish_offer.credit_network','left');
        $this->db->join('credit_offer_purchased','credit_offer_purchased.offerId = credit_publish_offer.id','left');
		if($data['offerdate'] != '')
		{
			$this->db->where('credit_publish_offer.addedondate' , date('Y-m-d',strtotime($data['offerdate'])));
			
		}
		if($data['offertype'] != '')
		{
			$this->db->where('credit_publish_offer.offer_type' , $data['offertype']);
		}
		if($data['networkOperator'] != '')
		{
			$this->db->where('credit_publish_offer.credit_network' , $data['networkOperator']);
		}
		$this->db->where('credit_publish_offer.userRefId',$userRefId);
        $this->db->where('credit_publish_offer.status',4);
		$this->db->order_by('credit_publish_offer.id','Desc');
		$result = $this->db->get();
		$result = $result->result();
		return $result;
	}
	public function getHomeOffersDetailByKey($data,$limit = NULL)
	{
		$userRefId = $this->session->userdata('userRefId');
		$this->db->select('credit_publish_offer.*,credit_networks.network');
        $this->db->from('credit_publish_offer');
        $this->db->join('credit_networks','credit_networks.id = credit_publish_offer.credit_network','left');
        if(isset($data['offertype']) &&  $data['offertype'] != '')
		{
			$this->db->where('credit_publish_offer.offer_type' , $data['offertype']);
		}
		if(isset($data['networkOperator']) &&  $data['networkOperator'] != '')
		{
			$this->db->where('credit_publish_offer.credit_network' , $data['networkOperator']);
		}
		// if($this->session->userdata('userRefId'))
		// {
			// $this->db->where('userRefId !=',$userRefId);
		// }
		$this->db->where('credit_publish_offer.status',1);
		$this->db->where('credit_publish_offer.status !=',5);
		$this->db->order_by('credit_publish_offer.id','desc');
		if($limit != '')
		{
			$this->db->limit(5);
		}
		$result = $this->db->get();
		$result = $result->result();
		return $result;
	}
	public function getPurOfferDetail()
	{
		$userRefId = $this->session->userdata('userRefId');
		$this->db->select('*');
		$this->db->from('credit_offer_purchased');
		$this->db->where('userRefId',$userRefId);
		$this->db->where('offer_confirmation',0);
		$result = $this->db->get();
		$result = $result->result();
		return $result;
	}
	public function addContactForm($data)
	{
		$data['addedondate'] = date('Y-m-d');
		$this->db->insert('credit_contact_form',$data);
		$db_error = $this->db->error();		
		if ($db_error['code'] == 0) 		
		{			
			$result['success'] = true;			
			$result['success_message'] = 'Message sent successfully';		
		} 		
		else		
		{			
			$result['success'] = false;			
			$result['error_message'] = 'Message not sent successfully';		
		}	
		
		return $result;
	}
	public function getFooterDetailById($id)
	{
		$this->db->select('*');		
		$this->db->from('credit_frontend_footer');
		$this->db->where('id',$id);
		$result = $this->db->get();		
		$result = $result->row();		
		return $result;
	}
	// public function getCommissionDetail()
	// {
		// $this->db->select('*');
        // $this->db->from('network_operator');
        // $result = $this->db->get();
        // $result = $result->result();
        // return $result;
	// }
}